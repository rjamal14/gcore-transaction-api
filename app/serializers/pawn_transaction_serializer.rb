# Transaction Serializer
class PawnTransactionSerializer < ActiveModel::Serializer
  attributes :id, :sge, :status, :contract_date, :office_id, :due_date, :product_id,
             :auction_date, :loan_amount, :estimate_value, :prolongation_period, :prolongation_date, :monthly_fee
  has_one :customer, only: [:name, :cif_number]
  has_one :company, only: [:name, :cif_number]
  has_many :transaction_insurance_items
  has_many :transaction_deviations

  def estimate_value
    object.try(:transaction_insurance_items).sum(:estimated_value)
  end
end
