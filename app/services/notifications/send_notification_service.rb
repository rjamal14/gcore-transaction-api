# Journal
require 'json'
require 'base64'

module Notifications
  class SendNotificationService
    def initialize(msg)
      @queue_name = "#{ENV['rbmq_env']}-notifications-queue"

      @notification = {
        type: msg[:type],
        offices: msg[:offices],
        subject: msg[:subject],
        body: msg[:body],
        url: msg[:url]
      }
    end

    def execute
      @notification
      publish
    end

    def channel
      @channel ||= ::Publisher::BunnyPublisher.connection.create_channel
    end

    def exchange
      @exchange ||= channel.exchange(
        "#{ENV['rbmq_env']}-notifications-queue",
        type: 'direct',
        durable: true
      )
    end

    def publish
      exchange.publish(@notification.to_json, routing_key: @queue_name)
      channel.close
    end
  end
end
