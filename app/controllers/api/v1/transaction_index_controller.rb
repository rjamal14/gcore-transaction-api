module Api
  module V1
    # Transaction index
    class TransactionIndexController < ApplicationController
      skip_before_action :current_user, only: :repayment_autocomplete
      def index_disbursement
        @transactions = PawnTransaction.where(transaction_type: :disbursement)
        transaction_type
      end

      def index_installment
        @transactions = PawnTransaction.where(transaction_type: :installment)
        transaction_type
      end

      def index_canceled
        @transactions = PawnTransaction.where(transaction_type: :cancelled)
        transaction_type
      end

      def index_repayment
        @transactions = PawnTransaction.where(transaction_type: :repayment)
        transaction_type
      end

      def index_auction
        @transactions = PawnTransaction.where(transaction_type: :auction)
        transaction_type
      end

      def index_time_extension
        @transactions = PawnTransaction.where(transaction_type: :prolongation)
        transaction_type
      end

      def repayment_autocomplete
        return response_bad('Parameter Tidak lengkap') unless params.key?(:office_id)

        transactions = PawnTransaction.repayment.where(office_id: params[:office_id], status: :published,
                                                       payment_status: false)
        transactions = if params[:query]
                         transactions.where('sge ILIKE ?', "%#{params[:query]}%")
                       else
                         transactions
                       end
        transactions = transactions.limit(10).order(created_at: :desc)
        transactions = transactions.map { |transaction| RepaymentAutocompleteSerializer.new(transaction, root: false) }

        response_success(transactions, 'Data ditemukan')
      end

      private

      def transaction_type
        @transactions = case params[:type]
                        when 'customer'
                          @transactions.customer
                        when 'company'
                          @transactions.company
                        else
                          @transactions
                        end
        check_office
      end

      def check_office
        @transactions = case current_user.office_type
                        when 'head'
                          @transactions
                        when 'region'
                          @transactions.where(region_id: current_user.region)
                        when 'area'
                          @transactions.where(area_id: current_user.area)
                        when 'branch'
                          @transactions.where(branch_id: current_user.branch)
                        when 'unit'
                          @transactions.where(office_id: current_user.office_id)
                        end
        render_response
      end

      def render_response
        @transactions = @transactions.page(params[:page] || 1).per(params[:per_page] || 10)
                                     .order("#{params[:order_by] || 'created_at'} #{params[:order] || 'desc'}")
        @transactions = if params[:search]
                          @transactions.search(params[:search])
                        else
                          @transactions
                        end

        @transactions = @transactions.ransack(params[:q]).result
        serial_transactions = @transactions.map { |transactions| TransactionIndexSerializer.new(transactions, root: false) }

        response_index(@transactions, serial_transactions)
      rescue StandardError => e
        response_bad(e)
      end
    end
  end
end
