# CustomerContact
class CustomerContact < ApplicationRecord
  acts_as_paranoid
  validates_uniqueness_of :phone_number

  attr_accessor :residence_province_name,
                :residence_city_name,
                :residence_region_name,
                :residence_subdistric_name,
                :identity_province_name,
                :identity_city_name,
                :identity_region_name,
                :identity_subdistric_name

  belongs_to :customer, -> { with_deleted }, optional: true
end
