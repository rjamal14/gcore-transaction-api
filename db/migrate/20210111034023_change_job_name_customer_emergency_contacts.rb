class ChangeJobNameCustomerEmergencyContacts < ActiveRecord::Migration[6.0]
  def change
    change_column :customer_emergency_contacts, :job_name, :string, :null => true
  end
end
