class CreateCashoutTransaction < ActiveRecord::Migration[6.0]
  def change
    create_table :cashout_transactions do |t|
      t.string :user_id
      t.string :user_name
      t.datetime :date
      t.string :ref
      t.float :amount
      t.text :description
      t.integer :status
      t.string :office_id
      t.string :created_by_id
      t.string :updated_by_id
      t.string :deleted_by_id
      t.datetime :deleted_at, index: true
      t.timestamps
    end
  end
end
