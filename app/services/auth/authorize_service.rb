# Autorize Service
require 'oauth2'

module Auth
  class AuthorizeService
    def initialize
      @client = OAuth2::Client.new(
        ENV['CLIENT_ID'],
        ENV['CLIENT_SECRET'],
        site: ENV['AUTH_URL'],
        logger: Logger.new("log/auth-#{Time.now.strftime('%d-%m-%Y')}.log", 'daily')
      )

      @token_barier = "Bearer #{credentials.token}"
    end

    def credentials
      @client.client_credentials.get_token
    end

    def password(username, password)
      @client.password.get_token(username, password)
    end

    def auth_code(code)
      @client.auth_code.get_token(code, redirect_uri: 'urn:ietf:wg:oauth:2.0:oob')
    end

    def token
      credentials
    end

    def set_token(token_barier = '')
      @token_barier = if token_barier.present?
                        token_barier
                      else
                        "Bearer #{credentials.token}"
                      end
    end

    def current_user
      response = RestClient.get "#{ENV['AUTH_URL']}/oauth/token/info", { Authorization: @token_barier }
    rescue RestClient::Unauthorized, RestClient::Forbidden, RestClient::ImATeapot, RestClient::NotFound
      nil
    else
      data = JSON.parse(response.body)

      if data['resource_owner_type'] == 'User'
        assign_user(data['user'])
      else
        'application'
      end
    end

    def assign_user(data_user)
      data_user['id'] = data_user['_id']['$oid']
      data_user.delete('_id')

      assign_office(data_user)

      @assign_user ||= User.new(data_user.to_h)
    end

    def assign_office(data_user)
      office_parent_data = office_parent(data_user['user_office']['officeable_type'], data_user['user_office']['officeable_id']['$oid'])
      data_user['office_id'] = data_user['user_office']['officeable_id']['$oid']
      data_user['office_type'] = data_user['user_office']['officeable_type']
      data_user['office_name'] = office_parent_data.present? ? office_parent_data['office_name'] : nil
      data_user.delete('user_office')
      %w[branch area region head].map do |office|
        data_user[office] = begin
          office_parent_data[office]['$oid']
        rescue StandardError
          nil
        end
      end
    end

    def get_master(url)
      response = RestClient.get "#{ENV['API_URL_MASTER']}/api/v1/#{url}", { Authorization: @token_barier }
    rescue RestClient::Unauthorized, RestClient::Forbidden, RestClient::ImATeapot, RestClient::NotFound
      nil
    else
      JSON.parse(response.body)
    end

    def get_office(url)
      response = RestClient.get "#{ENV['API_URL_MASTER']}/api/v1/#{url}", {}
    rescue RestClient::Unauthorized, RestClient::Forbidden, RestClient::ImATeapot, RestClient::NotFound
      nil
    else
      JSON.parse(response.body)
    end

    def office_parent(type, id)
      get_office("offices/#{id}?office_type=#{type}")['offices']['data']
    end
  end
end
