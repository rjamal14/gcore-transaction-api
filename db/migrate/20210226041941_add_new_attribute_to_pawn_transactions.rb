class AddNewAttributeToPawnTransactions < ActiveRecord::Migration[6.0]
  def change
    add_column :pawn_transactions, :estimated_value, :integer, default: 0
  end
end
