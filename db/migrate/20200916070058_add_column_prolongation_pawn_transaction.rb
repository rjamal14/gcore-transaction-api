class AddColumnProlongationPawnTransaction < ActiveRecord::Migration[6.0]
  def change
    add_column :pawn_transactions, :prolongation_period, :integer
    add_column :pawn_transactions, :prolongation_date, :date
  end
end
