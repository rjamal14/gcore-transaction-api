class CreateDashboards < ActiveRecord::Migration[6.0]
  def change
    create_table :dashboards do |t|
      t.string :office_id
      t.date :date
      t.string :month
      t.integer :os_disbursement
      t.integer :os_repayment
      t.integer :os_auction
      t.integer :os_prolongation
      t.integer :os_cancelled
      t.integer :os_installment
      t.integer :os_dpd
      t.timestamps
    end
  end
end
