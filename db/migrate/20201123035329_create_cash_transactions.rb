class CreateCashTransactions < ActiveRecord::Migration[6.0]
  def change
    create_table :cash_transactions do |t|
      t.string :req_id, unique: true
      t.string :req_subject
      t.text :req_description
      t.integer :req_status
      t.string :applicant_name
      t.string :req_coa
      t.string :office_id
      t.string :created_by_id
      t.string :updated_by_id
      t.string :deleted_by_id
      t.datetime :deleted_at, index: true
      t.timestamps
    end
  end
end
