class TransactionRule < ApplicationRecord
  acts_as_paranoid
  attribute :minimum_amount
  attribute :maximum_amount

  validates_presence_of :maximum_amount, :minimum_amount
  has_many :transaction_rule_approvals

  accepts_nested_attributes_for :transaction_rule_approvals

  def validate_amount
    errors.add(:jumlah, 'tidak valid') unless minimum_amount < maximum_amount
  end
end
