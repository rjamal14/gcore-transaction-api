class AddIndexToTransaction < ActiveRecord::Migration[6.0]
  def change
    add_index :pawn_transactions, :transaction_type
    add_index :pawn_transactions, :area_id
    add_index :pawn_transactions, :office_id
    add_index :pawn_transactions, :branch_id
    add_index :pawn_transactions, :status
    add_index :pawn_transactions, :region_id
    add_index :pawn_transactions, :insurance_item_id
    add_index :pawn_transactions, :product_id
    add_index :pawn_transactions, :customer_id
  end
end
