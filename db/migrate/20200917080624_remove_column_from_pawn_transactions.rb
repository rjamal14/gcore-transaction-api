class RemoveColumnFromPawnTransactions < ActiveRecord::Migration[6.0]
  def change
    remove_column :pawn_transactions, :disbursement_status, :string
  end
end
