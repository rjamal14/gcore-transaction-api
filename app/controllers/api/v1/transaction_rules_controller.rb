# transaction_rule Controller
module Api
  module V1
    # transaction_rule  Controller
    class TransactionRulesController < ApplicationController
      before_action :set_transaction_rule, only: [:show, :update, :destroy]

      # GET /transaction_rule
      def index
        @transaction_rules = TransactionRule
        @transaction_rules = @transaction_rules.page(params[:page] || 1).per(params[:per_page] || 10)
                                               .order("#{params[:order_by] || 'created_at'} #{params[:order] || 'desc'}")
        serial_transaction_rules = @transaction_rules.map { |transaction_rules| TransactionRuleSerializer.new(transaction_rules, root: false) }

        response_index(@transaction_rules, serial_transaction_rules)
      rescue StandardError => e
        response_bad(e)
      end

      # GET /transaction_rule/1
      def show
        transaction_rule_serial = TransactionRuleSerializer.new(@transaction_rule)
        response_success(transaction_rule_serial, 'Data Ditemukan')
      end

      # POST /transaction_rule
      def create
        @transaction_rule = TransactionRule.new(transaction_rule_params.merge(created_by_id: current_user.id))
        @transaction_rule_serial = TransactionRuleSerializer.new(@transaction_rule)

        if @transaction_rule.save
          response_created(@transaction_rule_serial, 'Data Tersimpan')
        else
          response_error(@transaction_rule, 'Data Gagal Tersimpan')
        end
      end

      # PATCH/PUT /transaction_rule/1
      def update
        if @transaction_rule.update(transaction_rule_params.merge(updated_by_id: current_user.id))
          response_updated(@transaction_rule, 'Data Terupdate')
        else
          response_error(@transaction_rule, 'Data Gagal Terupdate')
        end
      end

      # DELETE /transaction_rule/1
      def destroy
        return unless @transaction_rule.destroy

        response_ok('Data transaction_rule Terhapus')
        @transaction_rule.update(deleted_by_id: current_user.id)
      end

      private

      # Use callbacks to share common setup or constraints between actions.
      def set_transaction_rule
        @transaction_rule = TransactionRule.find(params[:id])
      rescue StandardError
        response_not_found('Data Tidak Ditemukan')
      end

      # Only allow a trusted parameter "white list" through.
      def transaction_rule_params
        params.require(:transaction_rule).permit(:minimum_amount, :maximum_amount,
                                                 transaction_rule_approvals_attributes: [:id, :user_role, :_destroy])
      end
    end
  end
end
