class InstallmentDetail < ApplicationRecord
  belongs_to :pawn_transaction

  attribute :installment_remaining_order, default: 1

  def assign_remaining_loan
    self.remaining_loan = pawn_transaction.loan_amount
  end
end
