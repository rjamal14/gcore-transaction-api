# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_04_27_033437) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "cash_transactions", force: :cascade do |t|
    t.string "req_id"
    t.string "req_subject"
    t.text "req_description"
    t.integer "req_status"
    t.string "applicant_name"
    t.string "req_coa"
    t.string "office_id"
    t.string "created_by_id"
    t.string "updated_by_id"
    t.string "deleted_by_id"
    t.datetime "deleted_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.float "req_amount"
    t.index ["deleted_at"], name: "index_cash_transactions_on_deleted_at"
  end

  create_table "cashout_accounts", force: :cascade do |t|
    t.bigint "cashout_transaction_id"
    t.string "coa"
    t.text "description"
    t.integer "type_account"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["cashout_transaction_id"], name: "index_cashout_accounts_on_cashout_transaction_id"
  end

  create_table "cashout_transactions", force: :cascade do |t|
    t.string "user_id"
    t.string "user_name"
    t.datetime "date"
    t.string "ref"
    t.float "amount"
    t.text "description"
    t.integer "status"
    t.string "office_id"
    t.string "created_by_id"
    t.string "updated_by_id"
    t.string "deleted_by_id"
    t.datetime "deleted_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["deleted_at"], name: "index_cashout_transactions_on_deleted_at"
  end

  create_table "companies", force: :cascade do |t|
    t.string "name"
    t.string "cif_number"
    t.string "phone_number"
    t.string "tax_number"
    t.string "city_id"
    t.string "city_name"
    t.text "address"
    t.string "branch_office_id"
    t.string "branch_office_name"
    t.boolean "status", default: false
    t.string "created_by_id"
    t.string "updated_by_id"
    t.string "deleted_by_id"
    t.datetime "deleted_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "area_code"
    t.index ["deleted_at"], name: "index_companies_on_deleted_at"
  end

  create_table "company_pics", force: :cascade do |t|
    t.string "name"
    t.integer "company_id"
    t.string "position"
    t.string "identity_number"
    t.string "phone_number"
    t.string "address"
    t.datetime "deleted_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "identity_type"
    t.index ["deleted_at"], name: "index_company_pics_on_deleted_at"
  end

  create_table "customer_bank_accounts", force: :cascade do |t|
    t.bigint "customer_id"
    t.string "account_number", null: false
    t.string "account_name", null: false
    t.string "bank_name", null: false
    t.string "branch_name"
    t.datetime "deleted_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "main_account", default: false
    t.index ["customer_id"], name: "index_customer_bank_accounts_on_customer_id"
    t.index ["deleted_at"], name: "index_customer_bank_accounts_on_deleted_at"
  end

  create_table "customer_contacts", force: :cascade do |t|
    t.bigint "customer_id"
    t.string "residence_address", null: false
    t.string "residence_province", null: false
    t.string "residence_city", null: false
    t.string "residence_region", null: false
    t.string "residence_postal_code", null: false
    t.string "identity_address", null: false
    t.string "identity_province", null: false
    t.string "identity_city", null: false
    t.string "identity_region", null: false
    t.string "identity_postal_code", null: false
    t.string "phone_number", null: false
    t.string "telephone_number"
    t.datetime "deleted_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "residence_subdistric"
    t.string "identity_subdistric"
    t.index ["customer_id"], name: "index_customer_contacts_on_customer_id"
    t.index ["deleted_at"], name: "index_customer_contacts_on_deleted_at"
  end

  create_table "customer_emergency_contacts", force: :cascade do |t|
    t.bigint "customer_id"
    t.string "name", null: false
    t.string "relation", null: false
    t.string "nationality", null: false
    t.string "address", null: false
    t.string "job_name"
    t.datetime "deleted_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "phone_number"
    t.string "birth_place"
    t.date "birth_date"
    t.index ["customer_id"], name: "index_customer_emergency_contacts_on_customer_id"
    t.index ["deleted_at"], name: "index_customer_emergency_contacts_on_deleted_at"
  end

  create_table "customer_jobs", force: :cascade do |t|
    t.bigint "customer_id"
    t.string "job_type", null: false
    t.string "company_name"
    t.string "customer_type", null: false
    t.string "tax_number"
    t.string "income_source", null: false
    t.string "disbursement_type", null: false
    t.string "financing_purpose", null: false
    t.integer "salary_amount", null: false
    t.datetime "deleted_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["customer_id"], name: "index_customer_jobs_on_customer_id"
    t.index ["deleted_at"], name: "index_customer_jobs_on_deleted_at"
  end

  create_table "customer_notes", force: :cascade do |t|
    t.bigint "customer_id"
    t.string "subject"
    t.text "note_description"
    t.string "created_by_id"
    t.string "updated_by_id"
    t.string "deleted_by_id"
    t.datetime "deleted_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["customer_id"], name: "index_customer_notes_on_customer_id"
    t.index ["deleted_at"], name: "index_customer_notes_on_deleted_at"
  end

  create_table "customers", force: :cascade do |t|
    t.string "cif_number", null: false
    t.string "branch_office_id", null: false
    t.string "branch_office_name"
    t.string "name", null: false
    t.string "degree", null: false
    t.string "identity_number", null: false
    t.integer "identity_type", null: false
    t.date "expired_date"
    t.date "birth_date", null: false
    t.string "birth_place", null: false
    t.string "id_scan_image"
    t.string "mother_name", null: false
    t.integer "gender"
    t.boolean "status", default: false
    t.string "created_by_id"
    t.string "updated_by_id"
    t.string "deleted_by_id"
    t.datetime "deleted_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "email"
    t.string "referral_code"
    t.integer "point"
    t.string "created_by_name"
    t.text "reason"
    t.integer "marital_status"
    t.index ["deleted_at"], name: "index_customers_on_deleted_at"
  end

  create_table "dashboards", force: :cascade do |t|
    t.string "office_id"
    t.date "date"
    t.string "month"
    t.integer "os_disbursement"
    t.integer "os_repayment"
    t.integer "os_auction"
    t.integer "os_prolongation"
    t.integer "os_cancelled"
    t.integer "os_installment"
    t.integer "os_dpd"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "installment_details", force: :cascade do |t|
    t.bigint "pawn_transaction_id"
    t.integer "tenor"
    t.integer "monthly_installment"
    t.float "monthly_interest"
    t.integer "monthly_fee"
    t.integer "remaining_loan"
    t.integer "installment_remaining_order"
    t.datetime "deleted_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "down_payment"
    t.index ["deleted_at"], name: "index_installment_details_on_deleted_at"
    t.index ["pawn_transaction_id"], name: "index_installment_details_on_pawn_transaction_id"
  end

  create_table "installment_items", force: :cascade do |t|
    t.bigint "pawn_transaction_id"
    t.integer "installment_order"
    t.date "due_date"
    t.date "payment_date"
    t.integer "payment_amount"
    t.integer "fine_amount"
    t.datetime "deleted_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "status", default: false
    t.index ["deleted_at"], name: "index_installment_items_on_deleted_at"
    t.index ["pawn_transaction_id"], name: "index_installment_items_on_pawn_transaction_id"
  end

  create_table "pawn_transactions", force: :cascade do |t|
    t.string "insurance_item_id"
    t.string "customer_id"
    t.string "company_id"
    t.string "sge"
    t.string "product_id"
    t.integer "status"
    t.integer "transaction_type"
    t.date "contract_date"
    t.date "due_date"
    t.date "auction_date"
    t.integer "loan_amount"
    t.integer "admin_fee"
    t.integer "monthly_fee"
    t.string "created_by_id"
    t.string "updated_by_id"
    t.string "deleted_by_id"
    t.datetime "deleted_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "insurance_item_name"
    t.float "slte_rate"
    t.integer "maximum_loan"
    t.float "maximum_loan_percentage"
    t.string "notes"
    t.string "office_id"
    t.integer "prolongation_period"
    t.date "prolongation_date"
    t.string "office_type"
    t.string "branch_id"
    t.string "area_id"
    t.string "region_id"
    t.string "head_id"
    t.integer "per_15_days_fee"
    t.integer "parent_id"
    t.string "product_name"
    t.float "fine_percentage"
    t.boolean "payment_status"
    t.float "payment_amount"
    t.integer "prolongation_order", default: 0
    t.date "repayment_date"
    t.string "parent_sge"
    t.integer "estimated_value", default: 0
    t.float "interest_rate"
    t.integer "is_option"
    t.index ["area_id"], name: "index_pawn_transactions_on_area_id"
    t.index ["branch_id"], name: "index_pawn_transactions_on_branch_id"
    t.index ["customer_id"], name: "index_pawn_transactions_on_customer_id"
    t.index ["deleted_at"], name: "index_pawn_transactions_on_deleted_at"
    t.index ["insurance_item_id"], name: "index_pawn_transactions_on_insurance_item_id"
    t.index ["office_id"], name: "index_pawn_transactions_on_office_id"
    t.index ["product_id"], name: "index_pawn_transactions_on_product_id"
    t.index ["region_id"], name: "index_pawn_transactions_on_region_id"
    t.index ["status"], name: "index_pawn_transactions_on_status"
    t.index ["transaction_type"], name: "index_pawn_transactions_on_transaction_type"
  end

  create_table "transaction_approvals", force: :cascade do |t|
    t.integer "pawn_transaction_id"
    t.integer "user_id"
    t.string "user_type"
    t.string "attachment"
    t.string "updated_by_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "transaction_deviations", force: :cascade do |t|
    t.bigint "pawn_transaction_id"
    t.integer "deviation_type"
    t.integer "office_type"
    t.string "office_id"
    t.datetime "deleted_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "status"
    t.string "rejected_reason"
    t.integer "value"
    t.index ["deleted_at"], name: "index_transaction_deviations_on_deleted_at"
    t.index ["deviation_type"], name: "index_transaction_deviations_on_deviation_type"
    t.index ["office_id"], name: "index_transaction_deviations_on_office_id"
    t.index ["pawn_transaction_id"], name: "index_transaction_deviations_on_pawn_transaction_id"
    t.index ["status"], name: "index_transaction_deviations_on_status"
  end

  create_table "transaction_insurance_items", force: :cascade do |t|
    t.string "name"
    t.string "product_insurance_item_id"
    t.string "ownership"
    t.integer "amount"
    t.float "carats"
    t.text "description"
    t.integer "pawn_transaction_id"
    t.integer "estimated_value"
    t.datetime "deleted_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "insurance_item_image"
    t.string "product_insurance_item_name"
    t.float "gross_weight"
    t.float "net_weight"
    t.integer "buy_price"
    t.index ["deleted_at"], name: "index_transaction_insurance_items_on_deleted_at"
  end

  create_table "transaction_point_rules", force: :cascade do |t|
    t.string "activity_name"
    t.integer "point_amount"
    t.date "expired_date"
    t.boolean "type"
    t.string "created_by_id"
    t.string "updated_by_id"
    t.string "deleted_by_id"
    t.datetime "deleted_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["deleted_at"], name: "index_transaction_point_rules_on_deleted_at"
  end

  create_table "transaction_points", force: :cascade do |t|
    t.bigint "customer_id"
    t.bigint "transaction_point_id"
    t.datetime "deleted_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["customer_id"], name: "index_transaction_points_on_customer_id"
    t.index ["deleted_at"], name: "index_transaction_points_on_deleted_at"
    t.index ["transaction_point_id"], name: "index_transaction_points_on_transaction_point_id"
  end

  create_table "transaction_rule_approvals", force: :cascade do |t|
    t.bigint "transaction_approval_id"
    t.string "user_role"
    t.datetime "deleted_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "transaction_rule_id"
    t.index ["deleted_at"], name: "index_transaction_rule_approvals_on_deleted_at"
    t.index ["transaction_approval_id"], name: "index_transaction_rule_approvals_on_transaction_approval_id"
  end

  create_table "transaction_rules", force: :cascade do |t|
    t.integer "minimum_amount"
    t.integer "maximum_amount"
    t.string "updated_by_id"
    t.string "deleted_by_id"
    t.datetime "deleted_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "created_by_id"
    t.index ["deleted_at"], name: "index_transaction_rules_on_deleted_at"
  end

  create_table "users", force: :cascade do |t|
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

end
