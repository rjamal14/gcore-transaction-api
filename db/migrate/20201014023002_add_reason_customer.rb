class AddReasonCustomer < ActiveRecord::Migration[6.0]
  def change
    add_column :customers, :reason, :text, null: true
  end
end
