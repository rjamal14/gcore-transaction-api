lock '~> 3.14.1'

set :application, 'gcda-transaction-api'
set :repo_url, 'git@bitbucket.org:c-aio/gcore-transaction-api.git'
set :deploy_to, '/home/deploy/apps/gcda-transaction-api'
append :linked_files, 'config/master.key', 'config/database.yml', '.env'
append :linked_dirs, 'public/uploads'
set :keep_releases, 5
set :rbenv_type, :user # or :system, or :fullstaq (for Fullstaq Ruby), depends on your rbenv setup
set :rbenv_ruby, '2.6.4'

namespace :puma do
  desc 'Create Directories for Puma Pids and Socket'
  task :make_dirs do
    on roles(:app) do
      execute "mkdir #{shared_path}/tmp/sockets -p"
      execute "mkdir #{shared_path}/tmp/pids -p"
    end
  end

  before :start, :make_dirs
end

namespace :deploy do
  desc 'Initial Deploy'
  task :initial do
    on roles(:app) do
      before 'deploy:restart', 'puma:start'
      invoke 'deploy'
    end
  end

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      invoke 'puma:restart'
    end
  end
end

namespace :db do
  desc 'Do DB migrate'
  task :migrate do
    on roles(:app) do
      within release_path do
        with rails_env: fetch(:rails_env) do
          execute :rake, 'db:migrate'
        end
      end
    end
  end
  task :setup do
    on roles(:app) do
      within release_path do
        with rails_env: fetch(:rails_env) do
          execute :rake, 'db:setup'
        end
      end
    end
  end
end
