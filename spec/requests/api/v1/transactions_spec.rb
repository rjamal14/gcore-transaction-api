require 'swagger_helper'

RSpec.describe 'api/v1/transactions', type: :request do
  path '/api/v1/transactions/num_of_transactions' do
    get('Number of Transactions') do
      tags 'Transaction'
      security [Bearer: {}]

      parameter name: :product_id, in: :query, type: :string
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/transactions/published' do
    get('Transaction Published') do
      tags 'Transaction'
      security [Bearer: {}]

      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/transactions/transaction_outstanding' do
    get('Transaction Outstanding') do
      tags 'Transaction'
      security [Bearer: {}]

      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/transactions/{id}/repayments' do
    # You'll want to customize the parameter types...
    parameter name: 'id', in: :path, type: :string, description: 'id'

    put('Create Repayment Transaction') do
      tags 'Transaction'
      security [Bearer: {}]

      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    get('Get Repayment Transaction') do
      tags 'Transaction'
      security [Bearer: {}]

      parameter name: 'type', in: :query, type: :string
      parameter name: 'page', in: :query, type: :string
      parameter name: 'per_page', in: :query, type: :string
      parameter name: 'order_by', in: :query, type: :string
      parameter name: 'order', in: :query, type: :string
      parameter name: 'search', in: :query, type: :string

      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/transactions/{id}/prolongations' do
    # You'll want to customize the parameter types...
    parameter name: 'id', in: :path, type: :string, description: 'id'

    put('Create Prolongation Transaction') do
      tags 'Transaction'
      security [Bearer: {}]

      parameter name: :transaction, in: :body, description: 'Transaction Parameter', schema: {
        type: :object,
        example: {
          "transaction": {
            "insurance_item_id": '5f6c1cead77fab0cf66c6ff8',
            "insurance_item_name": 'Logam Mulia',
            "customer_id": 104,
            "payment_amount": 1_000_000,
            "transaction_type": 'prolongation',
            "product_id": '5f851b20d77fab397da27fb7',
            "product_name": 'Gadai Reguler',
            "status": 'waiting_approval',
            "contract_date": '2020-09-17',
            "due_date": '2020-10-22',
            "auction_date": '2020-11-11',
            "loan_amount": 1_802_500,
            "maximum_loan_percentage": 82,
            "maximum_loan": 2_000_000,
            "admin_fee": 48_000,
            "monthly_fee": 21_840,
            "changed": true,
            "transaction_insurance_items_attributes":
            [{
              "product_insurance_item_id": '5f6c1cead77fab0cf66c6ff8',
              "name": 'Emas Bulat',
              "ownership": 'Warisan',
              "amount": 1,
              "gross_weight": 1.2,
              "net_weight": 1,
              "carats": 16,
              "estimated_value": 2_062_500,
              "description": 'Emas Dapat dari warisan',
              "insurance_item_image": 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBw0QDQ0NDQ0NDQ0NDQ0NDQ0NDQ8NDQ0NFREWFhURExMYHT4gGBoxGxUVITEhJSkrOi4uFx8zODMsNygtLisBCgoKDg0OFQ8PFS0ZFRkrKy0tLS0tKy03Ny0rKzc3LS0rNy0rNysrKysrNysrKysrKy0rKysrKysrKysrKysrK//AABEIANkA6QMBIgACEQEDEQH/xAAbAAACAwEBAQAAAAAAAAAAAAADBQIEBgEAB//EADAQAAICAgEDBAIBAgUFAAAAAAABAgMEESEFMVEGEkFhEyJxI6EUFTJCYhYzgZGx/8QAGgEBAQEBAQEBAAAAAAAAAAAAAQACAwQGBf/EAB8RAQEBAQABBQEBAAAAAAAAAAABEQISAxMhMVEiMv/aAAwDAQACEQMRAD8AVdG6WpfC7755Y8r6JHwiPp+vg0ddR8/6vq9eVevnmYT19Cj4RZr6BHwhzCsLFHL3ev1vxhLZ0GGvgo39Dj4Rp7ZcFG4fd6/V4wir6JH6LdXQY+EMKkXKhnq9fpnMKY9Aj4RKfQIa7IdI9Y+B93r9a8IymT0SK32KtHRo+40WUylCWmM9Xr9PhE8ToUPotS6BD6O0ZGvkvVZGx9zr9XhCuXp+PhB6vT0foc1LYeC0anqdC8Qqj6chrsjj9OQ+h2rD3vNe50z4Qgn6dh4RXn6ej4RpmyDiPudfq8IzcfT8fCDw9Ow+h4oE0PudC8whfp2H0Rfp6H0PpSA2WD7lZ8YQy6BD6Ix6DD6Gd15TszNfI+dHjHodAr+if/T9f0U59U18ljF6lv5Lz6XjHr/T8Pa+3ZiT/JY+ImwjbuEv4E2zrz3VeYU+nIcGmrgZ302uDTQR+f60/o8fScY8EZBV2ByOWN6BMrWRLjR78Q4VOuDLVUQkaQsaxkMCZCx8BZor2Mca1QySi1yMrY7Ks4GpCre9ovYdr+SnOISmWhxNLiWLQadiEdOTr5C/4r7NYDJ2ko2itXhYXCjSNiJxmhZ+Y48kQaStiBnkxFF+Z9lC7qH2MZp9PKj5K12XHRnreo/ZTs6l9iDrIykKsvJZTnm7+SvZfsWQcvJl8F/o903oWWLY46NX2FNbh7dcv4F+hvhV/wBN/wAC/wBh05FKPTcuDQ/mSMt6enwNbsjR5vV/0ufo0/xaISy0IbMz7BPN+zkWihlclmu5GYpy+e4yx8gWjxWImpi6FpYhYJidrKlgecgEhIEkBsDyAWCYq2g0TtBxFJoLHZCKLFcRT0UTU9E3Hgq3T0SHleBneUrLwE7xCxfa2L74thoz2FVexFJroPyUpxex3kUi++oQo+5nYWEL3oBXPkgYwY/6Q+xnKWP+lvsKbXDn/Tf8C38hYxbP6cv4FvvN8s1m+hX8f+xjfbszvRrH/wDRrKTOfqc/05834ck9sPVj7A1JuQ7wMfejHg1qvRhfQyoxhjViLQaNKReJ1TjXomuA1qivkqWWLyXi1KLsjIHG1eSbktD4nQJlexhrWVpMvEzoGxA0GkgUkXi1rqmWaplCTYemTHEYOXBRyCx7uCtaWJRsQCcS3NApQFI0xL1S4K9UC5WuCCnkIW5ERvfEX3QIEmRX3K1VXI1uqBU08kEsege9Oq7FLHqQ5woLgkb0R/py/gV6HFevxv8AgUbR05FZXolO/wC47jjfRU9P1bNFCgvU+3Hn6LqcTnsPMHH7cEaaFsa4taMF6yOoizKydfI4zV+v/gynVHLnRFDJ6j9irI6pz3KOY58ibJVuyMa3H6jv5GNeVtdzHYMLOB/jVz1yTWmbt2R2AjGQSJYZRUiSqBq1B68iHyR1FY2/g46tF2GXXoqZGZWR0KUgUgM8uO+D0bkyxan7TqqJVlquBFXjUFUQ0oaByZACcNleygvcHXFEiazG+gH4ND11IrZFBAs92i5i5Hbko5MGQobJNLHK/R8/Aq/xJD8z9j/gWfm+zfLNOvTdfCNLCoQ+meyNRBHXufLhKhXWXKUQggsWYw6jl/6TO59W9mhvfApyolh1m7sTfwVLOnb+DRus57F4DDKWYXTe3A6qwNR7BsbS+BjGa9pY0SW4+vgo5HA9yNMT5tfcsMJsjJ0ULOpa+Sxn47exHk4Un5CQrVnW9f7v7lefWt/7v7im/ps2+7IR6ZL7Gw6b19S2+43xMreuTM4/TZb+TQ9Pw2tGbDD/ABpdhlji7Fq0kNMeIF65FG2YwvQuuqBORmFhIFCoPXUSFhHZ6+rgPTALcuCTP5NBT/Fod5ERfdAkoXS1Fir3jPKr4Yp9hrlitd6ZfCNTUY305d2NbjT2ejufLhF+K4ONk0v12KszL9u+TOFctkUrkJsjrGt8i+7rn/IMTQOJB6MxLr3/ACB/57z/AKiwxr4S+w6u4MzidS93yOMe3aDGtXW9lfIq2FhIlIDKS5OLv4Kb6fv4H1kCMKwOkT6Rv4Of5P8ARpI1kvxlpZuHSeexdpwda4G34zqgFMVYUaDwjoL7SLRkoSiBlVsss4RChQGjSdiFTBIxgctXATZCwkoXxKVsBnZEq2QAlGTXwxP7DQ5Mf1Yl0b5c6N0Cw2fT59jEdBNlgS7Hq6+3CH8v+2zGddyGt6NfKf8ATZjeuR3szhYjP6jJN9xJl9Un9jjqFHLEOZT3GJWfVZ/Z6PVJ7XcEqAleNyuDXwWr6Hmyejd9NnuKMJ0SnWjc9OX6I50mdbDplOMiasM0rDSJRiir+UnC0ytXYxRL2IrxsCKZlqCexEXE97j2yKMkDkgjISMtINESbIgY6iaYNEiSezjI7ObIvOJWuiHcivbIEo5a/ViPQ7y3+rEezfLnXegVs12GtCD05TwaaqGj1dfbjPpec/6bMx1Z9x9bP9TN9Tn3MnGWz0tsQZrQ86lLuZfqNvcYUYOJYqS2KK7y7RaHSa7pUlwa3CuXtMF06/saXEyuO5hNB+ZHvyCuF5YhYBXHMnBgK2GiCWq5BYyKkZBYyMtLSkTTK8ZE0wQrZFnNnDLTzIs6yDZFI4c2e2Be2cbPEWSjjYC1hpFe0Cp5T/ViTY4yn+rEuzfLnWj9M1cD+dYt9K18D26s9fX258/RZfHhiLPo3s0d0Shfj7MtMN1LD7mS6phvk+n9QwuHwZTqeD34GXGWIowmMsfAY1xsD6G2N076Dq6SzExGh1jVPSD14evgswp0YSFcGW6kRhAsVRM1D1IPohUg2gTmicSOiaMkSIRMEiSYETZ7ZDZxsCns5sjs8BiR4imdCtOHGdIyBRFle1hmytayKnlP9WJdjfKfDE2zfLnW39KS4Q8vkZ70rLhD26R6u78jnn4VZvkj7Nnm+SzVFGNPiW5uPx2Mz1HE78G6yaloRZuOuS1Yy+LhfQ3xsL6LeLjIbY+PHQWjCOWLr4BOoe5FKF9sEZ1Yo+w7ALNAwtA8JBlIqJhq2GpYRJI5AmgTyOnkeAvHjx4C8jx46FMePbPEZA05KQOUzk5FeyZKJysK9lgOywDKYFDJlwxPsaXv9WKdm+axWz9Ly4HtrM96XfA9sPT39nn6Bb5DVTK8o8k4mCPkW8CjLmXrm9C3IiyDmPMY02iqqDLdewrIl9ovtsDXbKc09gnpSIbPaPe0KzUkFrYHQWBJbrYQBBhIsyhEzpFMkgLx3R1HQKOjujpzYUuSYKbJyYGxgQrJFS2Ya1lO3ZGBWWA/eDtTOQiyIlr/AFYqGso/qxb7DXLFav0w+DRtGZ9My4RpoSPT6l+WuJ8Iusi4FjfBXsmY02BzRWsq2FdxH8paAVUTUD0rQcrw0VC2BWlAPK4E7A1kP2HnAJ7zkpBaKC0eienIj7g0DRYSLARYWLM6hkEiCiwkWWkRHiOzzYanWRbOORByDWo7JgpM62RYECwrWIuSiClWRL7IHoQLcqSUKS1ASr/Viv8AGaCdX6v+BR+P6N8s0w9NzWlyjTVzXlGK9PmjrPV3Pk8X4OXNa7ooZNv2gb7FDJMY1ojv57nVcvIqfckixnTC29eSpZkryVrCpaZsFpksheSSt+xTEIixnTaNi8o7Ka8oWwJS7FYLVqc15RyM15KMziDBppGS8hYSXlCuASBnFprGS8oIpLyhZEIiw6Y+5eSDmvKKTIMPErrmvKIOxeSlIGwwww968o6pLyhejsQw6ZLXlHfavKKMScR8Vq3+JfRONS8opxJxCcrVq2C9r7CX2ryi/b/pYoOnMFr/2Q=='
            }]
          }
        }
      }
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    get('Get Prolongation Transaction') do
      tags 'Transaction'
      security [Bearer: {}]

      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/transactions/{id}/contract' do
    # You'll want to customize the parameter types...
    parameter name: 'id', in: :path, type: :string, description: 'id'

    get('Get Contract') do
      tags 'Transaction'
      security [Bearer: {}]

      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/transactions/{id}/print_repayment' do
    # You'll want to customize the parameter types...
    parameter name: 'id', in: :path, type: :string, description: 'id'

    get('print_repayment transaction') do
      tags 'Transaction'
      security [Bearer: {}]

      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/transactions/{id}/print_disbursement' do
    # You'll want to customize the parameter types...
    parameter name: 'id', in: :path, type: :string, description: 'id'

    get('print_disbursement transaction') do
      tags 'Transaction'
      security [Bearer: {}]

      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/transactions' do
    get('list transactions') do
      tags 'Transaction'
      security [Bearer: {}]

      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    post('Create Transaction') do
      tags 'Transaction'
      security [Bearer: {}]

      parameter name: :transaction, in: :body, description: 'Transaction Parameter', schema: {
        type: :object,
        example: {
          "transaction": {
            "insurance_item_id": '5f6c1cead77fab0cf66c6ff8',
            "insurance_item_name": 'Logam Mulia',
            "customer_id": 112,
            "transaction_type": 'disbursement',
            "product_id": '5f851b20d77fab397da27fb7',
            "product_name": 'Gadai Reguler',
            "status": 'waiting_approval',
            "contract_date": '2020-09-17',
            "due_date": '2020-10-22',
            "auction_date": '2020-11-11',
            "loan_amount": 1_802_500,
            "maximum_loan_percentage": 82,
            "maximum_loan": 2_000_000,
            "admin_fee": 48_000,
            "monthly_fee": 21_840,
            "transaction_insurance_items_attributes":
            [{
              "product_insurance_item_id": '5f6c1cead77fab0cf66c6ff8',
              "name": 'Emas Bulat',
              "ownership": 'Warisan',
              "amount": 1,
              "gross_weight": 1.2,
              "net_weight": 1,
              "carats": 16,
              "estimated_value": 2_062_500,
              "description": 'Emas Dapat dari warisan',
              "insurance_item_image": 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBw0QDQ0NDQ0NDQ0NDQ0NDQ0NDQ8NDQ0NFREWFhURExMYHT4gGBoxGxUVITEhJSkrOi4uFx8zODMsNygtLisBCgoKDg0OFQ8PFS0ZFRkrKy0tLS0tKy03Ny0rKzc3LS0rNy0rNysrKysrNysrKysrKy0rKysrKysrKysrKysrK//AABEIANkA6QMBIgACEQEDEQH/xAAbAAACAwEBAQAAAAAAAAAAAAADBQIEBgEAB//EADAQAAICAgEDBAIBAgUFAAAAAAABAgMEESEFMVEGEkFhEyJxI6EUFTJCYhYzgZGx/8QAGgEBAQEBAQEBAAAAAAAAAAAAAQACAwQGBf/EAB8RAQEBAQABBQEBAAAAAAAAAAABEQISAxMhMVEiMv/aAAwDAQACEQMRAD8AVdG6WpfC7755Y8r6JHwiPp+vg0ddR8/6vq9eVevnmYT19Cj4RZr6BHwhzCsLFHL3ev1vxhLZ0GGvgo39Dj4Rp7ZcFG4fd6/V4wir6JH6LdXQY+EMKkXKhnq9fpnMKY9Aj4RKfQIa7IdI9Y+B93r9a8IymT0SK32KtHRo+40WUylCWmM9Xr9PhE8ToUPotS6BD6O0ZGvkvVZGx9zr9XhCuXp+PhB6vT0foc1LYeC0anqdC8Qqj6chrsjj9OQ+h2rD3vNe50z4Qgn6dh4RXn6ej4RpmyDiPudfq8IzcfT8fCDw9Ow+h4oE0PudC8whfp2H0Rfp6H0PpSA2WD7lZ8YQy6BD6Ix6DD6Gd15TszNfI+dHjHodAr+if/T9f0U59U18ljF6lv5Lz6XjHr/T8Pa+3ZiT/JY+ImwjbuEv4E2zrz3VeYU+nIcGmrgZ302uDTQR+f60/o8fScY8EZBV2ByOWN6BMrWRLjR78Q4VOuDLVUQkaQsaxkMCZCx8BZor2Mca1QySi1yMrY7Ks4GpCre9ovYdr+SnOISmWhxNLiWLQadiEdOTr5C/4r7NYDJ2ko2itXhYXCjSNiJxmhZ+Y48kQaStiBnkxFF+Z9lC7qH2MZp9PKj5K12XHRnreo/ZTs6l9iDrIykKsvJZTnm7+SvZfsWQcvJl8F/o903oWWLY46NX2FNbh7dcv4F+hvhV/wBN/wAC/wBh05FKPTcuDQ/mSMt6enwNbsjR5vV/0ufo0/xaISy0IbMz7BPN+zkWihlclmu5GYpy+e4yx8gWjxWImpi6FpYhYJidrKlgecgEhIEkBsDyAWCYq2g0TtBxFJoLHZCKLFcRT0UTU9E3Hgq3T0SHleBneUrLwE7xCxfa2L74thoz2FVexFJroPyUpxex3kUi++oQo+5nYWEL3oBXPkgYwY/6Q+xnKWP+lvsKbXDn/Tf8C38hYxbP6cv4FvvN8s1m+hX8f+xjfbszvRrH/wDRrKTOfqc/05834ck9sPVj7A1JuQ7wMfejHg1qvRhfQyoxhjViLQaNKReJ1TjXomuA1qivkqWWLyXi1KLsjIHG1eSbktD4nQJlexhrWVpMvEzoGxA0GkgUkXi1rqmWaplCTYemTHEYOXBRyCx7uCtaWJRsQCcS3NApQFI0xL1S4K9UC5WuCCnkIW5ERvfEX3QIEmRX3K1VXI1uqBU08kEsege9Oq7FLHqQ5woLgkb0R/py/gV6HFevxv8AgUbR05FZXolO/wC47jjfRU9P1bNFCgvU+3Hn6LqcTnsPMHH7cEaaFsa4taMF6yOoizKydfI4zV+v/gynVHLnRFDJ6j9irI6pz3KOY58ibJVuyMa3H6jv5GNeVtdzHYMLOB/jVz1yTWmbt2R2AjGQSJYZRUiSqBq1B68iHyR1FY2/g46tF2GXXoqZGZWR0KUgUgM8uO+D0bkyxan7TqqJVlquBFXjUFUQ0oaByZACcNleygvcHXFEiazG+gH4ND11IrZFBAs92i5i5Hbko5MGQobJNLHK/R8/Aq/xJD8z9j/gWfm+zfLNOvTdfCNLCoQ+meyNRBHXufLhKhXWXKUQggsWYw6jl/6TO59W9mhvfApyolh1m7sTfwVLOnb+DRus57F4DDKWYXTe3A6qwNR7BsbS+BjGa9pY0SW4+vgo5HA9yNMT5tfcsMJsjJ0ULOpa+Sxn47exHk4Un5CQrVnW9f7v7lefWt/7v7im/ps2+7IR6ZL7Gw6b19S2+43xMreuTM4/TZb+TQ9Pw2tGbDD/ABpdhlji7Fq0kNMeIF65FG2YwvQuuqBORmFhIFCoPXUSFhHZ6+rgPTALcuCTP5NBT/Fod5ERfdAkoXS1Fir3jPKr4Yp9hrlitd6ZfCNTUY305d2NbjT2ejufLhF+K4ONk0v12KszL9u+TOFctkUrkJsjrGt8i+7rn/IMTQOJB6MxLr3/ACB/57z/AKiwxr4S+w6u4MzidS93yOMe3aDGtXW9lfIq2FhIlIDKS5OLv4Kb6fv4H1kCMKwOkT6Rv4Of5P8ARpI1kvxlpZuHSeexdpwda4G34zqgFMVYUaDwjoL7SLRkoSiBlVsss4RChQGjSdiFTBIxgctXATZCwkoXxKVsBnZEq2QAlGTXwxP7DQ5Mf1Yl0b5c6N0Cw2fT59jEdBNlgS7Hq6+3CH8v+2zGddyGt6NfKf8ATZjeuR3szhYjP6jJN9xJl9Un9jjqFHLEOZT3GJWfVZ/Z6PVJ7XcEqAleNyuDXwWr6Hmyejd9NnuKMJ0SnWjc9OX6I50mdbDplOMiasM0rDSJRiir+UnC0ytXYxRL2IrxsCKZlqCexEXE97j2yKMkDkgjISMtINESbIgY6iaYNEiSezjI7ObIvOJWuiHcivbIEo5a/ViPQ7y3+rEezfLnXegVs12GtCD05TwaaqGj1dfbjPpec/6bMx1Z9x9bP9TN9Tn3MnGWz0tsQZrQ86lLuZfqNvcYUYOJYqS2KK7y7RaHSa7pUlwa3CuXtMF06/saXEyuO5hNB+ZHvyCuF5YhYBXHMnBgK2GiCWq5BYyKkZBYyMtLSkTTK8ZE0wQrZFnNnDLTzIs6yDZFI4c2e2Be2cbPEWSjjYC1hpFe0Cp5T/ViTY4yn+rEuzfLnWj9M1cD+dYt9K18D26s9fX258/RZfHhiLPo3s0d0Shfj7MtMN1LD7mS6phvk+n9QwuHwZTqeD34GXGWIowmMsfAY1xsD6G2N076Dq6SzExGh1jVPSD14evgswp0YSFcGW6kRhAsVRM1D1IPohUg2gTmicSOiaMkSIRMEiSYETZ7ZDZxsCns5sjs8BiR4imdCtOHGdIyBRFle1hmytayKnlP9WJdjfKfDE2zfLnW39KS4Q8vkZ70rLhD26R6u78jnn4VZvkj7Nnm+SzVFGNPiW5uPx2Mz1HE78G6yaloRZuOuS1Yy+LhfQ3xsL6LeLjIbY+PHQWjCOWLr4BOoe5FKF9sEZ1Yo+w7ALNAwtA8JBlIqJhq2GpYRJI5AmgTyOnkeAvHjx4C8jx46FMePbPEZA05KQOUzk5FeyZKJysK9lgOywDKYFDJlwxPsaXv9WKdm+axWz9Ly4HtrM96XfA9sPT39nn6Bb5DVTK8o8k4mCPkW8CjLmXrm9C3IiyDmPMY02iqqDLdewrIl9ovtsDXbKc09gnpSIbPaPe0KzUkFrYHQWBJbrYQBBhIsyhEzpFMkgLx3R1HQKOjujpzYUuSYKbJyYGxgQrJFS2Ya1lO3ZGBWWA/eDtTOQiyIlr/AFYqGso/qxb7DXLFav0w+DRtGZ9My4RpoSPT6l+WuJ8Iusi4FjfBXsmY02BzRWsq2FdxH8paAVUTUD0rQcrw0VC2BWlAPK4E7A1kP2HnAJ7zkpBaKC0eienIj7g0DRYSLARYWLM6hkEiCiwkWWkRHiOzzYanWRbOORByDWo7JgpM62RYECwrWIuSiClWRL7IHoQLcqSUKS1ASr/Viv8AGaCdX6v+BR+P6N8s0w9NzWlyjTVzXlGK9PmjrPV3Pk8X4OXNa7ooZNv2gb7FDJMY1ojv57nVcvIqfckixnTC29eSpZkryVrCpaZsFpksheSSt+xTEIixnTaNi8o7Ka8oWwJS7FYLVqc15RyM15KMziDBppGS8hYSXlCuASBnFprGS8oIpLyhZEIiw6Y+5eSDmvKKTIMPErrmvKIOxeSlIGwwww968o6pLyhejsQw6ZLXlHfavKKMScR8Vq3+JfRONS8opxJxCcrVq2C9r7CX2ryi/b/pYoOnMFr/2Q=='
            }]
          }
        }
      }
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/transactions/{id}' do
    # You'll want to customize the parameter types...
    parameter name: 'id', in: :path, type: :string, description: 'id'

    get('Get Transaction') do
      tags 'Transaction'
      security [Bearer: {}]

      parameter name: :transaction, in: :body, description: 'Transaction Parameter', schema: {
        type: :object,
        example: {
          "status": 'approved'
        }
      }
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    patch('update transaction') do
      tags 'Transaction'
      security [Bearer: {}]

      parameter name: :transaction, in: :body, description: 'Transaction Parameter', schema: {
        type: :object,
        example: {
          "status": 'published'
        }
      }
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    put('Update Transaction') do
      tags 'Transaction'
      security [Bearer: {}]

      parameter name: :transaction, in: :body, description: 'Transaction Parameter', schema: {
        type: :object,
        example: {
          "status": 'published'
        }
      }
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    delete('delete transaction') do
      tags 'Transaction'
      security [Bearer: {}]

      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end
end
