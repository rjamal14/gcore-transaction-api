# Transaction Insurance Item Model
class TransactionApproval < ApplicationRecord
  mount_base64_uploader :attachment, TransactionApprovalUploader
  after_update :update_dashboard

  validates :user_id, presence: true, on: :update
  validates :updated_by_id, presence: true, on: :update
  validates_presence_of :pawn_transaction, :user_type

  belongs_to :pawn_transaction, optional: true

  def update_dashboard
    trans = pawn_transaction

    if trans.present?
      dashboards = Dashboard.find_or_create_by(date: trans.contract_date)
      dashboards.update_attributes({
                                     "os_#{trans.transaction_type}" => dashboards["os_#{trans.transaction_type}"].to_i + trans.loan_amount.to_i,
                                     'month' => trans.contract_date.strftime('%Y-%m'),
                                     'office_id' => trans.office_id
                                   })
    end
  end
end
