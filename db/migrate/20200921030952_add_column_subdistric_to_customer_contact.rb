class AddColumnSubdistricToCustomerContact < ActiveRecord::Migration[6.0]
  def change
    add_column :customer_contacts, :residence_subdistric, :string
    add_column :customer_contacts, :identity_subdistric, :string
  end
end
