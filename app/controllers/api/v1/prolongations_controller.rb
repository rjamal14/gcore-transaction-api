# Prolongations Controller
module Api
  module V1
    # Prolongations  Controller
    class ProlongationsController < TransactionsController
      before_action :set_pawn_transaction, only: :create
      before_action :check_type_of_transaction, only: [:create, :inquiry]
      after_action :pub_journal, only: :create
      skip_before_action :set_deviation, only: :create

      # This is not an Actual Create function, this is Update but create new Transaction Intance
      def create
        @pawn_transaction.payment_amount = pawn_transaction_params[:payment_amount]

        transaction_temp = @pawn_transaction
        cashier_status = check_cashier_status(type: 'debit', balance: @pawn_transaction.payment_amount)
        return true if cashier_status.present?

        transaction_temp.transaction_type = 'prolongation'
        return response_bad('Tranasksi Tidak bisa Diperpanjang') unless @type_of_transaction && @pawn_transaction.status == 'published'

        transaction_params = if pawn_transaction_params[:changed] == true
                               Transaction::TransactionApprovalCheckService.new(transaction: pawn_transaction_params).execute
                             else
                               update_dashboard(pawn_transaction_params)
                               pawn_transaction_params
                             end
        transaction_params[:status] = 'approved' if pawn_transaction_params[:changed] == false
        @payment_amount = transaction_params[:payment_amount]
        transaction_params[:payment_amount] = 0
        transaction_params[:transaction_type] = 'prolongation'
        transaction_params = merge_transaction_params(transaction_params)
        prolongation_params = { transaction_type: :repayment,
                                prolongation_attributes: transaction_params,
                                payment_status: true,
                                payment_amount: @payment_amount,
                                updated_by_id: current_user.id,
                                repayment_date: Date.today }
        if @pawn_transaction.update(prolongation_params)
          send_notification unless @pawn_transaction.status == 'published'
          response_updated(@pawn_transaction.prolongation, 'Data Perpanjangan Terupdate')
        else
          response_error(@pawn_transaction, 'Data Perpanjangan Gagal Terupdate')
        end
      end

      def inquiry
        return response_bad('Tranasksi Tidak bisa Diperpanjang') unless @type_of_transaction && @pawn_transaction.status == 'published'

        prolongation_bill = Transaction::TransactionBillService.new(id: params[:id]).prolongation_bill
        response_success(prolongation_bill, 'Data sukses')
      rescue StandardError => e
        response_bad(e)
      end

      private

      def merge_transaction_params(params)
        params.merge(created_by_id: current_user.id,
                     sge: generate_number('sge'),
                     office_id: current_user.office_id,
                     office_type: current_user.office_type,
                     branch_id: current_user.branch,
                     area_id: current_user.area,
                     region_id: current_user.region,
                     head_id: current_user.head,
                     parent_sge: @pawn_transaction.sge,
                     prolongation_order: (@pawn_transaction.prolongation_order + 1))
      end

      def pub_journal
        publish_cashier = Cashier::PubCashierProlongationService.new(data: @pawn_transaction.prolongation)
        publish_cashier.execute(headers: { Authorization: request.headers['Authorization'] }) if @pawn_transaction.status == 'published'
        return true if %w[minus close].include?(publish_cashier)
      end
    end
  end
end
