class TransactionRuleApproval < ApplicationRecord
  acts_as_paranoid
  validates_presence_of :user_role
  belongs_to :transaction_rule, dependent: :destroy
end
