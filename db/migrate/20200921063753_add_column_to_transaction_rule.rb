class AddColumnToTransactionRule < ActiveRecord::Migration[6.0]
  def change
    add_column :transaction_rules, :created_by_id, :string
  end
end
