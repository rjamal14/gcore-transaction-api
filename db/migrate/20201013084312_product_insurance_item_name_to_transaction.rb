class ProductInsuranceItemNameToTransaction < ActiveRecord::Migration[6.0]
  def change
    add_column :transaction_insurance_items, :product_insurance_item_name, :string
  end
end
