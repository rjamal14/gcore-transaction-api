module Api
  module V1
    class DeviationsController < ApplicationController
      before_action :set_deviation, only: [:show, :approve, :reject]
      before_action :is_valid_deviation, only: [:approve, :reject]
      before_action :is_unit

      def index
        return response_bad('User Tidak Valid') unless is_valid_user

        deviations = TransactionDeviation.where(office_id: current_user.office_id)
        deviations = deviations.search(params[:search]) if params.key?(:search)
        deviations = deviations.page(params[:page] || 1).per(params[:per_page] || 10)
                               .order(status: :asc, created_at: :desc)
        serial_deviations = deviations.map do |deviation|
          DeviationSerializer.new(deviation, root: false)
        end

        response_index(deviations, serial_deviations)
      rescue StandardError => e
        response_bad(e)
      end

      def show
        transaction = @deviation.pawn_transaction
        response_success(TransactionDetailDeviationSerializer.new(transaction, root: false), 'Data Transaksi Ditemukan')
      end

      def approve
        response_ok("Approval Deviasi #{@deviation.pawn_transaction.sge} berhasil") if
          @deviation.update(status: :approved)
      end

      def reject
        deviation = Transaction::RevertDeviationService.new(@deviation, transaction_deviation_params[:rejected_reason])
        response_ok("Deviasi #{@deviation.pawn_transaction.sge} Ditolak") if deviation.execute
      end

      private

      def is_unit
        response_bad('User Unit Tidak Berhak Mengakses Approval Deviasi') if current_user.office_type == 'unit'
      end

      def is_valid_deviation
        return response_bad('Deviasi Sudah Pernah Disetujui/Ditolak') if %w[approved rejected].include?(@deviation.status)

        response_bad('User Tidak Di ijinkan melakukan approval') unless current_user.office_id == @deviation.office_id && is_valid_user
      end

      def transaction
        @deviation.transaction
      end

      def is_valid_user
        current_user.user_role['role_name'] == 'KU' || current_user.office_type == 'head'
      end

      def set_deviation
        @deviation = TransactionDeviation.find(params[:id])
      rescue ActiveRecord::RecordNotFound
        response_bad('Data Deviasi tidak ditemukan')
      end

      def transaction_deviation_params
        params.require(:transaction_deviation).permit(:rejected_reason)
      end
    end
  end
end
