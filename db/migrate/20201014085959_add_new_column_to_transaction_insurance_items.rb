class AddNewColumnToTransactionInsuranceItems < ActiveRecord::Migration[6.0]
  def change
    add_column :transaction_insurance_items, :gross_weight, :float
    add_column :transaction_insurance_items, :net_weight, :float
    remove_column :transaction_insurance_items, :weight
  end
end
