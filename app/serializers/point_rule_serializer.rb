# CustomerSerializer
class PointRuleSerializer < ActiveModel::Serializer
  attributes :id, :cif_number, :name, :branch_office_name, :os, :status, :phone, :sge, :email, :city
  def os
    object.pawn_transactions.sum(:loan_amount)
  end

  def phone
    object.customer_contact.phone_number
  end

  def city
    object.customer_contact.residence_city
  end

  def sge
    object.pawn_transactions.count
  end
end
