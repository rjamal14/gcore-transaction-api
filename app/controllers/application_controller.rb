# APP COntroller
class ApplicationController < ActionController::Base
  before_action :current_user
  skip_before_action :verify_authenticity_token
  before_action :set_notification
  include ActionController::MimeResponds
  include ActionController::Serialization
  include Response
  include Converter

  def auth
    @auth ||= Auth::AuthorizeService.new
  end

  def current_user
    render json: { message: 'Unauthorize' }, status: :unauthorized unless request.headers['Authorization'].present?

    auth.set_token(request.headers['Authorization'])
    @current_user ||= auth.current_user

    response_auth_failed if @current_user.nil?
    @current_user
  end

  def get_master(url)
    auth.get_master(url)
  end

  def generate_number(type)
    auth.get_office("number_formats/generate?type=#{type}&office_id=#{current_user.office_id}&office_type=#{current_user.office_type}")['data']
  end

  def check_cashier_status(balance:, type:, _office_id: current_user.office_id)
    res = Cashier::CheckCashierStatusService.new(balance: balance, type: type, office_id: _office_id).execute

    return unless %w[minus close last_error unapproved].include?(res)

    response_message = case res
                       when 'close'
                         'Kasir tutup'
                       when 'minus'
                         'Saldo Kasir Kurang'
                       when 'last_error'
                         'Kasir Kemarin Belum Tertutup'
                       when 'unapproved'
                         'Pembukaan Kasir Belum Disetujui'
                       else
                         'Kesalahan Sistem'
                       end
    response_bad(response_message)
  end

  def check_cashier_report(type, data)
    cashier_report = Cashier::PublishCashierReportService.new(data: data)
    res = cashier_report.execute(type: type, headers: { Authorization: request.headers['Authorization'] })

    return unless %w[minus close].include?(res)

    response_message = res == 'close' ? 'Kasir Tutup' : 'Saldo Kurang'
    response_bad(response_message)
  end

  def set_notification
    request.env['exception_notifier.exception_data'] = { 'server' => request.env['SERVER_NAME'] }
  end
end
