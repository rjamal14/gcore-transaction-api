class EditColumnInTransactionRentalCost < ActiveRecord::Migration[6.0]
  def change
    add_column :pawn_transactions, :per_15_days_fee, :integer
  end
end
