class AddColumnOfficeTypeTransaction < ActiveRecord::Migration[6.0]
  def change
    add_column :pawn_transactions, :office_type, :string
  end
end
