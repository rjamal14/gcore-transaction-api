class AddParentSgEtoPawnTransactions < ActiveRecord::Migration[6.0]
  def change
    add_column :pawn_transactions, :parent_sge, :string
  end
end
