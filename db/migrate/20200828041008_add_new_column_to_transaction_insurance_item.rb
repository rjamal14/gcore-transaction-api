class AddNewColumnToTransactionInsuranceItem < ActiveRecord::Migration[6.0]
  def change
    add_column :transaction_insurance_items, :insurance_item_image, :string
  end
end
