# Transaction Insurance Item Model
class TransactionInsuranceItem < ApplicationRecord
  acts_as_paranoid

  attribute :pawn_transaction_id, :integer
  attribute :product_insurance_item_id, :string
  attribute :product_insurance_item_name, :string
  attribute :name, :string
  attribute :ownership, :string
  attribute :amount, :integer
  attribute :carats, :float
  attribute :weight, :float
  attribute :description, :text
  attribute :estimated_value, :integer
  attribute :insurance_item_image

  mount_base64_uploader :insurance_item_image, TransactionInsuranceItemUploader

  belongs_to :pawn_transaction

  validates_presence_of :pawn_transaction, :product_insurance_item_id, :name, :ownership, :amount, :carats, :gross_weight,
                        :description, :net_weight
end
