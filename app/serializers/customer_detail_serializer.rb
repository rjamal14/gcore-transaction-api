# Customer Detail
class CustomerDetailSerializer < ActiveModel::Serializer
  attributes :id, :cif_number, :name, :branch_office_name, :status, :identity_type, :identity_number, :id_scan_image, :expired_date,
             :gender, :birth_date, :birth_place, :created_by_id, :updated_by_id, :customer_contact_data, :mother_name, :email,
             :marital_status, :degree, :customer_job, :customer_emergency_contacts, :customer_notes, :customer_bank_accounts,
             :referral_code, :transaction_history, :active_transaction, :transaction_points, :created_at, :created_by_name
  def active_transaction
    object.pawn_transactions.where.not(status: [:approved, :waiting_approval]).map { |x| TransactionIndexSerializer.new(x) }
  end

  def transaction_history
    object.pawn_transactions.map { |x| TransactionIndexSerializer.new(x) }
  end

  def customer_contact_data
    {
      id: object.try(:customer_contact).try(:id),
      residence_address: object.try(:customer_contact).try(:residence_address),
      residence_province: object.try(:customer_contact).try(:residence_province),
      residence_city: object.try(:customer_contact).try(:residence_city),
      residence_region: object.try(:customer_contact).try(:residence_region),
      residence_subdistric: object.try(:customer_contact).try(:residence_subdistric),
      residence_province_name: object.try(:customer_contact).try(:residence_province_name),
      residence_city_name: object.try(:customer_contact).try(:residence_city_name),
      residence_region_name: object.try(:customer_contact).try(:residence_region_name),
      residence_subdistric_name: object.try(:customer_contact).try(:residence_subdistric_name),
      residence_postal_code: object.try(:customer_contact).try(:residence_postal_code),
      identity_address: object.try(:customer_contact).try(:identity_address),
      identity_province: object.try(:customer_contact).try(:identity_province),
      identity_city: object.try(:customer_contact).try(:identity_city),
      identity_region: object.try(:customer_contact).try(:identity_region),
      identity_subdistric: object.try(:customer_contact).try(:identity_subdistric),
      identity_province_name: object.try(:customer_contact).try(:identity_province_name),
      identity_city_name: object.try(:customer_contact).try(:identity_city_name),
      identity_region_name: object.try(:customer_contact).try(:identity_region_name),
      identity_subdistric_name: object.try(:customer_contact).try(:identity_subdistric_name),
      identity_postal_code: object.try(:customer_contact).try(:identity_postal_code),
      phone_number: object.try(:customer_contact).try(:phone_number),
      telephone_number: object.try(:customer_contact).telephone_number,
      deleted_at: object.try(:customer_contact).try(:deleted_at),
      created_at: object.try(:customer_contact).try(:created_at),
      updated_at: object.try(:customer_contact).try(:updated_at)
    }
  end
end
