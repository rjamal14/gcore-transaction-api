# Transaction Insurance Item Serializer
class TransactionInsuranceItemSerializer < ActiveModel::Serializer
  attributes :id, :name, :product_insurance_item_id, :ownership, :insurance_item_image, :amount, :net_weight, :gross_weight, :carats, :description,
             :pawn_transaction_id, :estimated_value, :buy_price, :product_insurance_item_name
end
