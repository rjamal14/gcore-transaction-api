module Cashier
  class PubCashierDisbursementService
    def initialize(data:)
      @data = data
    end

    def payload
      {
        id: @data.id,
        office_id: @data.office_id,
        sge: @data.sge,
        balance: @data.loan_amount,
        admin_fee: @data.admin_fee,
        description: "Pencairan SGE : #{@data.sge}",
        monthly_fee: @data.monthly_fee,
        contract_date: @data.contract_date
      }
    end

    def execute(headers:)
      RestClient.post "#{ENV['API_URL_ACCOUNTING']}/api/v1/cashiers/transaction/disbursement",
                      payload, headers
    rescue RestClient::NotFound
      'close'
    rescue RestClient::BadRequest
      'minus'
    end
  end
end
