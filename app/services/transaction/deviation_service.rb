# Deviation
module Transaction
  # Deviation Service
  class DeviationService
    def initialize(transaction:, deviation:, user:)
      @transaction = transaction
      @customer = Customer.find(transaction[:customer_id])
      @ltv = deviation['data']['ltv']
      @admin = deviation['data']['admin']
      @rental = deviation['data']['rental']
      @insurance_item = deviation['data']['insurance_item']
      @one_obligor = deviation['data']['one_obligor']
      @user = user
      @deviation_item = []
      @deviation_item_tmp = []
      @active_os = PawnTransaction.active_transaction.where(customer_id: transaction[:customer_id]).sum(:loan_amount)
    end

    def deviation_obj(type, office_type, value)
      {
        deviation_type: type,
        office_type: office_type,
        office_id: eval("@user.#{office_type}"),
        value: value
      }
    end

    def deviation_ltv
      case @transaction[:maximum_loan_percentage]
      when 0..@ltv['base']
        nil
      when @ltv['base']..@ltv['branch']
        @deviation_item.push(deviation_obj('ltv', 'branch', @ltv['base']))
      when @ltv['branch']..@ltv['area']
        @deviation_item.push(deviation_obj('ltv', 'area', @ltv['base']))
      when @ltv['area']..@ltv['region']
        @deviation_item.push(deviation_obj('ltv', 'region', @ltv['base']))
      when @ltv['region']..@ltv['head']
        @deviation_item.push(deviation_obj('ltv', 'head', @ltv['base']))
      else
        raise StandardError, 'Deviasi Rasio Pinjaman Terlalu Tinggi'
      end
    end

    def deviation_one_obligor
      case @transaction[:loan_amount].to_i + @customer.pawn_transactions.active_transaction.sum(:loan_amount)
      when 0..@one_obligor['base']
        nil
      when @one_obligor['base']..@one_obligor['branch']
        @deviation_item.push(deviation_obj('one_obligor', 'branch', @one_obligor['base']))
      when @one_obligor['branch']..@one_obligor['area']
        @deviation_item.push(deviation_obj('one_obligor', 'area', @one_obligor['base']))
      when @one_obligor['area']..@one_obligor['region']
        @deviation_item.push(deviation_obj('one_obligor', 'region', @one_obligor['base']))
      when @one_obligor['region']..@one_obligor['head']
        @deviation_item.push(deviation_obj('one_obligor', 'head', @one_obligor['base']))
      else
        raise StandardError, 'Deviasi One Obligor Terlalu Tinggi'
      end
    end

    def deviation_admin
      val_admin = @admin['base'] - @transaction[:admin_fee]
      case val_admin
      when -Float::INFINITY..0
        nil
      when 1..@admin['branch']
        @deviation_item.push(deviation_obj('admin', 'branch', @admin['base']))
      when @admin['branch']..@admin['area']
        @deviation_item.push(deviation_obj('admin', 'area', @admin['base']))
      when @admin['area']..@admin['region']
        @deviation_item.push(deviation_obj('admin', 'region', @admin['base']))
      when @admin['region']..@admin['head']
        @deviation_item.push(deviation_obj('admin', 'head', @admin['base']))
      else
        raise StandardError, 'Deviasi Biaya Admin Terlalu Tinggi'
      end
    end

    def deviation_rental
      val = @transaction[:monthly_fee].to_f * 100 / @transaction[:loan_amount].to_f
      case val
      when @rental['base']..100
        nil
      when @rental['branch']..@rental['base']
        @deviation_item.push(deviation_obj('rental', 'branch', @rental['base']))
      when @rental['area']..@rental['branch']
        @deviation_item.push(deviation_obj('rental', 'area', @rental['base']))
      when @rental['region']..@rental['area']
        @deviation_item.push(deviation_obj('rental', 'region', @rental['base']))
      when @rental['head']..@rental['region']
        @deviation_item.push(deviation_obj('rental', 'head', @rental['base']))
      else
        raise StandardError, 'Deviasi Biaya Sewa Terlalu Tinggi'
      end
    end

    def deviation_insurance_item_weight(weight)
      case weight
      when @insurance_item['branch_weight']..@insurance_item['min_weight']
        @deviation_item_tmp.push({
                                   level: 1,
                                   deviation_type: :insurance_item,
                                   office_type: :branch,
                                   office_id: @user.branch
                                 })
      when @insurance_item['area_weight']..@insurance_item['branch_weight']
        @deviation_item_tmp.push({
                                   level: 2,
                                   deviation_type: :insurance_item,
                                   office_type: :area,
                                   office_id: @user.area
                                 })
      when @insurance_item['region_weight']..@insurance_item['area_weight']
        @deviation_item_tmp.push({
                                   level: 3,
                                   deviation_type: :insurance_item,
                                   office_type: :region,
                                   office_id: @user.region
                                 })
      when 0..@insurance_item['head_weight']
        @deviation_item_tmp.push({
                                   level: 4,
                                   deviation_type: :insurance_item,
                                   office_type: :head,
                                   office_id: @user.head
                                 })
      end
    end

    def deviation_insurance_item_karats(karats)
      case karats.to_f
      when @insurance_item['branch_karats']..@insurance_item['base_karats'].to_f
        @deviation_item_tmp.push({
                                   level: 1,
                                   deviation_type: :insurance_item,
                                   office_type: :branch,
                                   office_id: @user.branch
                                 })
      when @insurance_item['area_karats']..@insurance_item['branch_karats']
        @deviation_item_tmp.push({
                                   level: 2,
                                   deviation_type: :insurance_item,
                                   office_type: :area,
                                   office_id: @user.area
                                 })
      when @insurance_item['region_karats']..@insurance_item['area_karats']
        @deviation_item_tmp.push({
                                   level: 3,
                                   deviation_type: :insurance_item,
                                   office_type: :region,
                                   office_id: @user.region
                                 })
      when 0..@insurance_item['area_karats']
        @deviation_item_tmp.push({
                                   level: 4,
                                   deviation_type: :insurance_item,
                                   office_type: :head,
                                   office_id: @user.head
                                 })
      end
    end

    def assign_deviation_insurance_item
      level = 0
      @deviation_item_tmp.each do |item|
        if item[:level] > level
          @deviation_insurance_item = item
          level = item[:level]
        end
      end
      @deviation_item.push(@deviation_insurance_item.slice(:deviation_type, :office_type, :office_id)) unless @deviation_insurance_item.nil?
    end

    def execute
      # Enable this later when ysing deviation insurance item
      # @transaction[:transaction_insurance_items_attributes].map do |val|
      #   deviation_insurance_item_karats(val[:carats].to_f)
      #   deviation_insurance_item_weight(val[:weight].to_f)
      # end
      # assign_deviation_insurance_item
      deviation_admin
      deviation_rental
      deviation_ltv
      deviation_one_obligor
      @transaction[:transaction_deviations_attributes] = @deviation_item
      @deviation_item
    end
  end
end
