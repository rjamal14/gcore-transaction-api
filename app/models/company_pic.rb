# Compnay PIC model
class CompanyPic < ApplicationRecord
  acts_as_paranoid

  attribute :name, :string
  attribute :phone_number, :string
  attribute :company_id, :integer
  attribute :position, :string
  attribute :identity_number, :string
  attribute :identity_type
  attribute :address, :text

  enum identity_type: [:ktp, :sim, :passport]
  validates_presence_of :name, :phone_number, :identity_number, :identity_type, :address,
                        :position
  validates_uniqueness_of :phone_number, :identity_number
  belongs_to :company
end
