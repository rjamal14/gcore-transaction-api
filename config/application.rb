require_relative 'boot'

require "rails"
# Pick the frameworks you want:
require "active_model/railtie"
require "active_job/railtie"
require "active_record/railtie"
require "active_storage/engine"
require "action_controller/railtie"
require "action_mailer/railtie"
require "action_mailbox/engine"
require "action_text/engine"
require "action_view/railtie"
require "action_cable/engine"
require "active_model_serializers"
require "i18n/backend/fallbacks"
require 'humanize'
# require "sprockets/railtie"
# require "rails/test_unit/railtie"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module GcoreTransactionApi
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 6.0
    config.i18n.load_path += Dir[Rails.root.join('config', 'locales', '**', '*.{rb,yml}')]
    config.i18n.default_locale = 'id'
    I18n::Backend::Simple.include I18n::Backend::Fallbacks
    config.i18n.fallbacks = { 'id' => 'en' }
    config.time_zone = 'Jakarta'
    config.middleware.insert_before 0, Rack::Cors do
      allow do
        origins '*'
        resource(
            '*',
            headers: :any,
            methods: [:get, :patch, :put, :delete, :post, :options]
        )
      end
    end
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.

    # Only loads a smaller set of middleware suitable for API only apps.
    # Middleware like session, flash, cookies can be added back manually.
    # Skip views, helpers and assets when generating a new resource.
    config.api_only = true

    Humanize.configure do |config|
      config.default_locale = :id  # [:en, :es, :fr, :tr, :de, :id], default: :en
      config.decimals_as = :digits # [:digits, :number], default: :digits
    end

    config.middleware.use ExceptionNotification::Rack,
                          # email: {
                          #  email_prefix: '[PREFIX] ',
                          #  sender_address: %("notifier" <#{ENV['MAILER_SENDER']}>),
                          #  exception_recipients: ENV['DEV_MAIL_LISTS'].split(',')
                          # },
                          slack: {
                            webhook_url: ENV['SLACK_WEBHOOK'],
                            channel: ENV['SLACK_CHANNEL'],
                            additional_parameters: {
                              icon_url: ENV['SLACK_ICON'],
                              mrkdwn: true
                            }
                          }
  end
end
