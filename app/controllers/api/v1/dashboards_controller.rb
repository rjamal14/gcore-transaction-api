module Api
  module V1
    # COA Controller
    class DashboardsController < ApplicationController
      skip_before_action :current_user, only: :show
      before_action :assign_transaction, only: :show
      def show
        render json: {
          transactions: {
            os: assign_active_transaction.sum(:loan_amount),
            noa: assign_active_transaction.count
          },
          transaction_installment: {
            os: assign_active_installment.sum(:loan_amount),
            noa: assign_active_installment.count
          },
          dpd: {
            os: assign_dpd_transaction.sum(:loan_amount),
            noa: assign_dpd_transaction.count
          },
          disbursement: transaction_filter(params[:disbursement_is_monthly], :disbursement),
          repayment: transaction_filter(params[:repayment_is_monthly], :repayment),
          prolongation: transaction_filter(params[:prolongation_is_monthly], :prolongation),
          installment: transaction_filter(params[:installment_is_monthly], :installment),
          dpd_graph: transaction_filter(params[:dpd_is_monthly], :dpd),
          outstanding: assign_outstanding(params[:outstanding_is_monthly])
        }
      end

      private

      def assign_transaction
        @transactions = params.key?(:office_id) ? PawnTransaction.where(office_id: params[:office_id]) : PawnTransaction
        begin_date = params.key?(:begin_date) ? params[:begin_date].to_date.beginning_of_day : Date.today.beginning_of_day
        end_date = params.key?(:end_date) ? params[:end_date].to_date.end_of_day : Date.today.end_of_day
        @transactions = @transactions.where(created_at: begin_date...end_date)
      end

      def assign_active_transaction
        @transactions.where(transaction_type: [:disbursement, :prolongation]).where('due_date >= ?', Date.today)
      end

      def assign_active_installment
        @transactions.where(transaction_type: :installment).where('due_date >= ?', Date.today)
      end

      def assign_dpd_transaction
        @transactions.where(transaction_type: [:disbursement, :prolongation, :installment]).where('due_date < ?', Date.today)
      end

      def transaction_filter(month, type)
        data = []
        month = to_boolean(month)
        begin_date = params.key?(:begin_date) ? params[:begin_date].to_date.beginning_of_month : Date.today.beginning_of_month
        end_date = params.key?(:end_date) ? params[:end_date].to_date.end_of_month : Date.today.end_of_month

        transactions = month ? Dashboard.where('EXTRACT(YEAR FROM date) = ? ', begin_date.year) : Dashboard.where(date: begin_date..end_date)
        transactions = transactions.where(office_id: params['office_id']) if params['office_id'].present?
        record = transactions.as_json

        (month ? 12 : end_date.day).times do |i|
          if month
            row = record.select { |r| r['date'].to_date.month == (i + 1) }
          else
            date = begin_date + i
            row = record.select { |r| r['date'].to_date == date }
          end

          data.push({
                      date: month ? (Date::MONTHNAMES[i + 1]) : (begin_date + i),
                      value: row.present? ? row.sum { |k| k["os_#{type}"].to_i } : nil
                    })
        end

        data
      end

      def assign_outstanding(month)
        data = []
        month = to_boolean(month)
        begin_date = params.key?(:begin_date) ? params[:begin_date].to_date.beginning_of_month : Date.today.beginning_of_month
        end_date = params.key?(:end_date) ? params[:end_date].to_date.end_of_month : Date.today.end_of_month

        transactions = month ? PawnTransaction.where('EXTRACT(YEAR FROM contract_date) = ? ', begin_date.year) : PawnTransaction.where(contract_date: begin_date..end_date)
        transactions = transactions.where(office_id: params['office_id']) if params['office_id'].present?
        transactions = transactions.where(status: 2, transaction_type: 0, payment_status: false)
        record = transactions.as_json

        (month ? 12 : end_date.day).times do |i|
          if month
            row = record.select { |r| r['contract_date'].to_date.month == (i + 1) }
          else
            date = begin_date + i
            row = record.select { |r| r['contract_date'].to_date == date }
          end

          data.push({
                      date: month ? (Date::MONTHNAMES[i + 1]) : (begin_date + i),
                      value: row.present? ? row.sum { |k| 1 } : nil
                    })
        end

        data
      end
    end
  end
end
