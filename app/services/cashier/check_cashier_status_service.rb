module Cashier
  class CheckCashierStatusService
    def initialize(balance:, type:, office_id:)
      @data = {
        balance: balance,
        type: type,
        office_id: office_id
      }
    end

    def execute
      RestClient.post "#{ENV['API_URL_ACCOUNTING']}/api/v1/cashiers/status",
                      @data, {}
    rescue RestClient::NotFound
      'close'
    rescue RestClient::BadRequest
      'minus'
    rescue RestClient::PreconditionFailed
      'last_error'
    rescue RestClient::Unauthorized
      'unapproved'
    end
  end
end
