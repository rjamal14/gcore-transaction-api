class CreateCustomerEmergencyContacts < ActiveRecord::Migration[6.0]
  def change
    create_table :customer_emergency_contacts do |t|
      t.belongs_to :customer
      t.string :name, null: false
      t.string :relation, null: false
      t.string :nationality, null: false
      t.string :address, null: false
      t.string :job_name, null: false
      t.datetime :deleted_at, index: true

      t.timestamps
    end
  end
end
