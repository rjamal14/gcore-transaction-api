# CashTransactionsSerializer
class CashoutTransactionsSerializer < ActiveModel::Serializer
  attributes :id, :user_id, :user_name, :date, :amount, :description, :ref, :status, :debit, :credit, :account, :created_at, :updated_at

  def debit
    debit = object.cashout_accounts.find { |v| v['type_account'] == 'debit' }

    {
      id: debit.try(:id),
      coa: debit.try(:coa),
      description: debit.try(:description)
    }
  end

  def credit
    credit = object.cashout_accounts.find { |v| v['type_account'] == 'credit' }

    {
      id: credit.try(:id),
      coa: credit.try(:coa),
      description: credit.try(:description)
    }
  end

  def account
    account = object.cashout_accounts.find { |v| v['type_account'] == 'account' }

    {
      id: account.try(:id),
      coa: account.try(:coa),
      description: account.try(:description)
    }
  end
end
