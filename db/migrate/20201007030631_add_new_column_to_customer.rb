class AddNewColumnToCustomer < ActiveRecord::Migration[6.0]
  def change
    add_column :customers, :created_by_name, :string
  end
end
