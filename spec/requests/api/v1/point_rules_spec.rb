require 'swagger_helper'

RSpec.describe 'api/v1/point_rules', type: :request do
  path '/api/v1/point_rules' do
    get('Get List Point Rules') do
      tags 'Point Rules'
      security [Bearer: {}]

      parameter name: 'page', in: 'query', type: :string
      parameter name: 'per_page', in: 'query', type: :string
      parameter name: 'order_by', in: 'query', type: :string
      parameter name: 'order', in: 'query', type: :string
      parameter name: 'search', in: 'query', type: :string
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    post('Create Point Rule') do
      tags 'Point Rules'
      security [Bearer: {}]

      parameter name: 'activity_name', in: 'body', type: :string
      parameter name: 'type', in: 'body', type: :string
      parameter name: 'point_amount', in: 'body', type: :string
      parameter name: 'expired_date', in: 'body', type: :string
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/point_rules/{id}' do
    # You'll want to customize the parameter types...
    parameter name: 'id', in: :path, type: :string, description: 'id'

    get('Show Point Rule') do
      tags 'Point Rules'
      security [Bearer: {}]

      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    put('Update Point Rule') do
      tags 'Point Rules'
      security [Bearer: {}]

      parameter name: 'activity_name', in: 'body', type: :string
      parameter name: 'type', in: 'body', type: :string
      parameter name: 'point_amount', in: 'body', type: :string
      parameter name: 'expired_date', in: 'body', type: :string
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    delete('Delete Point Rule') do
      tags 'Point Rules'
      security [Bearer: {}]

      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end
end
