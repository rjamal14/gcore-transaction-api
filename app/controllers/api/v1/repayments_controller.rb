module Api
  module V1
    # Prolongations  Controller
    class RepaymentsController < TransactionsController
      before_action :set_pawn_transaction, only: :create
      before_action :check_type_of_transaction, only: [:create, :inquiry]
      before_action :check_created_by_id, :validate_before_inquiry, only: :inquiry
      skip_before_action :current_user, only: :inquiry
      skip_before_action :set_deviation, only: :create
      after_action :update_dashboard, only: :create

      def create
        @pawn_transaction.payment_amount = Transaction::TransactionBillService.new(id: params[:id]).repayment_amount
        validate_before_create
        if @pawn_transaction.update(payment_status: true,
                                    updated_by_id: current_user.id,
                                    repayment_date: Date.today,
                                    transaction_type: :repayment)
          response_updated(@pawn_transaction, 'Data Pelunasan Terupdate')
          Journal::TransactionPublishService.new(@pawn_transaction).execute
        else
          response_error(@pawn_transaction, 'Data Pelunasan Gagal Terupdate')
        end
      end

      def inquiry
        respond_to do |format|
          office = get_master("offices/#{@pawn_transaction.office_id}?office_type=#{@pawn_transaction.office_type}")

          format.pdf do
            pdf = PrintInquiryRepaymentPdf.new(@pawn_transaction, @repayment_bill, office['offices']['data'], @user_created)
            send_data(
              pdf.render,
              filename: "E-SBG-(#{@transaction.sge})",
              type: 'application/pdf', disposition: 'inline'
            )
          end

          format.json { response_success(@repayment_bill, 'Data sukses') }
        end
      end

      private

      def validate_before_create
        transaction_temp = @pawn_transaction
        transaction_temp.transaction_type = 'repayment'

        cashier_status = check_cashier_status(type: 'debit', balance: @pawn_transaction.payment_amount)
        return true if cashier_status.present?

        cashier_report = check_cashier_report('debit', transaction_temp)
        return true if cashier_report.present?

        response_bad('Tranasksi Tidak bisa dilunasi') unless @type_of_transaction && @pawn_transaction.status == 'published'
      end

      def validate_before_inquiry
        check_cashier_status(balance: 0, type: :debit, _office_id: @user_created.office_id)
        return response_bad('Tranasksi Tidak bisa dilunasi') unless @type_of_transaction && @pawn_transaction.status == 'published'

        @repayment_bill = Transaction::TransactionBillService.new(id: params[:id]).repayment_bill
        @transaction = PawnTransaction.find(params[:id])
      end
    end
  end
end
