module Api
  module V1
    # Installment Controller
    class InstallmentsController < ApplicationController
      before_action :check_cashier, only: :create
      before_action :set_installment, only: [:show, :update, :destroy, :print_installment,
                                             :monthly_installment_payment, :monthly_installment_inquiry]
      skip_before_action :current_user, only: :print_installment
      before_action :set_residence_address, only: [:print_installment]

      def show
        @installment.approvals = []
        @installment.transaction_approvals.map do |k|
          next unless current_user.user_role['role_id']['$oid'] == k.user_type && k.user_id.nil?

          @installment.approvals.push({
                                        'approval_type' => k.user_type,
                                        'approval_url' => "#{ENV['BASE_URL']}/api/v1/transaction_approvals/#{k.id}"
                                      })
        end
        installment_serial = InstallmentSerializer.new(@installment)
        response_success(installment_serial, 'Data Ditemukan')
      end

      def create
        installment = Transaction::TransactionApprovalCheckService.new(transaction: installment_params).execute
        @installment = PawnTransaction.new(installment.merge(installment_added_attributes))
        @installment.installment_detail.assign_remaining_loan
        installment_serial = PawnTransactionSerializer.new(@installment)

        if @installment.save
          send_notification
          response_created(installment_serial, 'Data Kota Tersimpan')
        else
          response_error(@installment, 'Data Kota Gagal Tersimpan')
        end
      end

      def update
        if @installment.status == 'waiting_approval'
          installment_serial = InstallmentSerializer.new(@installment)
          if @installment.update(installment_params.merge(updated_by_id: current_user.id))
            response_updated(installment_serial, 'Data Kota Terupdate')
          else
            response_error(@installment, 'Data Kota Gagal Terupdate')
          end
        else
          response_bad('Status Transaksi sudah disetujuai, tidak bisa di update')
        end
      end

      def monthly_installment_payment
        @installment_detail = @installment.installment_detail
        @installment_item = @installment.installment_items
                                        .find_by(installment_order: @installment_detail.installment_remaining_order)
        @installment_item.update(installment_item_hash)
        @installment_detail.update(monthly_installment_hash)

        response_ok(@installment_item)
      rescue StandardError => e
        response_bad(e)
      end

      def print_installment
        return response_bad('transaksi cicilan sudah pernah dicetak') if @installment.status == 'published'
        return response_bad('transaksi cicilan belum disetujui') unless @installment.status == 'approved'

        if @installment.update(status: :published)
          res = Installment::GenerateMonthlyDueDateService.new(@installment)
          respond_to do |format|
            office = get_master("offices/#{@installment.office_id}?office_type=#{@installment.office_type}")
            format.pdf do
              pdf = PrintInstallmentPdf.new(@installment, office['offices']['data'], 'sign')
              send_data(
                pdf.render,
                filename: "E-SBG-(#{@installment.sge})",
                type: 'application/pdf', disposition: 'inline'
              )
            end
          end
        else
          response_bad('Print Gagal')
        end
      rescue StandardError => e
        response_bad(e)
      end

      def monthly_installment_inquiry
        response_success(InstallmentInquirySerializer.new(@installment, root: false), 'Data Cicilan')
      end

      # DELETE /transaction/1
      def destroy
        return unless @installment.destroy

        response_ok('Data transaction Terhapus')
        @installment.update(deleted_by_id: current_user.id)
      end

      private

      def installment_item_hash
        {
          payment_amount: monthly_installment_params[:payment_amount],
          payment_date: Date.today,
          fine_amount: monthly_installment_params[:fine_amount] || 0,
          status: true
        }
      end

      def monthly_installment_hash
        {
          remaining_loan: @installment_detail.remaining_loan - monthly_installment_params[:payment_amount] +
            @installment_detail.monthly_fee,
          installment_remaining_order: @installment_detail.installment_remaining_order + 1
        }
      end

      def installment_added_attributes
        {
          created_by_id: current_user.id,
          sge: generate_number('sge'),
          office_id: current_user.office_id,
          office_type: current_user.office_type,
          branch_id: current_user.branch,
          area_id: current_user.area,
          region_id: current_user.region,
          head_id: current_user.head
        }
      end

      def set_installment
        @installment = PawnTransaction.find(params[:id])
      rescue StandardError
        response_not_found('Data Tidak Ditemukan')
      end

      def create_installment_item
        @due_date = Date.today
        @installment_items_attributes = []
        installment_params[:installment_detail_attributes][:tenor].times do |i|
          @due_date += 30.days
          @installment_items_attributes.push({ installment_order: i + 1,
                                               due_date: @due_date })
        end
      end

      def check_cashier
        cashier_status = check_cashier_status(balance: installment_params[:loan_amount], type: 'credit')
        true if cashier_status.present?
      end

      def notification_hash
        {
          offices: [
            {
              office_id: current_user.office_id.to_s,
              role: 'KU'
            }
          ],
          type: 'siscab',
          subject: :transaction_approval,
          body: "Transaksi #{@installment.sge} perlu disetujui!",
          url: "#{ENV['SISCAB_URL']}/app/transaction/individual/#{@installment.id}/installment"
        }
      end

      def send_notification
        res = Notifications::SendNotificationService.new(notification_hash)
        res.execute
      end

      def installment_params
        params.require(:installment).permit(:insurance_item_id, :customer_id, :transaction_type, :product_name, :estimated_value,
                                            :company_id, :product_id, :status, :contract_date, :due_date, :auction_date,
                                            :loan_amount, :admin_fee, :monthly_fee, :per_15_days_fee, :insurance_item_name, :slte_rate,
                                            :maximum_loan, :maximum_loan_percentage, :notes, :changed, :payment_amount,
                                            transaction_insurance_items_attributes: [:id, :name,
                                                                                     :product_insurance_item_id,
                                                                                     :product_insurance_item_name,
                                                                                     :insurance_item_image, :buy_price,
                                                                                     :ownership, :amount, :net_weight,
                                                                                     :carats, :description, :gross_weight,
                                                                                     :estimated_value, :_destroy],
                                            transaction_deviation_item: [:type, :office_type, :office_id, :status],
                                            installment_detail_attributes: [:tenor, :monthly_installment, :monthly_interest, :monthly_fee,
                                                                            :down_payment],
                                            installment_items_attributes: [:due_date])
      end

      def monthly_installment_params
        params.require(:monthly_installment).permit(:payment_amount, :fine_amount)
      end

      def set_residence_address
        data_residence = {
          province: @installment.customer.customer_contact.residence_province,
          city: @installment.customer.customer_contact.residence_city,
          region: @installment.customer.customer_contact.residence_region,
          subdistric: @installment.customer.customer_contact.residence_subdistric
        }

        data_residence = get_master("address?#{data_residence.to_query}")['data']

        @installment.customer.customer_contact.residence_province_name = data_residence['province_name']
        @installment.customer.customer_contact.residence_city_name = data_residence['city_name']
        @installment.customer.customer_contact.residence_region_name = data_residence['region_name']
        @installment.customer.customer_contact.residence_subdistric_name = data_residence['subdistric_name']
      rescue StandardError
        @installment.customer.customer_contact.residence_province_name = nil
        @installment.customer.customer_contact.residence_city_name = nil
        @installment.customer.customer_contact.residence_region_name = nil
        @installment.customer.customer_contact.residence_subdistric_name = nil
      end
    end
  end
end
