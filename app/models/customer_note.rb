# CustomerNote
class CustomerNote < ApplicationRecord
  acts_as_paranoid
  belongs_to :customer, -> { with_deleted }, optional: true
end
