class CreateCustomerBankAccounts < ActiveRecord::Migration[6.0]
  def change
    create_table :customer_bank_accounts do |t|
      t.belongs_to :customer
      t.string :account_number, null: false
      t.string :account_name, null: false
      t.string :bank_name, null: false
      t.string :branch_name
      t.datetime :deleted_at, index: true

      t.timestamps
    end
  end
end
