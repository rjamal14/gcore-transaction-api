class CustomerCreateEmergencyContactsWorker
  include Sneakers::Worker

  queue_name = "#{Rails.env}-migrations.customers.emergency_contacts.create"
  from_queue queue_name, arguments: { 'x-dead-letter-exchange': "#{queue_name}-retry" }

  def work(msg)
    data = ActiveSupport::JSON.decode(msg)
    data.each do |value|
      CustomerEmergencyContact.new(value.to_h).save!
    end
    ack!
  rescue StandardError => e
    logger.fatal "Error message CustomerCreateEmergencyContactsWorker => #{e.message}"
    reject!
  end
end
