# Transaction Serializer
class TransactionDetailDeviationSerializer < ActiveModel::Serializer
  attributes :id, :sge, :status, :contract_date, :office_id, :customer, :due_date, :product_id, :insurance_item_id,
             :insurance_item_name, :auction_date, :loan_amount, :estimate_value,
             :monthly_fee, :admin_fee, :approvals, :download, :product_name, :is_deviation, :deviation_status,
             :deviation_approvals

  def download
    object.status == 'approved' ? "#{ENV['BASE_URL']}/api/v1/transactions/#{object.id}/contract.pdf" : nil
  end

  def deviations
    object.try(:transaction_deviations)
  end

  def is_deviation
    deviations.present?
  end

  def deviation_status
    deviations.unapproved.empty?
  end

  def deviation_approvals
    {
      one_obligor: find_deviation(:one_obligor),
      rental: find_deviation(:rental),
      admin: find_deviation(:admin),
      ltv: find_deviation(:ltv)
    }
  end

  def find_deviation(type)
    deviations.find_by(deviation_type: type).try(:office_type)
  end

  def total_buy_price
    object.try(:transaction_insurance_items).sum(:buy_price)
  end

  def customer
    {
      id: cus.try(:id),
      name: cus.name,
      cif_number: cus.cif_number,
      identity_number: cus.identity_number,
      address: cus_contact.try(:residence_address),
      phone_number: cus_contact.try(:phone_number),
      gender: cus.try(:gender)
    }
  end

  def cus
    object.try(:customer)
  end

  def cus_contact
    CustomerContact.with_deleted.find_by(customer_id: cus.id)
  end

  has_many :transaction_insurance_items
  has_many :transaction_deviations
  has_many :transaction_deviations, only: [:deviation_type, :status]

  def estimate_value
    object.try(:transaction_insurance_items).sum(:estimated_value)
  end
end
