class ChangeMaritalStatusInCustomers < ActiveRecord::Migration[6.0]
  def change
    remove_column :customers, :marital_status, :boolean
    add_column :customers, :marital_status, :integer
  end
end
