module Cashier
  class PublishCashierReportService
    def initialize(data:)
      @data = data
    end

    def description(type)
      "#{type} #{@data.sge}"
    end

    def cash_transaction
      {
        sge: "COA: #{@data.req_coa}/#{@data.applicant_name}",
        balance: @data.req_amount,
        type: 'credit',
        description: @data.req_description
      }
    end

    def transaction_debit
      {
        office_id: @data.office_id,
        sge: @data.sge,
        balance: @data.loan_amount,
        rental_cost: @data.payment_amount - @data.loan_amount,
        type: 'debit',
        description: @data.transaction_type == 'prolongation' ? description('Perpanjangan') : description('Pelunasan')
      }
    end

    def non_transactional
      {
        office_id: @data.office_id,
        sge: @data.ref,
        balance: @data.amount,
        type: 'credit',
        description: @data.description
      }
    end

    def check_type_of_transaction(type)
      case type
      when 'debit'
        transaction_debit
      when 'non_transactional'
        non_transactional
      else
        cash_transaction
      end
    end

    def execute(type:, headers:)
      payload = check_type_of_transaction(type)
      RestClient.post "#{ENV['API_URL_ACCOUNTING']}/api/v1/daily_cashes/transaction",
                      payload, headers
    rescue RestClient::NotFound
      'close'
    rescue RestClient::BadRequest
      'minus'
    end
  end
end
