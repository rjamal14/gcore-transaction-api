# Bill
module Transaction
  # Bill Service
  class TransactionListReportService
    def initialize(transactions:, params:)
      @params = params
      @transactions = transactions
      @transaction_type = params[:type]
      @transaction_ransack = ''
      @transaction_paginate = ''
      @transaction_serialize = ''
    end

    def get_data
      execute

      if @transaction_type == 'outstanding'
        {
          transaction: @transaction_ransack,
          transaction_paginate: @transaction_paginate,
          transaction_serialize: @transaction_serialize
        }
      else
        {
          transaction: @transaction_ransack,
          transaction_paginate: @transaction_paginate,
          transaction_serialize: @transaction_serialize,
          transaction_type_translate: translate_type(type: @transaction_type)
        }
      end
    end

    def execute
      set_ransack
      set_pagination
      set_serialize
    end

    def check_status
      if @transaction_type == 'outstanding'
        @transactions = @transactions.where(payment_status: false, status: %w[approved published])
      else
        @transactions = @transactions.where(status: :published)
        check_type_transaction if @transaction_type.present?
      end
    end

    def check_type_transaction
      @transactions = case @transaction_type
                      when 'disbursement'
                        @transactions.where(transaction_type: %w[disbursement repayment])
                      when 'prolongation'
                        @transactions.where(transaction_type: 'prolongation')
                      else
                        @transactions.where(transaction_type: @transaction_type)
                      end
    end

    def set_ransack
      check_status
      @transaction_ransack = @transactions.ransack(@params[:q]).result
    end

    def set_pagination
      @transaction_paginate = @transaction_ransack.page(@params[:page] || 1)
                                                  .per(@params[:per_page] || 10)
                                                  .order("#{@params[:order_by] || 'created_at'} #{@params[:order] || 'desc'}")
    end

    def set_serialize
      @transaction_serialize = @transaction_paginate.map { |transactions| TransactionIndexSerializer.new(transactions, root: false) }
    end

    def translate_type(type:)
      case type
      when 'disbursement'
        'Pencairan'
      when 'repayment'
        'Pelunasan'
      when 'prolongation'
        'Perpanjangan'
      when 'auction'
        'Lelang'
      end
    end
  end
end
