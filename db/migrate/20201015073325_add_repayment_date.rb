class AddRepaymentDate < ActiveRecord::Migration[6.0]
  def change
    add_column :pawn_transactions, :repayment_date, :date
  end
end
