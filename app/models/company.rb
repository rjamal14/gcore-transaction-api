# Company Model
class Company < ApplicationRecord
  include PgSearch::Model
  pg_search_scope :search, against: [:name, :cif_number, :phone_number, :branch_office_name],
                           using: {
                             tsearch: {
                               prefix: true
                             }
                           }
  acts_as_paranoid

  attribute :cif_number, :string
  attribute :branch_office_id, :string
  attribute :branch_office_name, :string
  attribute :name, :string
  attribute :phone_number, :string
  attribute :tax_number, :string
  attribute :city_id, :string
  attribute :city_name, :string
  attribute :address, :text
  attribute :status, :boolean, default: false
  attribute :created_by_id, :string
  attribute :updated_by_id, :string
  attribute :deleted_by_id, :string

  validates_presence_of :cif_number, :branch_office_id, :name, :phone_number, :tax_number, :city_id, :address,
                        :branch_office_name, :city_name, :company_pics
  validates_uniqueness_of :cif_number, :name, :phone_number, :tax_number
  has_many :company_pics, dependent: :destroy, index_errors: true
  has_many :pawn_transactions, index_errors: true
  accepts_nested_attributes_for :company_pics, allow_destroy: true
end
