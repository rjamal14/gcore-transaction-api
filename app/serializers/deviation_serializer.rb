# Transaction Index Serializer
class DeviationSerializer < ActiveModel::Serializer
  attributes :id, :sge, :deviation_type, :status, :contract_date, :due_date, :office_id, :loan_amount, :product_name,
             :transaction_type, :customer_name, :estimate_value, :insurance_item_name,
             :maximum_loan, :maximum_loan_percentage, :cif_number, :is_deviation, :tenor

  def transaction
    object.try(:pawn_transaction)
  end

  def transaction_type
    transaction.transaction_type
  end

  def customer_name
    transaction.try(:customer).try(:name)
  end

  def maximum_loan_percentage
    transaction.maximum_loan_percentage
  end

  def maximum_loan
    transaction.maximum_loan
  end

  def sge
    transaction.sge
  end

  def contract_date
    transaction.contract_date
  end

  def due_date
    transaction.due_date
  end

  def insurance_item_name
    transaction.insurance_item_name
  end

  def loan_amount
    transaction.loan_amount
  end

  def product_name
    transaction.product_name
  end

  def tenor
    transaction.try(:installment_detail).try(:tenor)
  end

  def estimate_value
    transaction.transaction_type == 'installment' ? transaction.try(:estimated_value) : transaction.transaction_insurance_items.sum(:estimated_value)
  end

  def cif_number
    transaction.try(:customer).try(:cif_number)
  end

  def is_deviation
    transaction.transaction_deviations.present?
  end
end
