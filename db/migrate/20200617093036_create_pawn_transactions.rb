class CreatePawnTransactions < ActiveRecord::Migration[6.0]
  def change
    create_table :pawn_transactions do |t|
      t.string :insurance_item_id
      t.string :customer_id
      t.string :company_id
      t.string :sge
      t.string :product_id
      t.integer :status
      t.integer :transaction_type
      t.date :contract_date
      t.date :due_date
      t.date :auction_date
      t.integer :loan_amount
      t.integer :admin_fee
      t.integer :monthly_fee
      t.string :created_by_id
      t.string :updated_by_id
      t.string :deleted_by_id
      t.datetime :deleted_at, index: true

      t.timestamps null: false
    end
  end
end
