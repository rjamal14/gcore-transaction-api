# transaction_approval Controller
module Api
  module V1
    # transaction_approval  Controller
    class TransactionApprovalsController < ApplicationController
      before_action :set_transaction_approval, only: [:update]
      before_action :check_deviation_status, only: :update
      skip_before_action :current_user, only: :check_transaction_status

      # PATCH/PUT /transaction_approval/1
      def update
        return if post_transaction_to_cashier.present?

        if @transaction_approval.update(transaction_approval_params.merge(user_id: current_user.id, updated_by_id: current_user.id))
          @transaction.update(status: :approved)
          response_updated(@transaction_approval, 'Data Terupdate')
        else
          response_error(@transaction_approval, 'Data Gagal Terupdate')
        end
      end

      def check_transaction_status
        return response_bad('Transaksi Belum Semua Sukses') if PawnTransaction.where(status: :waiting_approval,
                                                                                     office_id: params[:office_id]).present?

        response_ok('Transaksi Lengkap')
      end

      private

      def check_deviation_status
        response_bad('Masih Ada Deviasi yang belum disetujui') unless @transaction.transaction_deviations.unapproved.empty?
      end

      # Use callbacks to share common setup or constraints between actions.
      def set_transaction_approval
        @transaction_approval = TransactionApproval.find(params[:id])
        @transaction = PawnTransaction.find(@transaction_approval.pawn_transaction_id)
      rescue StandardError
        response_not_found('Data Tidak Ditemukan')
      end

      def post_transaction_to_cashier
        return unless %w[disbursement prolongation].include?(@transaction.transaction_type)

        res = journal_type.new(data: @transaction).execute(headers: { Authorization: request.headers['Authorization'] })

        return unless %w[minus close].include?(res)

        response_message = res == 'close' ? 'Kasir Tutup' : 'Saldo Kurang'
        response_bad(response_message)
      end

      def journal_type
        case @transaction.transaction_type
        when 'disbursement'
          Cashier::PubCashierDisbursementService
        when 'prolongation'
          Cashier::PubCashierProlongationService
        end
      end

      # Only allow a trusted parameter "white list" through.
      def transaction_approval_params
        params.require(:transaction_approval).permit(:attachment)
      end
    end
  end
end
