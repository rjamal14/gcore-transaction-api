class AddReqAmountCashTransaction < ActiveRecord::Migration[6.0]
  def change
    add_column :cash_transactions, :req_amount, :float
  end
end
