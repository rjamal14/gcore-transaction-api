# company Controller
module Api
  module V1
    # company  Controller
    class CompaniesController < ApplicationController
      before_action :set_company, only: [:show, :update, :destroy]
      before_action :check_branch_office, only: :index

      # GET /company
      def index
        @companies = if params[:search]
                       @companies.search(params[:search])
                     else
                       @companies
                     end
        @companies = @companies.page(params[:page] || 1).per(params[:per_page] || 10)
                               .order("#{params[:order_by] || 'created_at'} #{params[:order] || 'desc'}")
        serial_companies = @companies.map { |companies| CompanySerializer.new(companies, root: false) }
        response_index(@companies, serial_companies)
      rescue StandardError => e
        response_bad(e)
      end

      # GET /company/1
      def show
        company_serial = CompanyDetailSerializer.new(@company)
        response_success(company_serial, 'Data Ditemukan')
      end

      # POST /company
      def create
        @company = Company.new(company_params.merge(created_by_id: current_user.id,
                                                    branch_office_id: current_user.office_id,
                                                    branch_office_name: current_user.office_name,
                                                    cif_number: generate_number('cif')))
        @company_serial = CompanySerializer.new(@company)

        if @company.save
          response_created(@company_serial, 'Data Kota Tersimpan')
        else
          response_error(@company, 'Data Kota Gagal Tersimpan')
        end
      end

      # PATCH/PUT /company/1
      def update
        if @company.update(company_params.merge(updated_by_id: current_user.id,
                                                branch_office_id: current_user.office_id,
                                                branch_office_name: current_user.office_name))
          response_updated(@company, 'Data Kota Terupdate')
        else
          response_error(@company, 'Data Kota Gagal Terupdate')
        end
      end

      # DELETE /company/1
      def destroy
        return unless @company.destroy

        response_ok('Data company Terhapus')
        @company.update(deleted_by_id: current_user.id)
      end

      private

      def check_branch_office
        @companies = if current_user.office_type == 'head'
                       Company
                     else
                       Company.where(branch_office_id: current_user.office_id)
                     end
      end

      # Use callbacks to share common setup or constraints between actions.
      def set_company
        @company = Company.find(params[:id])
      rescue StandardError
        response_not_found('Data Tidak Ditemukan')
      end

      # Only allow a trusted parameter "white list" through.
      def company_params
        params.require(:company).permit(:name, :phone_number,
                                        :city_id, :address, :tax_number, :city_name, :status, :area_code,
                                        company_pics_attributes: [:id, :name, :position, :phone_number,
                                                                  :address, :identity_number, :identity_type, :_destroy])
      end
    end
  end
end
