class AddNewColumnToPawnTransaction < ActiveRecord::Migration[6.0]
  def change
    add_column :pawn_transactions, :office_id, :string
  end
end
