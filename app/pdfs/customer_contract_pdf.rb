require 'prawn'
require 'prawn/measurement_extensions'
class CustomerContractPdf < Prawn::Document
  def initialize(transaction, office, type)
    super({ top_margin: 35, page_layout: :portrait, page_size: 'A4' })
    @transaction      = transaction
    @office           = office
    @type             = type

    first_page
  end

  def first_page
    wrapper_height = bounds.height
    wrapper_width = bounds.width

    y0_point = cursor
    
    move_down 2.cm
    indent(12.cm) do
      text "#{@office['branch_code']}", :style => :bold, :size => 9
      text "#{@office['branch_name']}", :style => :bold, :size => 9
    end
    
    y_point = cursor
    move_down 0.2.cm
    indent(2.cm) do
      move_down 0.1.cm
      text "#{@transaction.customer.cif_number}", :style => :bold, :size => 9
      move_down 0.1.cm
      text "#{@transaction.customer.name}", :style => :bold, :size => 9
      move_down 0.1.cm
      text "#{@transaction.customer.identity_number}", :style => :bold, :size => 9
      text "#{address_format(@transaction.customer.customer_contact)}", :style => :bold, :size => 8
      move_down 0.1.cm
      text "#{@transaction.customer.customer_contact.phone_number}", :style => :bold, :size => 9
    end
    
    y1_point = cursor
    move_up y_point - y1_point
    move_down 0.2.cm
    indent(9.cm) do
      move_down 0.4.cm
      text "#{@transaction.sge}", :style => :bold, :size => 9
      indent(2.5.cm) do
        move_down 0.2.cm
        text "#{@transaction.contract_date.try(:strftime, '%d %m %Y')}", :style => :bold, :size => 9
        move_down 0.5.cm
        text "#{@transaction.due_date.try(:strftime, '%d %m %Y')}", :style => :bold, :size => 9
        move_down 0.3.cm
        text "#{@transaction.auction_date.try(:strftime, '%d %m %Y')}", :style => :bold, :size => 9
      end
    end

    y2_point = cursor
    move_down 1.cm
    indent(0.5.cm) do
      text "#{format_brg(@transaction.transaction_insurance_items)}", :style => :bold, :size => 8
    end

    y3_point = cursor
    move_up y2_point - y3_point
    indent(9.cm) do
      indent(2.5.cm) do
        move_down 0.5.cm
        text "#{price_format(@transaction.transaction_insurance_items.map(&:estimated_value).inject(0, &:+).to_i)}", :style => :bold, :size => 9
        move_down 0.5.cm
        text "#{price_format(@transaction.loan_amount.to_i)}", :style => :bold, :size => 9
        move_down 0.4.cm
        text "#{price_format((@transaction.monthly_fee/2).to_i)}", :style => :bold, :size => 9
        move_down 0.4.cm
        text "#{price_format(@transaction.admin_fee.to_i)}", :style => :bold, :size => 9
        move_down 0.4.cm
       
      end
    end

    y4_point = cursor
    move_up y0_point - y4_point
    move_down 1.9.cm
    indent(16.cm) do
      text "#{@transaction.customer.cif_number}", :style => :bold, :size => 7
      text "#{@transaction.customer.name}", :style => :bold, :size => 7
      text "#{@transaction.customer.identity_number}", :style => :bold, :size => 7
      text "#{@transaction.customer.customer_contact.residence_address}", :style => :bold, :size => 7
    end

    indent(15.cm) do
      move_down 1.2.cm
      text "#{@transaction.sge}", :style => :bold, :size => 7
      move_down 0.4.cm
      text "#{@transaction.auction_date.try(:strftime, '%d %m %Y')}", :style => :bold, :size => 8
      move_down 0.4.cm
      text "#{@transaction.contract_date.try(:strftime, '%d %m %Y')}", :style => :bold, :size => 8
      move_down 0.5.cm
      text "#{format_brg2(@transaction.transaction_insurance_items)}", :style => :bold, :size => 7
    end

    indent(16.cm) do
      move_down 1.4.cm
      text "#{price_format(@transaction.transaction_insurance_items.map(&:estimated_value).inject(0, &:+).to_i)}", :style => :bold, :size => 8
      move_down 0.5.cm
      text "#{price_format(@transaction.loan_amount.to_i)}", :style => :bold, :size => 8
      move_down 0.5.cm
      text "#{@transaction.due_date.try(:strftime, '%d %m %Y')}", :style => :bold, :size => 8
    end

  end

  def price_format(value)
    ActionController::Base.helpers.number_to_currency(value, unit: '', strip_insignificant_zeros: true)
  end

  def strip_trailing_zero(n)
    n.to_s.sub(/\.?0+$/, '')
  end

  def address_format(n)
    "#{n.residence_address}, \n  #{n.residence_subdistric_name}, #{n.residence_region_name}, \n  #{n.residence_city_name}, #{n.residence_postal_code}, \n  #{n.residence_province_name}"
  end
  
  def format_brg(n)
    text=''
    n.each do |item|
        text = "#{text}#{item.amount.humanize} #{item.try(:name)} DTM#{strip_trailing_zero(item.carats)}KRT BERAT #{strip_trailing_zero(item.net_weight)}GRM \n"
    end

    text.upcase!
  end

  def format_brg2(n)
    text=''
    n.each do |item|
        text = "#{text}#{item.try(:name)}\n"
    end

    text.upcase!
  end

  
end
