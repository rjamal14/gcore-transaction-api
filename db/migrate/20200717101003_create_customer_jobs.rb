class CreateCustomerJobs < ActiveRecord::Migration[6.0]
  def change
    create_table :customer_jobs do |t|
      t.belongs_to :customer
      t.string :job_type, null: false
      t.string :company_name
      t.string :customer_type, null: false
      t.string :tax_number, null: false
      t.string :income_source, null: false
      t.string :disbursement_type, null: false
      t.string :financing_purpose, null: false
      t.integer :salary_amount, null: false
      t.datetime :deleted_at, index: true
      t.timestamps
    end
  end
end
