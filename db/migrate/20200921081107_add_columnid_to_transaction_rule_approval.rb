class AddColumnidToTransactionRuleApproval < ActiveRecord::Migration[6.0]
  def change
    add_column :transaction_rule_approvals, :transaction_rule_id, :string
  end
end
