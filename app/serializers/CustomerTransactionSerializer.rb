# CustomerSerializer
class CustomerTransactionSerializer < ActiveModel::Serializer
  attributes :id, :name, :address, :identity_number, :phone_number, :gender
  def address
    object.try(:customer_contact).try(:residence_address)
  end

  def phone_number
    object.try(:customer_contact).try(:phone_number)
  end
end
