# Module response
module Converter
  extend ActiveSupport::Concern

  def to_boolean(str)
    ActiveModel::Type::Boolean.new.cast(str)
  end
end
