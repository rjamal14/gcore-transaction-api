class AddProlongationOrderToPawnTransaction < ActiveRecord::Migration[6.0]
  def change
    add_column :pawn_transactions, :prolongation_order, :integer
  end
end
