class InstallmentItem < ApplicationRecord
  belongs_to :pawn_transaction

  validates_uniqueness_of :installment_order, scope: :pawn_transaction
end
