# role name
class User
  include ActiveModel::Model
  def initialize(attributes = {})
    attributes.each do |attr, _value|
      # Setter
      define_singleton_method("#{attr}=") { |val| attributes[attr] = val }
      # Getter
      define_singleton_method(attr) { attributes[attr] }
    end
  end
end
