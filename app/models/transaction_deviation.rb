class TransactionDeviation < ApplicationRecord
  acts_as_paranoid
  after_create :send_notification
  after_update :check_approval

  scope :unapproved, -> { where(status: :waiting) }
  scope :rejected, -> { where(status: :rejected) }

  enum office_type: [:branch, :area, :region, :head]
  enum status: [:waiting, :approved, :rejected]
  enum deviation_type: [:ltv, :rental, :admin, :one_obligor, :insurance_item]
  attribute :deviation_type
  attribute :office_id
  attribute :office_type
  attribute :status, default: :waiting

  attr_accessor :role

  belongs_to :pawn_transaction

  def notification_hash
    {
      offices: [
        {
          office_id: office_id,
          role: office_type == :head ? 'SUPER' : 'KA'
        }
      ],
      type: office_type == :head ? 'sispus' : 'siscab',
      subject: :transaction_deviation,
      body: "Transaksi Deviasi #{pawn_transaction.sge} perlu disetujui!",
      url: '/approval/deviation'
    }
  end

  def send_notification
    res = Notifications::SendNotificationService.new(notification_hash)
    res.execute
  end

  def check_approval
    send_notification_approved unless pawn_transaction.transaction_deviations.unapproved.present?
  end

  def notification_approved_hash
    {
      offices: [
        {
          office_id: pawn_transaction.office_id,
          role: pawn_transaction.office_type == 'head' ? 'SUPER' : 'KA'
        }
      ],
      type: pawn_transaction.office_type == 'head' ? 'sispus' : 'siscab',
      subject: :transaction_deviation,
      body: "Transaksi Deviasi #{pawn_transaction.sge} telah disetujui!",
      url: '/approval/deviation'
    }
  end

  def send_notification_approved
    res = Notifications::SendNotificationService.new(notification_approved_hash)
    res.execute
  end
end
