source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.6.4'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 6.0.3', '>= 6.0.3.1'
# Use postgresql as the database for Active Record
gem 'pg', '>= 0.18', '< 2.0'
# Use Puma as the app server
gem 'puma', '~> 4.1'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
# gem 'jbuilder', '~> 2.7'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 4.0'
# Use Active Model has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Active Storage variant
# gem 'image_processing', '~> 1.2'

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.4.2', require: false

gem 'active_model_serializers', '~>0.9.4', require: true
gem 'carrierwave-base64'
gem 'carrierwave'
gem 'dotenv-rails', require: 'dotenv/rails-now'
gem 'enumerize', '~> 2.3.1'
gem 'i18n'
gem 'kaminari'
gem "paranoia", "~> 2.2"
gem 'pg_search'
gem 'rack-cors'
gem 'rails-i18n', '~> 6.0.0'
gem 'rest-client', '~> 2.1.0'
gem 'rswag'
group :development, :staging, :test do
  gem 'rspec-rails'
  gem 'rswag-specs'
end
gem 'rubocop'
gem 'therubyracer'
gem 'prawn', '~> 2.1' # pdf
gem 'prawn-table', '~> 0.1.0' # pdf table
gem 'humanize', '~> 2.4.3'
gem 'whenever', require: false
gem 'ransack', '~> 2.3.2', github: 'activerecord-hackery/ransack'
gem 'oauth2', '~> 1.4.4'
gem 'caxlsx', '3.0.1'
gem 'caxlsx_rails', '~> 0.6.2'
gem 'bunny', '>= 2.13.0', require: true
gem 'sneakers', '>=2.11.0'

# Use Rack CORS for handling Cross-Origin Resource Sharing (CORS), making cross-origin AJAX possible
# gem 'rack-cors'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
end

group :development do
  gem 'capistrano', '~> 3.10'
  gem 'capistrano-bundler', '~> 1.4'
  gem 'capistrano-rails', '~> 1.4', require: false
  gem 'capistrano-rbenv', '~> 2.2'
  gem 'capistrano3-puma', '~> 4.0'
  gem 'listen', '~> 3.2'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'mimemagic', github: 'mimemagicrb/mimemagic', ref: '01f92d86d15d85cfd0f20dabd025dcbd36a8a60f'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]

gem 'aliyun-sdk'
gem 'carrierwave-aliyun'

gem 'slack-notifier'
gem 'exception_notification'
