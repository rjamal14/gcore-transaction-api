class EditColumnCustomerJobTax < ActiveRecord::Migration[6.0]
  def change
    change_column :customer_jobs, :tax_number, :string, :null => true
  end
end
