# Customer model
class Customer < ApplicationRecord
  include PgSearch::Model
  include Rails.application.routes.url_helpers
  pg_search_scope :search, against: [:name, :cif_number, :identity_number],
                           associated_against: {
                             customer_contact: :phone_number
                           },
                           using: {
                             tsearch: {
                               prefix: true
                             }
                           }
  acts_as_paranoid
  before_create :refer
  def refer
    self.referral_code = "CUST#{cif_number}"
  end
  enum gender: [:l, :p]
  enum identity_type: [:ktp, :sim, :passport]
  enum marital_status: [:single, :married, :divorced]

  attribute :cif_number
  attribute :gender
  attribute :identity_type
  attribute :status, default: true

  mount_base64_uploader :id_scan_image, CustomerUploader

  validates_uniqueness_of :cif_number, :identity_number
  validates_presence_of :cif_number, :identity_number, :name, :identity_type, :branch_office_id, :birth_date, :degree,
                        :gender, :mother_name
  has_many :customer_notes, dependent: :destroy, index_errors: true
  has_one :customer_contact, dependent: :destroy
  has_one :customer_job, dependent: :destroy
  has_many :customer_emergency_contacts, dependent: :destroy, index_errors: true
  has_many :pawn_transactions, index_errors: true
  has_many :customer_bank_accounts, dependent: :destroy, index_errors: true
  has_many :transaction_points, dependent: :destroy

  accepts_nested_attributes_for :customer_emergency_contacts, allow_destroy: true
  accepts_nested_attributes_for :customer_job, allow_destroy: true
  accepts_nested_attributes_for :customer_contact, allow_destroy: true
  accepts_nested_attributes_for :customer_notes, allow_destroy: true
  accepts_nested_attributes_for :customer_bank_accounts, allow_destroy: true
  accepts_nested_attributes_for :transaction_points, allow_destroy: true

  def ransackable_associations(_auth_object = nil)
    reflect_on_all_associations.map { |a| a.name.to_s }
  end

  def active_model_serializer
    CustomerDetailSerializer
  end
end
