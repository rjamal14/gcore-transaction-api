# Journal
require 'json'
require 'base64'

module Journal
  class CashoutPublishService
    def initialize(data)
      @queue_name = "#{ENV['rbmq_env']}-journal.cashout.create"

      @cashout = {
        cashout_id: data.id,
        amount: data.amount,
        account: data.cashout_accounts.where(type_account: :debit).last.try(:coa),
        description: data.description
      }
    end

    def execute
      publish
    end

    def channel
      @channel ||= ::Publisher::BunnyPublisher.connection.create_channel
    end

    def exchange
      @exchange ||= channel.exchange(
        "#{ENV['rbmq_env']}-journal",
        type: 'direct',
        durable: true
      )
    end

    def publish
      exchange.publish(@cashout.to_json, routing_key: @queue_name)
      channel.close
    end
  end
end
