# Transaction Insurance Item Model
class CashTransaction < ApplicationRecord
  acts_as_paranoid

  enum req_status: [:approved, :waiting_approval, :rejected]

  validates_presence_of :req_subject, :req_description, :req_status,
                        :req_amount, :applicant_name, :req_coa, :office_id
  validates :created_by_id, presence: true, on: :create
  validates :updated_by_id, presence: true, on: :update

  before_create :generate_req_id

  def generate_req_id
    self.req_id = "RK#{Time.now.strftime('%y%m%d%I%M')}"
  end
end
