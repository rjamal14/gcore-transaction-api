module Cashier
  class PubCashierProlongationService
    def initialize(data:)
      @data = data
    end

    def payload
      {
        id: @data.id,
        parent: @data,
        office_id: @data.office_id,
        sge: @data.sge,
        past_sge: @data.parent.sge,
        past_loan_amount: @data.parent.loan_amount,
        loan_amount: @data.loan_amount,
        rental_fee: @data.parent.payment_amount - @data.admin_fee,
        admin_fee: @data.admin_fee,
        description: "Jurnal Perpanjangan SGE : #{@data.sge}",
        monthly_fee: @data.parent.monthly_fee,
        due_date: @data.parent.due_date,
        payment_amount: @data.parent.payment_amount
      }
    end

    def execute(headers:)
      RestClient.post "#{ENV['API_URL_ACCOUNTING']}/api/v1/cashiers/transaction/prolongation",
                      payload, headers
    rescue RestClient::NotFound
      'close'
    rescue RestClient::BadRequest
      'minus'
    end
  end
end
