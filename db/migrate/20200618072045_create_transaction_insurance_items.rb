class CreateTransactionInsuranceItems < ActiveRecord::Migration[6.0]
  def change
    create_table :transaction_insurance_items do |t|
      t.string :name
      t.string :product_insurance_item_id
      t.string :ownership
      t.integer :amount
      t.float :weight
      t.float :carats
      t.text :description
      t.integer :pawn_transaction_id
      t.integer :estimated_value
      t.datetime :deleted_at, index: true

      t.timestamps
    end
  end
end
