namespace :dpd do
  desc 'service untuk mengambil transaksi yang telat bayar'
  task check: :environment do
    records = PawnTransaction.where(repayment_date: nil, due_date: Date.today - 1)

    if records.present?
      dashboards = Dashboard.find_or_create_by(date: records[0].due_date)
      dashboards.update_attributes({
                                     'os_dpd' => dashboards['os_dpd'].to_i + records.count,
                                     'month' => records[0].due_date.strftime('%Y-%m'),
                                     'office_id' => records[0].office_id
                                   })
    end
  end
end
