namespace :sneakers do
  desc 'Start work sneakers'
  task run: :environment do
    ::Rails.application.eager_load!
    Rake::Task['sneakers:run'].invoke
  end
end
