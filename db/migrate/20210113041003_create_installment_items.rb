class CreateInstallmentItems < ActiveRecord::Migration[6.0]
  def change
    create_table :installment_items do |t|
      t.belongs_to :pawn_transaction
      t.integer :installment_order
      t.date :due_date
      t.date :payment_date
      t.integer :payment_amount
      t.integer :fine_amount
      t.datetime :deleted_at, index: true

      t.timestamps
    end
  end
end
