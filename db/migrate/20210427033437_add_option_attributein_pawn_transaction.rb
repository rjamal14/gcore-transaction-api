class AddOptionAttributeinPawnTransaction < ActiveRecord::Migration[6.0]
  def change
    add_column :pawn_transactions, :is_option, :integer
  end
end
