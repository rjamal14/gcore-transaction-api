# Transaction Index Serializer
class TransactionIndexSerializer < ActiveModel::Serializer
  attributes :id, :sge, :status, :contract_date, :due_date, :office_id,
             :auction_date, :loan_amount, :product_id, :transaction_type, :admin_fee, :monthly_fee,
             :created_by_id, :updated_by_id, :company, :customer, :estimate_value, :insurance_item_name,
             :maximum_loan, :maximum_loan_percentage, :prolongation_order, :parent_sge, :cif_number, :is_deviation, :tenor
  has_one :customer, only: [:name, :cif_number]
  has_one :company, only: [:name, :cif_number]

  def tenor
    object.try(:installment_detail).try(:tenor)
  end

  def estimate_value
    object.transaction_type == 'installment' ? object.try(:estimated_value) : object.transaction_insurance_items.sum(:estimated_value)
  end

  def cif_number
    object.customer.cif_number
  end

  def is_deviation
    if object.transaction_deviations.empty?
      false
    elsif object.transaction_deviations.rejected.present?
      'rejected'
    elsif object.transaction_deviations.unapproved.present?
      'waiting'
    else
      'ready'
    end
  end
end
