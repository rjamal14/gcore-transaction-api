require 'prawn'
require 'prawn/measurement_extensions'
class PrintRepaymentPdf < Prawn::Document

  def initialize(transaction, office, user)
    super({margin: 0, page_layout: :portrait, page_size: [68.mm, 200.mm] })
    @transaction      = transaction
    @office           = office
    @user           = user

    first_page
  end

  def dot
    text "---------------------------------------------"
  end

  def first_page
    
    wrapper_height = bounds.height
    wrapper_width = bounds.width

    text "PT. Gadai Cahaya Dana Abadi", :size => 9, :style => :bold
    text "#{@office['branch_name']}", :size => 8, :style => :bold

    move_down 5
    text "Nota Transaksi ini merupakan satu kesatuan \n yang tidak terpisahkan dari", :size => 7, :style => :bold
    
    move_down 10
    text "SGE No : #{@transaction.sge}", :size => 8, :style => :bold
    
    move_down 10
    text "NOTA TRANSAKSI", :style => :bold, :size => 7, :align => :center
    text "PELUNASAN GADAI", :style => :bold, :size => 7, :align => :center
    text "#{@transaction.product_name}", :style => :bold, :size => 7, :align => :center
    dot

    move_down 5
    
    data = [
      ['Tgl Pencairan', ": #{@transaction.contract_date.try(:strftime, '%d %b %Y')}"],
      ['Nomor SGE', ": #{@transaction.sge}"],
      ['Pinjaman', ": #{price_format(@transaction.loan_amount.to_i)}"],
    ]

    table(
          data,
          column_widths: { 0 => 55},
          cell_style: {
            inline_format: true,
            size: 7,
            font_style: :bold, 
            padding_bottom: 0,
            border_width: 0
          }
        )
      
    move_down 10
    dot

    data = [
      ['Sewa Modal', ": #{price_format((@transaction.monthly_fee.to_f/2).to_f)} / 15 hari"],
      ['Jml.Hari Real', ": #{rental_time}"],
      ['Jml.Sewa Modal', ": #{price_format(rental_cost)}"],
      ['Kewajiban Bayar', ": #{price_format(rental_cost + @transaction.loan_amount)}"],
    ]

    table(
          data,
          column_widths: { 0 => 71},
          cell_style: {
            inline_format: true,
            size: 7,
            font_style: :bold, 
            padding_bottom: 0,
            border_width: 0
          }
        )

    move_down 20

    text "Barang jaminan :", :size => 8, :style => :bold
    text "#{format_brg(@transaction.transaction_insurance_items)}", :size => 7, :style => :bold

    move_down 20
    
    text "Tanggal Jatuh tempo : #{@transaction.due_date.try(:strftime, '%d %b %Y')}", :size => 8, :style => :bold
    move_down 10
    text "Jika kredit init tidak dilunasi / diperpanjang sampai tanggal jatuh tempo maka barang jaminan akan dilelang mulai", :size => 7, :style => :bold
    move_down 10
    text "Tanggal Lelang : #{@transaction.auction_date.try(:strftime, '%d %b %Y')}", :size => 8, :style => :bold
    
    move_down 30
    text "Terimakasih atas kepercayaan anda", :size => 7, :style => :bold
    move_down 10
    
    
    y_point = cursor
    indent(10) do
      text "Nama Petugas", :size => 7, :style => :bold
      move_down 30
      text "#{@user.first_name} #{@user.last_name}", :size => 7, :style => :bold
      text "#{@user.nik}", :size => 7, :style => :bold
    end

    y2_point = cursor
    move_up y_point - y2_point
    indent(100) do
      text "Nama Nasabah", :size => 7, :style => :bold
      move_down 30
      text "#{@transaction.customer.name}", :size => 7, :style => :bold
    end

    move_down 40
    text "#{Time.now}", :size => 7, :style => :bold
    move_down 10
  end
  
  def price_format(value)
    return "Rp.#{ActionController::Base.helpers.number_to_currency(value, unit: '', strip_insignificant_zeros: true)}"
  end

  def rental_time
    (Date.today - @transaction.contract_date).to_i.zero? ? 1 : (Date.today - @transaction.contract_date).to_i
  end
  
  def rental_cost
    (@transaction.monthly_fee.to_f/2).to_f * (rental_time.to_f / 15).ceil
  end

  def format_brg(n)
    text=''
    n.each do |item|
        text = "#{text}#{item.amount.humanize} #{item.try(:name)} DTM#{strip_trailing_zero(item.carats)}KRT BERAT #{strip_trailing_zero(item.net_weight)}GRM \n"
    end

    text.upcase!
  end

  def strip_trailing_zero(n)
    n.to_s.sub(/\.?0+$/, '')
  end
end