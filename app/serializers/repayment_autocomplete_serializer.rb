# CustomerSerializer
class RepaymentAutocompleteSerializer < ActiveModel::Serializer
  attributes :id, :sge, :name
  def name
    object.try(:customer).try(:name)
  end
end
