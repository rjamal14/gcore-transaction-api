class EditPawnTransaction < ActiveRecord::Migration[6.0]
  def change
    add_column :pawn_transactions, :interest_rate, :float
  end
end
