module Transaction
  # Deviation Service
  class TransactionApprovalCheckService
    def initialize(transaction:)
      @transaction = transaction
      @transaction_role = []
      @transaction_approvals = []
    end

    def check(amount)
      @transaction_role = TransactionRule.where('? >= minimum_amount', amount)
                                         .where('? <= maximum_amount', amount)
                                         .last.try(:transaction_rule_approvals)
    end

    def set_approval
      @transaction_role.map do |v|
        @transaction_approvals.push({
                                      user_type: v.user_role
                                    })
      end
    end

    def execute
      check(@transaction[:loan_amount])
      set_approval
      @transaction.merge(transaction_approvals_attributes: @transaction_approvals)
    end
  end
end
