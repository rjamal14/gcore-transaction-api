# CUstomer Emergency Contact
class CustomerEmergencyContact < ApplicationRecord
  acts_as_paranoid
  belongs_to :customer, -> { with_deleted }, optional: true
end
