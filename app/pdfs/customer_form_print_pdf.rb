require 'prawn'
require 'prawn/measurement_extensions'
class CustomerFormPrintPdf < Prawn::Document
  def initialize(customer, area)
    super({ top_margin: 35, page_layout: :portrait, page_size: 'A4' })
    @customer = customer
    @area = area
    first_page
  end

  def first_page
    wrapper_height = bounds.height
    wrapper_width = bounds.width

    bounding_box([0, bounds.height], width: wrapper_width, height: wrapper_height) do
      y0_point = cursor
      table(
        [
          [{ image: open(@area['logo']['url']), fit: [94, 94], position: :center }]
        ], cell_style: { border_width: 0 }
      )

      y1_point = cursor
      move_up y0_point - y1_point

      indent(wrapper_width/3+10) do 
        move_down 1.cm
        text "FORMULIR GADAI EMAS", :style => :bold, :size => 14
      end
      
      bounding_box([0, y1_point], width: wrapper_width, height: 15) do
        stroke do
            fill_color '36DA5C'
            fill_and_stroke_rounded_rectangle [0,cursor], wrapper_width, 15, 1
            fill_color '000000'
        end
        move_down 3
        text "DATA PRIBADI PEMOHON", :style => :bold, :size => 12, :align => :center
        
        stroke_bounds
      end

      doubledot = ':.........................................................'

      marital_status = case @customer.marital_status
                         when 'single'
                           'Belum kawin'
                         when 'married'
                           'Kawin'
                         when 'divorced'
                           'Cerai'
                       end
      birth_place = @customer.customer_emergency_contacts[0].birth_place || nil
      birth_date = @customer.customer_emergency_contacts[0].birth_date || nil

      emergency_contacts_ttl = if birth_place.present? && birth_date.present?
                                 "#{birth_place}, #{birth_date}"
                               else
                                  doubledot
                               end      


      split_a = make_table([
          [{ content: 'Nama Lengkap (sesuai KTP)', height: 10 }, { content: ": #{@customer.name}"}],
          [{ content: 'Tempat Lahir/Tanggal Lahir', height: 10 }, { content: ": #{@customer.birth_place}, #{@customer.birth_date}"}],
          [{ content: 'Alamat', height: 10 }, { content: ": #{@customer.customer_contact.identity_address}"}],
          [{ content: 'Kelurahan', height: 10 }, { content: ": #{@customer.customer_contact.identity_subdistric_name}"}],
          [{ content: 'Kecamatan', height: 10 }, { content: ": #{@customer.customer_contact.identity_region_name}"}],
          [{ content: 'Kota', height: 10 }, { content: ": #{@customer.customer_contact.identity_city_name}"}],
          [{ content: 'Kode Pos', height: 10 }, { content: ": #{@customer.customer_contact.identity_postal_code}"}],
          [{ content: 'Provinsi', height: 10 }, { content: ": #{@customer.customer_contact.identity_province_name}"}],
          [{ content: 'Status Pernikahan', height: 10 }, { content: ": #{marital_status}"}],
          [{ content: 'Nama Gadis Ibu Kandung', height: 10 }, { content: ": #{@customer.mother_name}"}]
        ],
        width: wrapper_width / 2,
        cell_style: { 
          inline_format: true, 
          border_width: 0,
          size: 8
        }
      )

      split_b = make_table([
          [{ content: 'No. KTP', height: 10 }, { content: ": #{@customer.identity_number}"}],
          [{ content: 'No. NPWP', height: 10 }, { content: ": #{@customer.customer_job.tax_number}"}],
          [{ content: 'Telp. Rumah', height: 10 }, { content: ": #{@customer.customer_contact.telephone_number}"}],
          [{ content: 'HP', height: 10 }, { content: ": #{@customer.customer_contact.phone_number}"}],
          [{ content: '<b>DATA KONTAK</b>', height: 10}, { content: "<b>SUAMI/ISTRI/Saudara yang bisa dihubungi</b>"}],
          [{ content: 'Nama', height: 10 }, { content: ": #{@customer.customer_emergency_contacts[0].name}"}],
          [{ content: 'TTL', height: 10 }, { content: ": #{emergency_contacts_ttl}"}],
          [{ content: 'Pekerjaan', height: 10 }, { content: ": #{@customer.customer_emergency_contacts[0].job_name}"}],
          [{ content: 'Alamat', height: 10 }, { content: ": #{@customer.customer_emergency_contacts[0].address}"}],
          [{ content: 'Telepon', height: 10 }, { content: ": #{@customer.customer_emergency_contacts[0].phone_number}"}],
        ],
        width: wrapper_width / 2,
        cell_style: { 
          inline_format: true, 
          border_width: 0, 
          size: 8
        }
      )

      tabel_split = make_table(
        [
          [split_a, split_b]
        ], 
        column_widths: [wrapper_width/2],
        width: wrapper_width,
        cell_style: { 
          inline_format: true, border_width: 0
        }
      )

      table(
        [
          [tabel_split]
        ], width: wrapper_width, cell_style: { border_width: 1 }
      )

      y2_point=cursor
      bounding_box([0, y2_point], width: wrapper_width, height: 15) do
        stroke do
            fill_color '36DA5C'
            fill_and_stroke_rounded_rectangle [0,cursor], wrapper_width, 15, 1
            fill_color '000000'
        end
        move_down 3
        text "Keterangan Taksasi Barang Gadai Emas", :style => :bold, :size => 12, :align => :center
        
        stroke_bounds
      end
      
      move_down 5.cm
      y3_point=cursor
      
      bounding_box([0, y3_point], width: wrapper_width, height: 60) do

        bounding_box([0, 60], width: wrapper_width/5, height: 60) do
          indent(4) do
            move_down 5
            text "STLE :", :size => 8
          end
          stroke_horizontal_rule
          
          indent(4) do
            move_down 5
            text "Nomor BTE :", :size => 8
          end
          stroke_horizontal_rule
          
          indent(4) do
            move_down 5
            text "Tgl Kredit :", :size => 8
          end
          stroke_horizontal_rule
          
          indent(4) do
            move_down 5
            text "Tgl Jtuh Tempo :", :size => 8
          end

          stroke_bounds
        end
        
        bounding_box([wrapper_width/5, 60], width: wrapper_width/5, height: 60) do
          move_down 5
          text "Penaksir I", :size => 8, :align => :center
          stroke_horizontal_rule
          
          indent(4) do
            move_down 5
            text "Taksiran :", :size => 8
          end
          stroke_horizontal_rule
          
          indent(4) do
            move_down 5
            text "Uang Pinjaman :", :size => 8
          end
          stroke_horizontal_rule

          stroke_bounds
        end

        bounding_box([wrapper_width/5*2, 60], width: wrapper_width/5, height: 60) do
          move_down 5
          text "Penaksir II/KPK", :size => 8, :align => :center
          stroke_horizontal_rule
          
          indent(4) do
            move_down 5
            text "Taksiran :", :size => 8
          end
          stroke_horizontal_rule
          
          indent(4) do
            move_down 5
            text "Uang Pinjaman :", :size => 8
          end
          stroke_horizontal_rule

          stroke_bounds
        end

        bounding_box([wrapper_width/5*3, 60], width: wrapper_width/5, height: 60) do
          move_down 5
          text "Komite Penilai", :size => 8, :align => :center
          stroke_horizontal_rule
          
          indent(4) do
            move_down 5
            text "Taksiran :", :size => 8
          end
          stroke_horizontal_rule
          
          indent(4) do
            move_down 5
            text "Uang Pinjaman :", :size => 8
          end
          stroke_horizontal_rule

          stroke_bounds
        end

        bounding_box([wrapper_width/5*4, 60], width: wrapper_width/5, height: 60) do
          move_down 5
          text "Nasabah", :size => 8, :align => :center
          stroke_horizontal_rule
          
          move_down 30
          text "ttd", :size => 8, :align => :center
          
          
          stroke_bounds
        end
        
        stroke_bounds
      end
      
      y4_point=cursor
      bounding_box([0, y4_point], width: wrapper_width, height: 15) do
        stroke do
            fill_color '36DA5C'
            fill_and_stroke_rounded_rectangle [0,cursor], wrapper_width, 15, 1
            fill_color '000000'
          end
          move_down 3
          text "REFERENSI", :style => :bold, :size => 12, :align => :center
          
          stroke_bounds
      end
        
        move_down 3
        indent(4) do
          text "Pernah menjadi nasabah #{@area['company_name']} :", :size => 8
        end
        stroke_horizontal_rule
        
        move_down 3
        indent(4) do
          text "Tahun :", :size => 8
        end
        stroke_horizontal_rule
        
      y5_point=cursor
      bounding_box([0, y5_point], width: wrapper_width, height: 15) do
        stroke do
            fill_color '36DA5C'
            fill_and_stroke_rounded_rectangle [0,cursor], wrapper_width, 15, 1
            fill_color '000000'
        end
          move_down 3
          text "PERYATAAN", :style => :bold, :size => 12, :align => :center
          
          stroke_bounds
      end
        
        move_down 5

        text_pernyataan = 'Saya menyatakan bahwa semua informasi yang diberikan adalah '\
                          'benar. Informasi ini diberikan untuk tujuan permohonan pembiayaan Gadai, '\
                          "dengan ini memberikan kuasa kepada #{@area['company_name']} untuk "\
                          'mendapatkan dan meneliti seluruh informasi lebih jauh yang diperlukan, '\
                          'dan saya akan memberikan informasi terbaru apabila terdapat perubahan '\
                          'data dalam aplikasi ini. Dengan ini pula saya menyatakan bersedia dan '\
                          'akan mentaati segala persyaratan dan ketentuan yang berlaku di '\
                          "#{@area['company_name']} \n \n"\
                          "Saya mengetahui dan menyetujui bahwa #{@area['company_name']} "\
                          'berhak menolak permohonan pembiayaan Gadai yang diajukan tanpa '\
                          'menyebutkan alasan atau keterangan lainnya.'


        indent(4) do
          text "#{text_pernyataan}", :size => 8
        end

        move_down 30
        indent(300) do
          text "NASABAH", :size => 8, :align => :center
        end

        move_down 40
        indent(300) do
          text "..................................................", :size => 8, :align => :center
        end
        

      stroke_bounds
    end
  end
    
  def image_path(path_name = nil)
    path_name = path_name.present? ? path_name : 'images/logo/mini-gadai-jabar-logo.png'
    "#{Rails.root}/public/#{path_name}"
  end
end
