class AddColumnInPawnTransaction < ActiveRecord::Migration[6.0]
  def change
    add_column :pawn_transactions, :disbursement_status, :integer, null: false, default: 0
  end
end
