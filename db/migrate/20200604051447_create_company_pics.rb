class CreateCompanyPics < ActiveRecord::Migration[6.0]
  def change
    create_table :company_pics do |t|
      t.string :name
      t.integer :company_id
      t.string :position
      t.string :identity_number
      t.string :identity_type
      t.string :phone_number
      t.string :address
      t.datetime :deleted_at, index: true

      t.timestamps
    end
  end
end
