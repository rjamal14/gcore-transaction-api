module Api
  module V1
    # Insurance Item Controller
    class InsuranceItemsController < ApplicationController
      skip_before_action :current_user, only: :total_insurance_item
      def total_insurance_item
        @transactions = PawnTransaction.where(insurance_item_id: params[:insurance_item_id])
        @transactions = filter_by_date(@transactions, params[:date]) if params.key?(:date)
        render json: { total: @transactions.count }
      end

      private

      def filter_by_date(transactions, date)
        date = date.to_i
        from_date = if date.zero?
                      Date.today.beginning_of_month
                    else
                      Date.today - date.months
                    end
        transactions.where(created_at: from_date..Date.today)
      end
    end
  end
end
