Rails.application.routes.draw do
  mount Rswag::Ui::Engine => '/api-docs'
  mount Rswag::Api::Engine => '/api-docs'
  namespace :api do
    namespace :v1 do
      get '/customer/autocomplete' => 'customer#autocomplete'
      get '/transactions/num_of_transactions' => 'transactions#num_of_transactions'
      get '/products/' => 'products#index_of_products'
      get 'transaction_disbursements' => 'transaction_index#index_disbursement'
      get 'transaction_repayments' => 'transaction_index#index_repayment'
      get 'transaction_auctions' => 'transaction_index#index_auction'
      get 'transaction_cancels' => 'transaction_index#index_canceled'
      get 'transaction_time_extensions' => 'transaction_index#index_time_extension'
      get 'installments' => 'transaction_index#index_installment'
      get '/transactions/published' => 'transactions#transaction_published'
      get '/transactions/transaction_outstanding' => 'transactions#transaction_outstanding'
      get 'total_insurance_items' => 'insurance_items#total_insurance_item'
      get '/customer/identity_number/:id', to: 'customer#show_by_id_number'
      get '/transactions/repayment/autocomplete', to: 'transaction_index#repayment_autocomplete'
      get '/transaction_approvals/status/:office_id', to: 'transaction_approvals#check_transaction_status'
      get '/dashboards', to: 'dashboards#show'
      resources :customer do
        resources :customer_notes
        resources :customer_profile
        resources :customer_contact
        member do
          get :customer_form_print
          get :transaction, to: 'customer#show_customer_on_transaction'
        end
      end

      resources :transactions do
        member do
          put :prolongations, to: 'prolongations#create'
          get :prolongations, to: 'prolongations#inquiry'
          get :repayments, to: 'repayments#inquiry'
          put :repayments, to: 'repayments#create'
          get :contract
          get :print_repayment
          get :print_disbursement
        end
      end
      resources :deviations, only: [:index, :show] do
        member do
          put 'approve'
          put 'reject'
        end
      end
      resources :point_rules
      resources :companies do
        resources :company_pics
      end

      resources :transaction_rules
      resources :transaction_approvals, only: :update
      resources :cash_transactions
      resources :cashout_transactions
      resources :installments, only: [:create, :update, :destroy, :show] do
        member do
          get 'inquiry', to: 'installments#monthly_installment_inquiry'
          put 'install', to: 'installments#monthly_installment_payment'
          get 'print', to: 'installments#print_installment'
        end
      end
    end
  end
end
