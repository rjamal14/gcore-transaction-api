class ChangeUserRoleAtTransactionApproval < ActiveRecord::Migration[6.0]
  def change
    change_column :transaction_rule_approvals, :user_role, :string, :null => true
  end
end
