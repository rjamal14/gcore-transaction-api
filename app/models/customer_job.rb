# CUstomer Job Model
class CustomerJob < ApplicationRecord
  acts_as_paranoid
  belongs_to :customer, -> { with_deleted }, optional: true
end
