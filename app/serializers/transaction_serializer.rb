# Transaction Serializer
class TransactionSerializer < ActiveModel::Serializer
  attributes :id, :sge, :office_id, :status, :contract_date, :due_date,
             :auction_date, :loan_amount, :product_id, :transaction_type, :admin_fee, :monthly_fee,
             :created_by_id, :updated_by_id, :transaction_insurance_items, :company, :customer, :estimate_value,
             :insurance_item_name, :insurance_item_id, :parent_sge, :prolongation_order
  has_one :customer, only: [:name, :cif_number]
  has_one :company, only: [:name, :cif_number]
  def estimate_value
    object.transaction_insurance_items.sum(:estimated_value)
  end
end
