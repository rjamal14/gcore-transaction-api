# Module response
module Response
  extend ActiveSupport::Concern

  def response_success(data, message, _added = {})
    render json: { data: data, code: 200, status: :success, message: message }.merge(_added), status: 200
  end

  def response_created(data, message, _added = {})
    render json: { data: data, code: 201, created: true, message: message }.merge(_added), status: 201
  end

  def response_updated(data, message, _added = {})
    render json: { data: data, code: 200, success: true, message: message }.merge(_added), status: 200
  end

  def response_error(data, message, _added = {})
    render json: { status: data.errors, code: 400, success: true, message: message }.merge(_added), status: 400
  end

  def response_bad(message, _added = {})
    render json: { code: 400, message: message, success: false }.merge(_added), status: 400
  end

  def response_ok(message, _added = {})
    render json: { code: 200, success: true, message: message }.merge(_added), status: 200
  end

  def response_not_found(message, _added = {})
    render json: { code: 404, error: 'Not Found', message: message }.merge(_added), status: 404
  end

  def response_index(datas, serial_datas, _added = {})
    render json: { data: serial_datas,
                   code: 200,
                   status: 'success',
                   total_data: datas.total_count,
                   total_page: datas.total_pages,
                   current_page: datas.current_page,
                   prev_page: datas.prev_page,
                   next_page: datas.next_page }.merge(_added), status: 200
  end

  def response_transaction(datas, paginates, serial_datas, _added = {})
    render json: { data: serial_datas,
                   code: 200,
                   status: 'success',
                   total_sge: datas.count,
                   total_loan_amount: datas.total_loan_amount,
                   total_admin_fee: datas.total_admin_fee,
                   total_data: paginates.total_count,
                   total_page: paginates.total_pages,
                   current_page: paginates.current_page,
                   prev_page: paginates.prev_page,
                   next_page: paginates.next_page }.merge(_added), status: 200
  end

  def response_product(datas, transaksi, _added = {})
    render json: { data: transaksi,
                   code: 200,
                   status: 'success',
                   total_data: datas['total_data'],
                   total_page: datas['total_page'],
                   current_page: datas['current_page'],
                   prev_page: datas['prev_page'],
                   next_page: datas['next_page'] }.merge(_added), status: 200
  end

  def response_auth_failed(_added = {})
    render json: { code: '403', status: 'forbidden', message: 'invalid access token' }.merge(_added), status: 403
    nil
  end

  def response_auth_timeout(_added = {})
    render json: { code: '400', status: 'timeout', message: 'connection timeout' }.merge(_added), status: 400
    nil
  end
end
