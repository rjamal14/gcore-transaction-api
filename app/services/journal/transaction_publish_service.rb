# Journal
require 'json'
require 'base64'

module Journal
  class TransactionPublishService
    def initialize(data)
      @queue_name = "#{ENV['rbmq_env']}-journal.transaction.create"
      @transaction = data.attributes
      @transaction['transaction_insurance_items'] = []

      data.transaction_insurance_items.map do |v|
        @transaction['transaction_insurance_items'] << v.attributes
      end
    end

    def execute
      publish
    end

    def channel
      @channel ||= ::Publisher::BunnyPublisher.connection.create_channel
    end

    def exchange
      @exchange ||= channel.exchange(
        "#{ENV['rbmq_env']}-journal",
        type: 'direct',
        durable: true
      )
    end

    def publish
      exchange.publish(@transaction.to_json, routing_key: @queue_name)
      channel.close
    end
  end
end
