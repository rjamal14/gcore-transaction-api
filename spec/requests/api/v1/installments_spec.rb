require 'swagger_helper'

RSpec.describe 'api/v1/installments', type: :request do
  path '/api/v1/installments/{id}/inquiry' do
    # You'll want to customize the parameter types...
    parameter name: 'id', in: :path, type: :string, description: 'ID of Installment Transaction'

    get('Get an Monthly Installment Detail') do
      tags 'Installments'
      security [Bearer: {}]
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/installments/{id}/install' do
    # You'll want to customize the parameter types...
    parameter name: 'id', in: :path, type: :string, description: 'ID of Installment Transaction'
    put('Installing an Monthly Installment') do
      tags 'Installments'
      security [Bearer: {}]
      parameter name: :monthly_installment, in: :body, schema: {
        type: :object,
        properties: {
          payment_amount: { type: :integer }
        },
        required: :payment_amount
      }
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/installments/{id}/print' do
    # You'll want to customize the parameter types...
    parameter name: 'id', in: :path, type: :string, description: 'ID of Installment Transaction'

    put('Print Installment Transaction Detail') do
      tags 'Installments'
      security [Bearer: {}]
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/installments' do
    post('Create Installment Transaction') do
      tags 'Installments'
      security [Bearer: {}]
      parameter name: :installment, in: :body, description: 'Installment Paramater', schema: {
        type: :object,
        example: {
          "installment": {
            "insurance_item_id": '5f6c1cead77fab0cf66c6ff8',
            "insurance_item_name": 'Logam Mulia',
            "customer_id": 112,
            "transaction_type": 'installment',
            "product_id": '5ff6b3f0e64d1e2548b7bdec',
            "product_name": 'Gadai Cicilan',
            "status": 'waiting_approval',
            "contract_date": '2020-09-17',
            "due_date": '2020-10-22',
            "auction_date": '2020-11-11',
            "loan_amount": 1_802_500,
            "maximum_loan_percentage": 82,
            "maximum_loan": 2_000_000,
            "admin_fee": 48_000,
            "monthly_fee": 6000,
            "transaction_insurance_items_attributes":
              [{
                "product_insurance_item_id": '5f6c1cead77fab0cf66c6ff8',
                "name": 'Emas Bulat',
                "ownership": 'Warisan',
                "amount": 1,
                "buy_price": 3_002_500,
                "gross_weight": 1.2,
                "net_weight": 1,
                "carats": 16,
                "estimated_value": 2_062_500,
                "description": 'Emas Dapat dari warisan',
                "insurance_item_image": 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD'
              }],
            "installment_detail_attributes": {
              "down_payment": 500_000,
              "tenor": 3,
              "monthly_interest": 1.2,
              "monthly_fee": 6000,
              "monthly_installment": 106_000
            }
          }
        }
      }
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/installments/{id}' do
    # You'll want to customize the parameter types...
    parameter name: 'id', in: :path, type: :string, description: 'ID of Installment Transaction'

    get('Show Installment Transaction') do
      tags 'Installments'
      security [Bearer: {}]
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    patch('Update Installment Transaction') do
      tags 'Installments'
      security [Bearer: {}]
      parameter name: :installment, in: :body, description: 'Installment Paramater', schema: {
        type: :object,
        example: {
          "installment": {
            "insurance_item_id": '5f6c1cead77fab0cf66c6ff8',
            "insurance_item_name": 'Logam Mulia',
            "customer_id": 112,
            "transaction_type": 'installment',
            "product_id": '5ff6b3f0e64d1e2548b7bdec',
            "product_name": 'Gadai Cicilan',
            "status": 'waiting_approval',
            "contract_date": '2020-09-17',
            "due_date": '2020-10-22',
            "auction_date": '2020-11-11',
            "loan_amount": 1_802_500,
            "maximum_loan_percentage": 82,
            "maximum_loan": 2_000_000,
            "admin_fee": 48_000,
            "monthly_fee": 6000,
            "transaction_insurance_items_attributes":
              [{
                "transaction_insurance_items_id": '5f6c1cead77fab0cf66c6ff8',
                "product_insurance_item_id": '5f6c1cead77fab0cf66c6ff8',
                "name": 'Emas Bulat',
                "ownership": 'Warisan',
                "amount": 1,
                "buy_price": 3_002_500,
                "gross_weight": 1.2,
                "net_weight": 1,
                "carats": 16,
                "estimated_value": 2_062_500,
                "description": 'Emas Dapat dari warisan',
                "insurance_item_image": 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD'
              }],
            "installment_detail_attributes": {
              "installment_detail_id": '5f6c1cead77fab0cf66c6ff8',
              "down_payment": 500_000,
              "tenor": 3,
              "monthly_interest": 1.2,
              "monthly_fee": 6000,
              "monthly_installment": 106_000
            }
          }
        }
      }
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    put('Update Installment Transaction') do
      tags 'Installments'
      security [Bearer: {}]
      parameter name: :installment, in: :body, description: 'Installment Paramater', schema: {
        type: :object,
        example: {
          "installment": {
            "insurance_item_id": '5f6c1cead77fab0cf66c6ff8',
            "insurance_item_name": 'Logam Mulia',
            "customer_id": 112,
            "transaction_type": 'installment',
            "product_id": '5ff6b3f0e64d1e2548b7bdec',
            "product_name": 'Gadai Cicilan',
            "status": 'waiting_approval',
            "contract_date": '2020-09-17',
            "due_date": '2020-10-22',
            "auction_date": '2020-11-11',
            "loan_amount": 1_802_500,
            "maximum_loan_percentage": 82,
            "maximum_loan": 2_000_000,
            "admin_fee": 48_000,
            "monthly_fee": 6000,
            "transaction_insurance_items_attributes":
              [{
                "transaction_insurance_items_id": '5f6c1cead77fab0cf66c6ff8',
                "product_insurance_item_id": '5f6c1cead77fab0cf66c6ff8',
                "name": 'Emas Bulat',
                "ownership": 'Warisan',
                "amount": 1,
                "buy_price": 3_002_500,
                "gross_weight": 1.2,
                "net_weight": 1,
                "carats": 16,
                "estimated_value": 2_062_500,
                "description": 'Emas Dapat dari warisan',
                "insurance_item_image": 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD'
              }],
            "installment_detail_attributes": {
              "installment_detail_id": '5f6c1cead77fab0cf66c6ff8',
              "down_payment": 500_000,
              "tenor": 3,
              "monthly_interest": 1.2,
              "monthly_fee": 6000,
              "monthly_installment": 106_000
            }
          }
        }
      }
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    delete('Delete Installment Transaction') do
      tags 'Installments'
      security [Bearer: {}]
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end
end
