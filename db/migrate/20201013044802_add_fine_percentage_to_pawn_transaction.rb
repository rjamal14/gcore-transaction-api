class AddFinePercentageToPawnTransaction < ActiveRecord::Migration[6.0]
  def change
    add_column :pawn_transactions, :fine_percentage, :float
    add_column :pawn_transactions, :payment_status, :boolean
    add_column :pawn_transactions, :payment_amount, :float
  end
end
