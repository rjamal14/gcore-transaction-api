class CreateInstallmentDetails < ActiveRecord::Migration[6.0]
  def change
    create_table :installment_details do |t|
      t.belongs_to :pawn_transaction
      t.integer :tenor
      t.integer :monthly_installment
      t.float :monthly_interest
      t.integer :monthly_fee
      t.integer :remaining_loan
      t.integer :installment_remaining_order
      t.datetime :deleted_at, index: true

      t.timestamps
    end
  end
end
