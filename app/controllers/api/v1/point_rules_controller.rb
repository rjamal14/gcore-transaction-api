# point_rule Controller
module Api
  module V1
    # point_rule  Controller
    class PointRulesController < ApplicationController
      before_action :set_point_rule, only: [:show, :update, :destroy]

      # GET /point_rule
      def index
        @point_rules = TransactionPointRule.page(params[:page] || 1).per(params[:per_page] || 10)
                                           .order("#{params[:order_by] || 'created_at'} #{params[:order] || 'desc'}")
        @point_rules = if params[:search]
                         @point_rules.search(params[:search])
                       else
                         @point_rules
                       end
        serial_point_rules = @point_rules.map { |point_rules| PointRuleSerializer.new(point_rules, root: false) }

        response_index(@point_rules, serial_point_rules)
      rescue StandardError => e
        response_bad(e)
      end

      def autocomplete
        @point_rules = TransactionPointRule.where('transaction_point_rules.activity_name LIKE :search',
                                                  search: "%#{params[:search]}%")
                                           .select(:activity_name, :point_amount, :type, :id)
        if !@point_rules.blank?
          response_success(@point_rules, 'Data Ditemukan')
        else
          response_success([], 'Data Tidak Ditemukan')
        end
      end

      # GET /point_rule/1
      def show
        point_rule_serial = PointRuleSerializer.new(@point_rule)
        response_success(point_rule_serial, 'Data Ditemukan')
      end

      # POST /point_rule
      def create
        @point_rule = TransactionPointRule.new(point_rule_params.merge(created_by_id: current_user.id))
        @point_rule_serial = PointRuleSerializer.new(@point_rule)

        if @point_rule.save
          response_created(@point_rule_serial, 'Data Kota Tersimpan')
        else
          response_error(@point_rule, 'Data Kota Gagal Tersimpan')
        end
      end

      # PATCH/PUT /point_rule/1
      def update
        if @point_rule.update(point_rule_params.merge(updated_by_id: current_user.id))
          response_updated(@point_rule, 'Data Kota Terupdate')
        else
          response_error(@point_rule, 'Data Kota Gagal Terupdate')
        end
      end

      # DELETE /point_rule/1
      def destroy
        return unless @point_rule.destroy

        response_ok('Data point_rule Terhapus')
        @point_rule.update(deleted_by_id: current_user.id)
      end

      private

      # Use callbacks to share common setup or constraints between actions.
      def set_point_rule
        @point_rule = TransactionPointRule.find(params[:id])
      rescue StandardError
        response_not_found('Data Tidak Ditemukan')
      end

      # Only allow a trusted parameter "white list" through.
      def point_rule_params
        params.require(:point_rule).permit(:activity_name, :type, :point_amount, :expired_date)
      end
    end
  end
end
