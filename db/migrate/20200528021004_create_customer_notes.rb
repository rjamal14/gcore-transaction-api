# CreateCustomerNotes
class CreateCustomerNotes < ActiveRecord::Migration[6.0]
  def change
    create_table :customer_notes do |t|
      t.belongs_to :customer
      t.string :subject
      t.text :note_description
      t.string :created_by_id
      t.string :updated_by_id
      t.string :deleted_by_id
      t.datetime :deleted_at, index: true
      t.timestamps null: false
    end
  end
end
