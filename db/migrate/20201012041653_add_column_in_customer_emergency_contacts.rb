class AddColumnInCustomerEmergencyContacts < ActiveRecord::Migration[6.0]
  def change
    add_column :customer_emergency_contacts, :phone_number, :string
  end
end
