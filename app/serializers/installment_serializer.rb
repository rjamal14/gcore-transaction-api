# Transaction Serializer
class InstallmentSerializer < ActiveModel::Serializer
  attributes :id, :sge, :status, :contract_date, :office_id, :customer, :due_date, :product_id, :insurance_item_id, :insurance_item_name,
             :auction_date, :loan_amount, :estimate_value, :prolongation_period, :prolongation_date, :monthly_fee, :admin_fee, :total_buy_price,
             :next_installment_due_date, :installment_order, :installment_payment_history, :approvals, :download, :installment_date_simulation,
             :is_deviation, :deviation_status, :deviation_approvals, :tenor

  def tenor
    object.try(:installment_detail).try(:tenor)
  end

  def download
    object.status == 'approved' ? "#{ENV['BASE_URL']}/api/v1/installments/#{object.id}/print" : nil
  end

  def deviations
    object.try(:transaction_deviations)
  end

  def is_deviation
    deviations.present?
  end

  def deviation_status
    deviations.unapproved.empty?
  end

  def deviation_approvals
    {
      one_obligor: find_deviation(:one_obligor),
      rental: find_deviation(:rental),
      admin: find_deviation(:admin),
      ltv: find_deviation(:ltv)
      # insurance_item: find_deviation(:insurance_item)
    }
  end

  def find_deviation(type)
    deviations.find_by(deviation_type: type).try(:office_type)
  end

  def total_buy_price
    object.try(:transaction_insurance_items).sum(:buy_price)
  end

  def next_installment_due_date
    object.installment_items.where(status: false).first.try(:due_date)
  end

  def installment_order
    object.try(:installment_detail).try(:installment_remaining_order)
  end

  def customer
    {
      id: cus.try(:id),
      name: cus.name,
      cif_number: cus.cif_number,
      identity_number: cus.identity_number,
      address: cus_contact.try(:residence_address),
      phone_number: cus_contact.try(:phone_number),
      gender: cus.try(:gender)
    }
  end

  def cus
    object.try(:customer)
  end

  def cus_contact
    CustomerContact.with_deleted.find_by(customer_id: cus.id)
  end
  has_one :company, only: [:name, :cif_number]
  has_many :transaction_insurance_items
  has_many :transaction_deviations
  has_one :installment_detail

  def estimate_value
    object.try(:estimated_value)
  end

  def installment_date_simulation
    generate_installment_date_simulation unless object.try(:installment_items).present?
    @date
  end

  def generate_installment_date_simulation
    @date = []
    due_date = Date.today
    object.try(:installment_detail).try(:tenor).times do |i|
      due_date += 30.days
      @date.push({
                   installment_order: i + 1,
                   installment_date: due_date
                 })
    end
  end

  def installment_payment_history
    object.installment_items.order(payment_date: :asc).map do |obj|
      {
        installment_order: obj.installment_order,
        payment_date: obj.payment_date || '-',
        payment_amount: obj.payment_amount || 0,
        due_date: obj.due_date || '-',
        fine_amount: obj.fine_amount || 0,
        payment_status: obj.status
      }
    end
  end
end
