require 'swagger_helper'

RSpec.describe 'api/v1/customer', type: :request do
  path '/api/v1/customer/autocomplete' do
    parameter name: 'query', in: :query, type: :string, description: 'Query'
    parameter name: 'office_id', in: :query, type: :string, description: 'Office ID'

    get('Autocomplete Customer') do
      tags 'Customer'
      security [Bearer: {}]
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/customer/identity_number/{id}' do
    # You'll want to customize the parameter types...
    parameter name: 'id', in: :path, type: :string, description: 'id'

    get('Show by ID Number') do
      tags 'Customer'
      security [Bearer: {}]
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/customer/{id}/customer_form_print' do
    # You'll want to customize the parameter types...
    parameter name: 'id', in: :path, type: :string, description: 'id'

    get('Customer Form Print') do
      tags 'Customer'
      security [Bearer: {}]
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/customer/{id}/transaction' do
    # You'll want to customize the parameter types...
    parameter name: 'id', in: :path, type: :string, description: 'id'

    get('Show Customer on Transaction') do
      tags 'Customer'
      security [Bearer: {}]
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/customer' do
    get('List All Customers') do
      tags 'Customer'
      security [Bearer: {}]

      parameter name: 'page', in: 'query', type: :string
      parameter name: 'per_page', in: 'query', type: :string
      parameter name: 'order_by', in: 'query', type: :string
      parameter name: 'order', in: 'query', type: :string
      parameter name: 'search', in: 'query', type: :string
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    post('Create Customer') do
      tags 'Customer'
      security [Bearer: {}]

      parameter name: :customer, in: :body, description: 'Customer Parameter', schema: {
        type: :object,
        example: {
          'customer': {
            'name': 'Bintang',
            'id_scan_image': 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/...'
          }
        }
      }
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/customer/{id}' do
    # You'll want to customize the parameter types...
    parameter name: 'id', in: :path, type: :string, description: 'id'

    get('Show Detail Customer') do
      tags 'Customer'
      security [Bearer: {}]

      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    patch('Update Customer') do
      tags 'Customer'
      security [Bearer: {}]

      parameter name: :customer, in: :body, description: 'Customer Parameter', schema: {
        type: :object,
        example: {
          "customer": {
            "cif_number": '21464370054',
            "branch_office_id": '123124',
            "name": 'jamal',
            "identity_number": '3214654324234',
            "identity_type": 'ktp',
            "birth_date": '1998-08-04',
            "birth_place": 'bandung',
            "city_id": '1',
            "address": 'jl lurus'
          }
        }
      }
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    put('Update Customer') do
      tags 'Customer'
      security [Bearer: {}]

      parameter name: :customer, in: :body, description: 'Customer Parameter', schema: {
        type: :object,
        example: {
          "customer": {
            "cif_number": '21464370054',
            "branch_office_id": '123124',
            "name": 'jamal',
            "identity_number": '3214654324234',
            "identity_type": 'ktp',
            "birth_date": '1998-08-04',
            "birth_place": 'bandung',
            "city_id": '1',
            "address": 'jl lurus'
          }
        }
      }
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    delete('Delete Customer') do
      tags 'Customer'
      security [Bearer: {}]
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/customer/{customer_id}/customer_profile' do
    # You'll want to customize the parameter types...
    parameter name: 'customer_id', in: :path, type: :string, description: 'id'

    post('Insert Customer Profile') do
      tags 'Customer'
      security [Bearer: {}]

      parameter name: :customer_profile, in: :body, description: 'Customer Profile Parameter', schema: {
        type: :object,
        example: {
          "customer_profile": {
            "marital_status": 'true',
            "mother_name": 'tati',
            "phone_number": '082240321240',
            "nationality": '1',
            "job_name": 'test'
          }
        }
      }
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/customer/{customer_id}/customer_profile/{profile_id}' do
    # You'll want to customize the parameter types...
    parameter name: 'customer_id', in: :path, type: :string, description: 'Customer ID'
    parameter name: 'profile_id', in: :path, type: :string, description: 'Prifile Customer ID'

    patch('Update Customer Profile') do
      tags 'Customer'
      security [Bearer: {}]

      parameter name: :customer_profile, in: :body, description: 'Customer Profile Parameter', schema: {
        type: :object,
        example: {
          "customer_profile": {
            "marital_status": 'true',
            "mother_name": 'tati',
            "phone_number": '082240321240',
            "nationality": '1',
            "job_name": 'test'
          }
        }
      }
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    delete('Delete Customer Profile') do
      tags 'Customer'
      security [Bearer: {}]

      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/customer/{customer_id}/customer_notes' do
    # You'll want to customize the parameter types...
    parameter name: 'customer_id', in: :path, type: :string, description: 'id'

    post('Insert Customer Note') do
      tags 'Customer'
      security [Bearer: {}]

      parameter name: :customer_note, in: :body, description: 'Customer Note Parameter', schema: {
        type: :object,
        example: {
          "customer_note": {
            "subject": 'test',
            "note_description": 'test'
          }
        }
      }
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/customer/{customer_id}/customer_notes/{note_id}' do
    # You'll want to customize the parameter types...
    parameter name: 'customer_id', in: :path, type: :string, description: 'Customer ID'
    parameter name: 'note_id', in: :path, type: :string, description: 'Note ID'

    patch('Update Customer Note') do
      tags 'Customer'
      security [Bearer: {}]

      parameter name: :customer_note, in: :body, description: 'Customer Note Parameter', schema: {
        type: :object,
        example: {
          "customer_note": {
            "subject": 'test',
            "note_description": 'test'
          }
        }
      }
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    delete('Delete Customer Note') do
      tags 'Customer'
      security [Bearer: {}]

      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/customer/{customer_id}/customer_contact' do
    # You'll want to customize the parameter types...
    parameter name: 'customer_id', in: :path, type: :string, description: 'id'

    post('Insert Customer Contact') do
      tags 'Customer'
      security [Bearer: {}]

      parameter name: :customer_contact, in: :body, description: 'Customer Contact Parameter', schema: {
        type: :object,
        example: {
          "customer_contact": {
            "name": 'test',
            "relation": 'test',
            "job_name": 'test',
            "nationality": 'test',
            "city_id": '1',
            "address": 'test'
          }
        }
      }
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/customer/{customer_id}/customer_contact/{contact_id}' do
    # You'll want to customize the parameter types...
    parameter name: 'customer_id', in: :path, type: :string, description: 'Customer ID'
    parameter name: 'contact_id', in: :path, type: :string, description: 'Contact ID'

    patch('Update Customer Contact') do
      tags 'Customer'
      security [Bearer: {}]

      parameter name: :customer_contact, in: :body, description: 'Customer Contact Parameter', schema: {
        type: :object,
        example: {
          "customer_contact": {
            "name": 'test',
            "relation": 'test',
            "job_name": 'test',
            "nationality": 'test',
            "city_id": '1',
            "address": 'test'
          }
        }
      }
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    delete('Delete Customer Contact') do
      tags 'Customer'
      security [Bearer: {}]

      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end
end
