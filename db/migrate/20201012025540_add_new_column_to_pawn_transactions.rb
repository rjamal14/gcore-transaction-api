class AddNewColumnToPawnTransactions < ActiveRecord::Migration[6.0]
  def change
    add_column :pawn_transactions, :parent_id, :integer
    add_column :pawn_transactions, :product_name, :string
  end
end
