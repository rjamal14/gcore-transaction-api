# Model Transaction
class PawnTransaction < ApplicationRecord
  include PgSearch::Model
  pg_search_scope :search, against: [:sge],
                           using: {
                             tsearch: {
                               prefix: true
                             }
                           }
  acts_as_paranoid

  before_create :set_interest_rate
  before_update :update_rate

  enum status: [:approved, :waiting_approval, :published]
  enum transaction_type: [:disbursement, :repayment, :auction, :prolongation, :cancelled, :installment]
  enum is_option: [:daily, :weekly, :monthly]

  scope :customer, -> { where(company_id: nil) }
  scope :company, -> { where(customer_id: nil) }
  scope :repayment, -> { where.not(transaction_type: :repayment) }
  scope :active, -> { where(status: :published) }
  scope :active_transaction, -> { where(status: :published, payment_status: false) }

  attribute :approvals
  attribute :parent_id, :integer
  attribute :insurance_item_id, :string
  attribute :office_id, :string
  attribute :office_type, :string
  attribute :customer_id, :integer
  attribute :company_id, :integer
  attribute :product_id, :string
  attribute :product_name, :string
  attribute :status
  attribute :transaction_type, default: :disbursement
  attribute :sge, :string
  attribute :contract_date, :date
  attribute :due_date, :date
  attribute :auction_date, :date
  attribute :loan_amount, :integer
  attribute :admin_fee, :integer
  attribute :monthly_fee, :integer
  attribute :prolongation_date, :date
  attribute :prolongation_period, :integer, default: nil
  attribute :created_by_id, :string
  attribute :updated_by_id, :string
  attribute :deleted_by_id, :string
  attribute :payment_status, default: false
  attribute :interest_rate
  attr_accessor :changed

  has_many :transaction_insurance_items, dependent: :destroy, index_errors: true
  has_many :transaction_deviations, index_errors: true, dependent: :destroy
  has_many :transaction_approvals, index_errors: true, dependent: :destroy
  has_many :installment_items, index_errors: true, dependent: :destroy
  has_one :installment_detail, dependent: :destroy
  belongs_to :customer, -> { with_deleted }, optional: true
  belongs_to :company, -> { with_deleted }, optional: true
  accepts_nested_attributes_for :transaction_insurance_items, allow_destroy: true
  accepts_nested_attributes_for :transaction_deviations, allow_destroy: true
  accepts_nested_attributes_for :transaction_approvals, allow_destroy: true
  accepts_nested_attributes_for :installment_detail, allow_destroy: true
  accepts_nested_attributes_for :installment_items, allow_destroy: true

  validate :cust_validation
  validates_presence_of :sge, :admin_fee, :auction_date, :due_date, :contract_date, :insurance_item_id, :loan_amount,
                        :monthly_fee, :product_id, :product_name, :status, :insurance_item_name
  validates_uniqueness_of :sge

  scope :total_admin_fee, -> { sum(:admin_fee) }
  scope :total_loan_amount, -> { sum(:loan_amount) }

  belongs_to :parent, -> { with_deleted }, class_name: 'PawnTransaction', optional: true
  has_one :prolongation, class_name: 'PawnTransaction', foreign_key: 'parent_id'
  has_one :repayment, class_name: 'PawnTransaction', foreign_key: 'parent_id'

  accepts_nested_attributes_for :prolongation
  accepts_nested_attributes_for :repayment

  def cust_validation
    # XOR operator more information here http://en.wikipedia.org/wiki/Xor
    errors.add(:customer, 'only customer, or company accepted') unless customer_id.blank? ^ company_id.blank?
  end

  def set_interest_rate
    return unless interest_rate.nil?

    self.interest_rate = monthly_fee.to_f / loan_amount * 100
  end

  def update_rate
    return unless status == 'waiting_approval' || deleted_at.nil?

    return unless loan_amount_changed? || interest_rate_changed?

    self.monthly_fee = loan_amount.to_f * interest_rate / 100
    self.per_15_days_fee = loan_amount.to_f * interest_rate / 200
  end
end
