class AddNewAttributeInInstallmentDetail < ActiveRecord::Migration[6.0]
  def change
    add_column :installment_details, :down_payment, :integer
  end
end
