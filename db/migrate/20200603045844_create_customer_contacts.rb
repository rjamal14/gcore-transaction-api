class CreateCustomerContacts < ActiveRecord::Migration[6.0]
  def change
    create_table :customer_contacts do |t|
      t.belongs_to :customer
      t.string :residence_address, null: false
      t.string :residence_province, null: false
      t.string :residence_city, null: false
      t.string :residence_region, null: false
      t.string :residence_postal_code, null: false
      t.string :identity_address, null: false
      t.string :identity_province, null: false
      t.string :identity_city, null: false
      t.string :identity_region, null: false
      t.string :identity_postal_code, null: false
      t.string :phone_number, null: false
      t.string :telephone_number
      t.datetime :deleted_at, index: true
      t.timestamps
    end
  end
end
