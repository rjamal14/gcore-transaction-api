class CreateCashoutAccount < ActiveRecord::Migration[6.0]
  def change
    create_table :cashout_accounts do |t|
      t.belongs_to :cashout_transaction
      t.string :coa
      t.text :description
      t.integer :type_account
      t.timestamps
    end
  end
end
