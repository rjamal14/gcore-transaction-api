# CashoutAccount
class CashoutAccount < ApplicationRecord
  enum type_account: [:debit, :credit, :account]
  validates_presence_of :cashout_transaction, :coa, :description, :type_account

  belongs_to :cashout_transaction, -> { with_deleted }
end
