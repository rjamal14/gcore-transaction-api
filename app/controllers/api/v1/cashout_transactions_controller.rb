# cashout_transaction Controller
module Api
  module V1
    # cashout_transaction  Controller
    class CashoutTransactionsController < ApplicationController
      before_action :set_cashout_transaction, only: [:show, :update, :destroy]
      before_action :get_by_office_id, only: :index

      def index
        @cashout_transactions = @cashout_transactions.page(params[:page] || 1).per(params[:per_page] || 10)
                                                     .order("#{params[:order_by] || 'created_at'} #{params[:order] || 'desc'}")
        @cashout_transactions = if params[:search]
                                  @cashout_transactions.search(params[:search])
                                else
                                  @cashout_transactions
                                end

        serial_cashout_transactions = @cashout_transactions.map do |cashout_transaction|
          CashoutTransactionsSerializer.new(
            cashout_transaction, root: false
          )
        end

        response_index(@cashout_transactions, serial_cashout_transactions)
      end

      def create
        @cashout_transaction = CashoutTransaction.new(
          cashout_transaction_params.merge(
            status: 'approved',
            user_id: current_user.id,
            user_name: "#{current_user.first_name} #{current_user.last_name}",
            created_by_id: current_user.id,
            office_id: current_user.office_id
          )
        )
        cashier_status = check_cashier_report('non_transactional', @cashout_transaction)
        return true if cashier_status.present?

        if @cashout_transaction.save
          Journal::CashoutPublishService.new(@cashout_transaction).execute
          response_created(@cashout_transaction, 'Data CashoutTransaction Tersimpan')
        else
          response_error(@cashout_transaction, 'Data CashoutTransaction Gagal Tersimpan')
        end
      end

      def show
        cashout_transaction = CashoutTransactionsSerializer.new(@cashout_transaction)
        response_success(cashout_transaction, 'Data Ditemukan')
      end

      private

      def get_by_office_id
        @cashout_transactions = if current_user.office_type == 'head'
                                  CashoutTransaction
                                else
                                  CashoutTransaction.where(office_id: current_user.office_id)
                                end

        @cashout_transactions = @cashout_transactions.ransack(params[:q]).result
      end

      def set_cashout_transaction
        @cashout_transaction = CashoutTransaction.find(params[:id])
      rescue StandardError
        response_not_found('Data Tidak Ditemukan')
      end

      def cashout_transaction_params
        params.require(:cashout_transaction).permit(
          :date, :ref, :amount, :description,
          cashout_accounts_attributes: [
            :id, :coa, :description,
            :type_account, :_destroy
          ]
        )
      end
    end
  end
end
