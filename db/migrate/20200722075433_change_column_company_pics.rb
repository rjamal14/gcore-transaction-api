class ChangeColumnCompanyPics < ActiveRecord::Migration[6.0]
  def change
    remove_column :company_pics, :identity_type
    add_column :company_pics, :identity_type, :integer
  end
end
