require 'swagger_helper'

RSpec.describe 'api/v1/cashout_transactions', type: :request do
  path '/api/v1/cashout_transactions' do
    get('Get List Cashout Transactions') do
      tags 'Cashout'
      security [Bearer: {}]

      parameter name: 'page', in: 'query', type: :string
      parameter name: 'per_page', in: 'query', type: :string
      parameter name: 'order_by', in: 'query', type: :string
      parameter name: 'order', in: 'query', type: :string
      parameter name: 'search', in: 'query', type: :string
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    post('Create Cashout Transaction') do
      tags 'Cashout'
      security [Bearer: {}]

      parameter name: :cashout, in: :body, description: 'Cashout Parameter', schema: {
        type: :object,
        example: {
          "cash_transaction": {
            "req_subject": 'Pembelian ATK',
            "req_description": 'Pembelian Map kertas a4 dll',
            "req_coa": '345859',
            "req_amount": 40_000,
            "applicant_name": 'jamal'
          }
        }
      }
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/cashout_transactions/{id}' do
    # You'll want to customize the parameter types...
    parameter name: 'id', in: :path, type: :string, description: 'id'

    get('Show Cashout Transaction') do
      tags 'Cashout'
      security [Bearer: {}]

      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end
end
