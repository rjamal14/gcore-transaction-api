module Installment
  class GenerateMonthlyDueDateService
    def initialize(installment)
      @installment = installment
    end

    def create_installment_item
      due_date = Date.today
      @due_date_arr = []
      @installment_items_attributes = []
      @installment.installment_detail.tenor.times do |i|
        due_date += 30.days
        @due_date_arr.push(due_date)
        @installment_items_attributes.push({ installment_order: i + 1,
                                             due_date: due_date })
      end
    end

    def execute
      create_installment_item
      @installment.update(installment_items_attributes: @installment_items_attributes)
      @due_date_arr
    end
  end
end
