# Company Detail
class CompanyDetailSerializer < ActiveModel::Serializer
  attributes :id, :name, :cif_number, :area_code, :phone_number, :branch_office_name, :branch_office_id, :status, :tax_number,
             :city_name, :city_id, :address, :created_by_id, :updated_by_id, :company_pics
end
