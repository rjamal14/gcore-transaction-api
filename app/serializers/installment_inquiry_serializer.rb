# Transaction Serializer
class InstallmentInquirySerializer < ActiveModel::Serializer
  attributes :id, :customer_name, :cif, :sge, :remaining_loan, :installment_amount, :payment_amount, :installment_fee,
             :installment_interest, :installment_order, :fine_amount

  def customer_name
    object.try(:customer).try(:name)
  end

  def remaining_loan
    object.try(:installment_detail).try(:remaining_loan)
  end

  def installment_amount
    object.try(:installment_detail).try(:monthly_installment) - object.try(:installment_detail).try(:monthly_fee)
  end

  def payment_amount
    object.try(:installment_detail).try(:monthly_installment)
  end

  def installment_fee
    object.try(:installment_detail).try(:monthly_fee)
  end

  def installment_interest
    object.try(:installment_detail).try(:monthly_interest)
  end

  def cif
    object.try(:customer).try(:cif_number)
  end

  def fine_amount
    0
  end

  def installment_order
    object.try(:installment_detail).try(:installment_remaining_order)
  end
end
