class Add < ActiveRecord::Migration[6.0]
  def change
    remove_column :transaction_deviations, :status, :boolean
    add_column :transaction_deviations, :status, :integer
    add_column :transaction_deviations, :rejected_reason, :string
    add_column :transaction_deviations, :value, :integer
    add_index :transaction_deviations, :status
    add_index :transaction_deviations, :office_id
    add_index :transaction_deviations, :deviation_type
  end
end
