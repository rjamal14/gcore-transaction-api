# Point Rule Model
class TransactionPointRule < ApplicationRecord
  include PgSearch
  pg_search_scope :search, against: [:activity_name],
                           using: {
                             tsearch: {
                               prefix: true
                             }
                           }
  acts_as_paranoid
  validates_presence_of :type, :activity_name, :point_amount, :expired_date
end
