# customer Controller
module Api
  module V1
    # customer  Controller
    class CustomerController < ApplicationController
      before_action :set_customer, only: [:show, :update, :destroy, :customer_form_print, :show_customer_on_transaction]
      before_action :check_branch_office, only: :index
      before_action :get_city_name, only: :index
      before_action :set_address, only: [:show, :customer_form_print]
      before_action :check_token, only: :customer_form_print
      skip_before_action :current_user, only: [:customer_form_print, :autocomplete]

      # GET /customer
      def index
        @customers = @customers.page(params[:page] || 1).per(params[:per_page] || 10)
                               .order("#{params[:order_by] || 'created_at'} #{params[:order] || 'desc'}")
        @customers = if params[:search]
                       @customers.search(params[:search])
                     else
                       @customers
                     end
        serial_customers = @customers.map do |customers|
          city_name = @city.detect { |v| v['id'] == customers.customer_contact.residence_city }['name']
          customers.customer_contact.residence_city_name = city_name
          CustomerSerializer.new(
            customers, root: false
          )
        end

        response_index(@customers, serial_customers)
      rescue StandardError => e
        response_bad(e)
      end

      def show_by_id_number
        customer = Customer.find_by_identity_number(params[:id])
        customer = CustomerTransactionSerializer.new(customer, root: false)
        response_success(customer, 'Data Nasabah DItemukan')
      end

      def show_customer_on_transaction
        customer = CustomerTransactionSerializer.new(@customer, root: false)
        response_success(customer, 'Data Ditemukan')
      end

      # GET /customer/autocomplete
      def autocomplete
        @customers = Customer.where(status: true, branch_office_id: params[:office_id])
        @customers = if params[:query]
                       field = params[:query_type] || 'name'
                       @customers.where("#{field} ILIKE ?", "%#{params[:query]}%")
                     else
                       @customers
                     end
        @customers = @customers.limit(10)
                               .order(created_at: :desc)
        @customers = @customers.map do |customer|
          CustomerAutocompleteSerializer.new(customer, root: false)
        end
        response_success(@customers, 'Data ditemukan')
      end

      # GET /customer/1
      def show
        render json: @customer, serializer: CustomerDetailSerializer
      end

      # POST /customer
      def create
        @customer = Customer.new(customer_params.merge(created_by_id: current_user.id,
                                                       created_by_name: "#{current_user.first_name} #{current_user.last_name}",
                                                       branch_office_id: current_user.office_id,
                                                       branch_office_name: current_user.office_name,
                                                       cif_number: generate_number('cif')))

        if @customer.save
          response_created(@customer, 'Data Customer Tersimpan')
        else
          response_error(@customer, 'Data Customer Gagal Tersimpan')
        end
      end

      # PATCH/PUT /customer/1
      def update
        if @customer.update(customer_params.merge(updated_by_id: current_user.id))
          response_updated(@customer, 'Data Customer Terupdate')
        else
          response_error(@customer, 'Data Customer Gagal Terupdate')
        end
      end

      # DELETE /customer/1
      def destroy
        return unless @customer.destroy

        response_ok('Data company Terhapus')
        @customer.update(deleted_by_id: current_user.id)
      end

      def customer_form_print
        respond_to do |format|
          area = get_master("areas/logo_company?office_id=#{@customer.branch_office_id}")

          format.pdf do
            pdf = CustomerFormPrintPdf.new(@customer, area['data'])

            send_data(
              pdf.render,
              filename: "CUSTOMER-(#{@customer.cif_number})",
              type: 'application/pdf', disposition: 'inline'
            )
          end
        end
      end

      private

      def check_token
        auth.set_token("Bearer #{params[:token]}")
        render json: { message: 'Unauthorize' }, status: :unauthorized unless auth.current_user.present?
      end

      def set_address
        data_identity = {
          province: @customer.customer_contact.identity_province,
          city: @customer.customer_contact.identity_city,
          region: @customer.customer_contact.identity_region,
          subdistric: @customer.customer_contact.identity_subdistric
        }

        data_residence = {
          province: @customer.customer_contact.residence_province,
          city: @customer.customer_contact.residence_city,
          region: @customer.customer_contact.residence_region,
          subdistric: @customer.customer_contact.residence_subdistric
        }

        data_identity = get_master("address?#{data_identity.to_query}")['data']
        data_residence = get_master("address?#{data_residence.to_query}")['data']

        initial_address('identity', data_identity.present? ? data_identity : '')
        initial_address('residence', data_residence.present? ? data_residence : '')
      end

      def check_branch_office
        @customers = if current_user.office_type == 'head'
                       Customer
                     else
                       Customer.where(branch_office_id: current_user.office_id)
                     end
        return response_index(@customers, @customers) if @customers.nil?

        @customers = @customers.ransack(params[:q]).result
      end

      def initial_address(type, data = '')
        initial = %w[province_name city_name
                     region_name subdistric_name]

        initial.each do |attrib|
          @customer.customer_contact.update("#{type}_#{attrib}": data[attrib].present? ? data[attrib] : nil)
        end
      end

      def get_city_name
        data_city = []

        @customers.map do |v|
          data_city.push(v.customer_contact.residence_city)
        end

        data_city = { data: data_city }.to_query
        @city = data_city.present? ? get_master("city/multi?#{data_city}")['cities'] : nil
      end

      def set_customer
        @customer = Customer.find(params[:id])
      rescue StandardError
        response_not_found('Data Tidak Ditemukan')
      end

      # Only allow a trusted parameter "white list" through.
      def customer_params
        params.require(:customer).permit(:name, :email,
                                         :identity_number, :identity_type, :expired_date, :birth_date, :degree,
                                         :id_scan_image, :mother_name, :birth_place, :gender, :status, :marital_status, :reason,
                                         customer_contact_attributes: [:id, :_destroy, :residence_address, :residence_province, :residence_city,
                                                                       :residence_region, :residence_postal_code, :residence_subdistric, :identity_address,
                                                                       :identity_province, :identity_city, :identity_region, :identity_subdistric,
                                                                       :identity_postal_code, :phone_number, :telephone_number],
                                         customer_job_attributes: [:id, :_destroy, :job_type, :tax_number, :income_source,
                                                                   :disbursement_type, :customer_type, :financing_purpose, :salary_amount],
                                         customer_notes_attributes: [:id, :_destroy, :subject, :note_description],
                                         customer_emergency_contacts_attributes: [:id, :_destroy, :name, :relation, :nationality,
                                                                                  :job_name, :address, :phone_number, :birth_place, :birth_date],
                                         customer_bank_accounts_attributes: [:id, :_destroy, :account_name, :account_number, :main_account, :bank_name, :branch_name],
                                         transaction_points_attributes: [:transaction_point_rule_id])
      end
    end
  end
end
