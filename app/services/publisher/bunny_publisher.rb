module Publisher
  class BunnyPublisher
    def self.connection
      @connection ||= start_connection
    end

    def self.start_connection
      Bunny.new(
        host: ENV['rbmq_host'],
        vhost: ENV['rbmq_vhost'],
        user: ENV['rbmq_user'],
        password: ENV['rbmq_password'],
        logger: Rails.logger
      ).start
    end
  end
end
