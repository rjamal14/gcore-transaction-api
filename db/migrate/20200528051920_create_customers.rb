# CreateCustomers
class CreateCustomers < ActiveRecord::Migration[6.0]
  def change
    create_table :customers do |t|
      t.string :cif_number, null: false
      t.string :branch_office_id, null: false
      t.string :branch_office_name
      t.string :name, null: false
      t.string :degree, null: false
      t.string :identity_number, null: false
      t.integer :identity_type, null: false
      t.date :expired_date
      t.date :birth_date, null: false
      t.string :birth_place, null: false
      t.string :id_scan_image
      t.string :mother_name, null: false
      t.boolean :marital_status, null: false
      t.integer :gender
      t.boolean :status, default: false
      t.string :created_by_id
      t.string :updated_by_id
      t.string :deleted_by_id
      t.datetime :deleted_at, index: true
      t.timestamps
    end
  end
end
