# Company
class CompanySerializer < ActiveModel::Serializer
  attributes :id, :name, :cif_number, :phone_number, :branch_office_name, :os, :status
  def os
    object.pawn_transactions.active.sum(:loan_amount)
  end
end
