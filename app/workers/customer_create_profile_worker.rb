class CustomerCreateProfileWorker
  include Sneakers::Worker

  queue_name = "#{Rails.env}-migrations.customers.profile.create"
  from_queue queue_name, arguments: { 'x-dead-letter-exchange': "#{queue_name}-retry" }

  def work(msg)
    data = ActiveSupport::JSON.decode(msg)
    data_customer = {
      'prior_customer_id' => data['prior_customer_id'],
      'residence_address' => data['address'],
      'residence_province' => data['province_id'],
      'residence_city' => data['city_id'],
      'residence_region' => data['district_id'],
      'residence_postal_code' => data['postal_code'],
      'identity_address' => data['address'],
      'identity_province' => data['province_id'],
      'identity_city' => data['city_id'],
      'identity_region' => data['district_id'],
      'identity_postal_code' => data['postal_code'],
      'phone_number' => data['phone_number'],
      'telephone_number' => nil
    }
    CustomerContact.new(data_customer.to_h).save!
    ack!
  rescue StandardError => e
    logger.fatal "Error message CustomerCreateProfileWorker => #{e.message}"
    reject!
  end
end
