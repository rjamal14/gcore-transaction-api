class AddNewFieldInTransactionInsuranceItem < ActiveRecord::Migration[6.0]
  def change
    add_column :transaction_insurance_items, :buy_price, :integer
  end
end
