class CreateTransactionRuleApprovals < ActiveRecord::Migration[6.0]
  def change
    create_table :transaction_rule_approvals do |t|
      t.belongs_to :transaction_approval
      t.integer :user_role
      t.datetime :deleted_at, index: true
      t.timestamps
    end
  end
end
