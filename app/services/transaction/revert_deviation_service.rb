# Deviation
module Transaction
  # Deviation Service
  class RevertDeviationService
    def initialize(deviation, message)
      @deviation = deviation
      @transaction = deviation.pawn_transaction
      @deviations = @transaction.transaction_deviations
      @message = message
    end

    def reject_deviation
      @deviation.update(rejected_reason: @message, status: :rejected)
    end

    def revert_admin
      @transaction.update(admin_fee: @deviation.value)
    end

    def revert_ltv
      dev_value = @deviation.value * @transaction.transaction_insurance_items.sum(:estimated_value) / 100

      loan_amount = @transaction.loan_amount <= dev_value ? @transaction.loan_amount : dev_value
      ltv = @transaction.loan_amount <= dev_value ? @transaction.maximum_loan_percentage : @deviation.value
      @transaction.update(maximum_loan_percentage: ltv,
                          loan_amount: loan_amount)
    end

    def revert_rental
      @transaction.update(interest_rate: @deviation.value)
    end

    def revert_one_obligor
      estimated_value = @transaction.transaction_insurance_items.sum(:estimated_value)
      loan_amount = @deviation.value - @transaction.customer.pawn_transactions.active_transaction.sum(:loan_amount)
      @transaction.update(loan_amount: loan_amount,
                          maximum_loan_percentage: loan_amount / estimated_value.to_f * 100)
    end

    def execute
      reject_deviation
      case @deviation.deviation_type
      when 'rental'
        revert_rental
      when 'ltv'
        revert_ltv
      when 'admin'
        revert_admin
      when 'one_obligor'
        revert_one_obligor
      else
        raise StandardError, 'Kesalahan Sistem'
      end
    end
  end
end
