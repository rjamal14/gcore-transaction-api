class CreateTransactionPoints < ActiveRecord::Migration[6.0]
  def change
    create_table :transaction_points do |t|
      t.belongs_to :customer
      t.belongs_to :transaction_point
      t.datetime :deleted_at, index: true

      t.timestamps
    end
  end
end
