class CreateTransactionRules < ActiveRecord::Migration[6.0]
  def change
    create_table :transaction_rules do |t|
      t.integer :minimum_amount
      t.integer :maximum_amount
      t.string :updated_by_id
      t.string :deleted_by_id
      t.datetime :deleted_at, index: true
      t.timestamps
    end
  end
end
