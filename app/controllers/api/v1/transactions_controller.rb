# transaction Controller
module Api
  module V1
    # transaction  Controller
    class TransactionsController < ApplicationController
      before_action :set_pawn_transaction, only: [:show, :update, :destroy, :contract, :print_repayment, :print_disbursement]
      before_action :check_approval, only: :contract
      before_action :check_published, only: :contract
      before_action :set_residence_address, only: [:contract, :print_repayment, :print_disbursement]
      before_action :check_type_of_transaction, only: :print_disbursement
      before_action :check_office, only: [:transaction_published, :transaction_outstanding]
      before_action :check_created_by_id, only: [:print_disbursement, :print_repayment]
      skip_before_action :current_user, only: [:contract, :print_repayment, :print_disbursement, :transaction_published,
                                               :transaction_outstanding]
      before_action :set_deviation, only: :create

      # GET /transaction
      def index
        @pawn_transactions = set_transactions_type.page(params[:page] || 1).per(params[:per_page] || 10)
                                                  .order("#{params[:order_by] || 'created_at'} #{params[:order] || 'desc'}")
        @pawn_transactions = if params[:search]
                               @pawn_transactions.search(params[:search])
                             else
                               @pawn_transactions
                             end
        serial_pawn_transactions = @pawn_transactions.map { |pawn_transactions| PawnTransactionSerializer.new(pawn_transactions, root: false) }

        response_index(@pawn_transactions, serial_pawn_transactions)
      rescue StandardError => e
        response_bad(e)
      end

      # GET /transaction/1
      def show
        @pawn_transaction.approvals = []
        @pawn_transaction.transaction_approvals.map do |k|
          next unless current_user.user_role['role_id']['$oid'] == k.user_type && k.user_id.nil?

          @pawn_transaction.approvals.push({
                                             'approval_type' => k.user_type,
                                             'approval_url' => "#{ENV['BASE_URL']}/api/v1/transaction_approvals/#{k.id}"
                                           })
        end
        installment_serial = TransactionDetailSerializer.new(@pawn_transaction, root: false)
        response_success(installment_serial, 'Data Ditemukan')
      end

      # POST /transaction
      def create
        @pawn_transaction_params = Transaction::TransactionApprovalCheckService.new(transaction: pawn_transaction_params).execute
        @pawn_transaction = PawnTransaction.new(@pawn_transaction_params.merge(disbursement_added_attributes))
        @pawn_transaction_serial = PawnTransactionSerializer.new(@pawn_transaction)

        cashier_status = check_cashier_status(type: 'credit', balance: @pawn_transaction.loan_amount)
        return true if cashier_status.present?

        if @pawn_transaction.save
          send_notification unless @pawn_transaction.transaction_deviations.present?
          response_created(@pawn_transaction_serial, 'Data Transaksi Tersimpan')
        else
          response_error(@pawn_transaction, 'Data Transaksi Gagal Tersimpan')
        end
      end

      # PATCH/PUT /transaction/1
      def update
        if @pawn_transaction.status == 'approved'
          response_bad('Status Transaksi sudah disetujuai, tidak bisa di update')
        else
          @pawn_transaction_serial = PawnTransactionSerializer.new(@pawn_transaction)
          if @pawn_transaction.update(pawn_transaction_params.merge(updated_by_id: current_user.id))
            response_updated(@pawn_transaction_serial, 'Data Transaksi Terupdate')
          else
            response_error(@pawn_transaction_serial, 'Data Transaksi Gagal Terupdate')
          end
        end
      end

      # DELETE /transaction/1
      def destroy
        return unless @pawn_transaction.destroy

        response_ok('Data transaction Terhapus')
        @pawn_transaction.update(deleted_by_id: current_user.id)
      end

      def contract
        respond_to do |format|
          office = get_master("offices/#{@pawn_transaction.office_id}?office_type=#{@pawn_transaction.office_type}")

          format.pdf do
            pdf = CustomerContractPdf.new(@pawn_transaction, office['offices']['data'], 'sign')

            send_data(
              pdf.render,
              filename: "E-SBG-(#{@pawn_transaction.sge})",
              type: 'application/pdf', disposition: 'inline'
            )
          end
        end
      end

      def print_repayment
        respond_to do |format|
          office = get_master("offices/#{@pawn_transaction.office_id}?office_type=#{@pawn_transaction.office_type}")

          format.pdf do
            pdf = PrintRepaymentPdf.new(@pawn_transaction, office['offices']['data'], @user_created)
            send_data(
              pdf.render,
              filename: "E-SBG-(#{@pawn_transaction.sge})",
              type: 'application/pdf', disposition: 'inline'
            )
          end
        end
      end

      def print_disbursement
        respond_to do |format|
          office = get_master("offices/#{@pawn_transaction.office_id}?office_type=#{@pawn_transaction.office_type}")

          format.pdf do
            pdf = PrintDisbursementPdf.new(@pawn_transaction, office['offices']['data'], @user_created)
            send_data(
              pdf.render,
              filename: "E-SBG-(#{@pawn_transaction.sge})",
              type: 'application/pdf', disposition: 'inline'
            )
          end
        end
      end

      def transaction_published
        params[:q] ||= {}
        params[:q][:created_at_lteq] = params[:q][:created_at_lteq].to_date.end_of_day if params[:q][:created_at_lteq].present?

        transaction_data = Transaction::TransactionListReportService.new(
          transactions: @transactions,
          params: params
        ).get_data

        @transactions = transaction_data[:transaction]
        @transaction_type = transaction_data[:transaction_type_translate]
        transaction_paginate = transaction_data[:transaction_paginate]
        serial_transactions = transaction_data[:transaction_serialize]

        respond_to do |format|
          format.xlsx do
            render xlsx: 'transaction_published',
                   template: 'xlsx/transaction_published.xlsx.axlsx'
          end

          format.json { response_transaction(@transactions, transaction_paginate, serial_transactions) }
        end
      end

      def transaction_outstanding
        # initial
        params[:q] ||= {}
        params[:q][:created_at_lteq] = params[:q][:created_at_lteq].to_date.end_of_day if params[:q][:created_at_lteq].present?
        params[:type] = 'outstanding'

        transaction_data = Transaction::TransactionListReportService.new(
          transactions: @transactions,
          params: params
        ).get_data

        @transactions = transaction_data[:transaction]
        transactions_paginate = transaction_data[:transaction_paginate]
        serial_transactions = transaction_data[:transaction_serialize]

        respond_to do |format|
          format.xlsx do
            render xlsx: 'transaction_outstanding',
                   template: 'xlsx/transaction_outstanding.xlsx.axlsx'
          end

          format.json { response_transaction(@transactions, transactions_paginate, serial_transactions) }
        end
      end

      private

      def disbursement_added_attributes
        {
          created_by_id: current_user.id,
          sge: generate_number('sge'),
          office_id: current_user.office_id,
          office_type: current_user.office_type,
          branch_id: current_user.branch,
          area_id: current_user.area,
          region_id: current_user.region,
          head_id: current_user.head,
          transaction_deviations_attributes: @deviation
        }
      end

      def set_residence_address
        data_residence = {
          province: @pawn_transaction.customer.customer_contact.residence_province,
          city: @pawn_transaction.customer.customer_contact.residence_city,
          region: @pawn_transaction.customer.customer_contact.residence_region,
          subdistric: @pawn_transaction.customer.customer_contact.residence_subdistric
        }

        data_residence = get_master("address?#{data_residence.to_query}")['data']

        @pawn_transaction.customer.customer_contact.residence_province_name = data_residence['province_name']
        @pawn_transaction.customer.customer_contact.residence_city_name = data_residence['city_name']
        @pawn_transaction.customer.customer_contact.residence_region_name = data_residence['region_name']
        @pawn_transaction.customer.customer_contact.residence_subdistric_name = data_residence['subdistric_name']
      rescue StandardError
        @pawn_transaction.customer.customer_contact.residence_province_name = nil
        @pawn_transaction.customer.customer_contact.residence_city_name = nil
        @pawn_transaction.customer.customer_contact.residence_region_name = nil
        @pawn_transaction.customer.customer_contact.residence_subdistric_name = nil
      end

      def check_created_by_id
        render json: { message: 'Unauthorize' }, status: :unauthorized unless params[:token].present?
        auth.set_token("Bearer #{params[:token]}")
        @user_created = auth.current_user
      end

      def check_type_of_transaction
        set_pawn_transaction
        @type_of_transaction = (@pawn_transaction.transaction_type == 'prolongation' && @pawn_transaction.prolongation.nil?) ||
                               (@pawn_transaction.transaction_type == 'disbursement' && @pawn_transaction.prolongation.nil?)
      end

      def check_published
        if @pawn_transaction.status == 'published'
          render template: 'transactions/401_transaction_print_sge'
        else
          @pawn_transaction.update(status: :published)
          # Journal::TransactionPublishService.new(@pawn_transaction).execute
        end
      end

      def check_approval(render: true)
        approval = @pawn_transaction.try(:transaction_approvals)
        if approval.where(user_id: nil).present?
          render json: { message: 'Approval belum dilakukan' }, status: :unauthorized if render
          false
        else
          true
        end
      end

      def set_deviation
        url = "product/#{pawn_transaction_params[:product_id]}/deviation/#{pawn_transaction_params[:insurance_item_id]}/#{current_user.region}/#{pawn_transaction_params[:loan_amount]}"
        deviation_item = get_master(url)
        @deviation = Transaction::DeviationService.new(transaction: pawn_transaction_params, deviation: deviation_item, user: current_user)
        @deviation = @deviation.execute
      rescue StandardError => e
        response_bad(e)
      end

      def set_transactions_type
        if params.key?(:transaction_type)
          @pawn_transactions = set_transaction.where(transaction_type: params[:transaction_type])
        else
          @pawn_transaction = set_transaction
        end
      end

      def set_transaction
        if params.key?(:type) && (params[:type] == 'customer' || params[:type] == 'company')
          check_transaction_type
        else
          @pawn_transaction = PawnTransaction
        end
      end

      def check_transaction_type
        @pawn_transaction = case params[:type]
                            when 'customer'
                              PawnTransaction.where(company_id: nil)
                            when 'company'
                              PawnTransaction.where(customer_id: nil)
                            else
                              PawnTransaction
                            end
      end

      def check_office
        render json: { message: 'Unauthorize' }, status: :unauthorized unless params[:token].present?
        return unless params[:token].present?

        auth.set_token("Bearer #{params[:token]}")
        current_user = auth.current_user

        @transactions = case current_user.office_type
                        when 'head'
                          PawnTransaction.where(head_id: current_user.head)
                        when 'region'
                          PawnTransaction.where(region_id: current_user.region)
                        when 'area'
                          PawnTransaction.where(area_id: current_user.area)
                        when 'branch'
                          PawnTransaction.where(branch_id: current_user.branch)
                        when 'unit'
                          PawnTransaction.where(office_id: current_user.office_id)
                        end
      end

      def notification_hash
        {
          offices: [
            {
              office_id: current_user.office_id.to_s,
              role: 'KU'
            }
          ],
          type: 'siscab',
          subject: :transaction_approval,
          body: "Transaksi #{@pawn_transaction.sge} perlu disetujui!",
          url: "/app/transaction/individual/#{@pawn_transaction.id}/installment"
        }
      end

      def send_notification
        res = Notifications::SendNotificationService.new(notification_hash)
        res.execute
      end

      # Use callbacks to share common setup or constraints between actions.
      def set_pawn_transaction
        @pawn_transaction = PawnTransaction.find(params[:id])
      rescue StandardError
        response_not_found('Data Tidak Ditemukan')
      end

      def update_dashboard(_transaction = @pawn_transaction)
        trans = _transaction

        if trans.present?
          dashboards = Dashboard.find_or_create_by(date: trans[:contract_date])
          hash = {
            "os_#{trans[:transaction_type] || 'prolongation'}" => dashboards["os_#{trans[:transaction_type] || 'prolongation'}"].to_i + trans[:payment_amount].to_i,
            'month' => trans[:contract_date].to_date.strftime('%Y-%m'),
            'office_id' => trans[:office_id]
          }
          dashboards.update_attributes(hash)
        end
      end

      # Only allow a trusted parameter "white list" through.
      def pawn_transaction_params
        params.require(:transaction).permit(:insurance_item_id, :customer_id, :transaction_type, :product_name, :interest_rate,
                                            :company_id, :product_id, :status, :contract_date, :due_date, :auction_date,
                                            :loan_amount, :admin_fee, :monthly_fee, :per_15_days_fee, :insurance_item_name, :slte_rate,
                                            :maximum_loan, :maximum_loan_percentage, :notes, :changed, :payment_amount, :is_option,
                                            transaction_insurance_items_attributes: [:id, :name,
                                                                                     :product_insurance_item_id,
                                                                                     :product_insurance_item_name,
                                                                                     :insurance_item_image,
                                                                                     :ownership, :amount, :net_weight,
                                                                                     :carats, :description, :gross_weight,
                                                                                     :estimated_value, :_destroy],
                                            transaction_deviation_item: [:type, :office_type, :office_id, :status])
      end
    end
  end
end
