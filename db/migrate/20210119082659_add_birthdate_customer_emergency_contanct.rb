class AddBirthdateCustomerEmergencyContanct < ActiveRecord::Migration[6.0]
  def change
    add_column :customer_emergency_contacts, :birth_place, :string, null: true
    add_column :customer_emergency_contacts, :birth_date, :date, null: true
  end
end

