class AddColumnToCustomerBankAccount < ActiveRecord::Migration[6.0]
  def change
    add_column :customer_bank_accounts, :main_account, :boolean, default: false
  end
end
