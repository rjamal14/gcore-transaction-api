class AddInsuranceItemNameToPawnTransaction < ActiveRecord::Migration[6.0]
  def change
    add_column :pawn_transactions, :insurance_item_name, :string
  end
end
