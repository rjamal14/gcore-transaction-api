# Bank Model
class CustomerBankAccount < ApplicationRecord
  acts_as_paranoid
  belongs_to :customer
end
