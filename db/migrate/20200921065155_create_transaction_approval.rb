class CreateTransactionApproval < ActiveRecord::Migration[6.0]
  def change
    create_table :transaction_approvals do |t|
      t.integer :pawn_transaction_id
      t.integer :user_id
      t.string :user_type
      t.string :attachment
      t.string :updated_by_id
      t.timestamps
    end
  end
end
