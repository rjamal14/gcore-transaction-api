# CashTransactionsSerializer
class CashTransactionsSerializer < ActiveModel::Serializer
  attributes :id, :req_id, :req_subject, :req_description, :req_amount, :applicant_name, :req_coa, :req_status
end
