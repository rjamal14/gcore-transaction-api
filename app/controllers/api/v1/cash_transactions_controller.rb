# cash_transaction Controller
module Api
  module V1
    # cash_transaction  Controller
    class CashTransactionsController < ApplicationController
      before_action :set_cash_transaction, only: [:show, :update, :destroy]
      before_action :get_by_office_id, only: :index
      before_action :check_cashier, only: [:create, :update, :destroy]

      def index
        @cash_transactions = if params[:search]
                               @cash_transactions.search(params[:search])
                             else
                               @cash_transactions
                             end

        @cash_transactions = @cash_transactions.page(params[:page] || 1).per(params[:per_page] || 10)

        serial_cash_transactions = @cash_transactions.map do |cash_transaction|
          CashTransactionsSerializer.new(
            cash_transaction, root: false
          )
        end

        response_index(@cash_transactions, serial_cash_transactions)
      end

      def create
        @cash_transaction = CashTransaction.new(
          cash_transaction_params.merge(
            req_status: 'approved',
            created_by_id: current_user.id,
            office_id: current_user.office_id
          )
        )
        cashier_status = check_cashier_report('cash', @cash_transaction)
        return true if cashier_status.present?

        if @cash_transaction.save
          response_created(@cash_transaction, 'Data CashTransaction Tersimpan')
        else
          response_error(@cash_transaction, 'Data CashTransaction Gagal Tersimpan')
        end
      end

      def show
        cash_transaction = CashTransactionsSerializer.new(@cash_transaction)
        response_success(cash_transaction, 'Data Ditemukan')
      end

      def update
        if @cash_transaction.update(cash_transaction_params.merge(updated_by_id: current_user.id))
          response_updated(@cash_transaction, 'Data CashTransaction Terupdate')
        else
          response_error(@cash_transaction, 'Data CashTransaction Gagal Terupdate')
        end
      end

      def destroy
        return unless @cash_transaction.destroy

        response_ok('Data company Terhapus')
        @cash_transaction.update(deleted_by_id: current_user.id)
      end

      private

      def get_by_office_id
        @cash_transactions = if current_user.office_type == 'head'
                               CashTransaction
                             else
                               CashTransaction.where(office_id: current_user.office_id)
                             end

        @cash_transactions = @cash_transactions.ransack(params[:q]).result
      end

      def set_cash_transaction
        @cash_transaction = CashTransaction.find(params[:id])
      rescue StandardError
        response_not_found('Data Tidak Ditemukan')
      end

      def cash_transaction_params
        params.require(:cash_transaction).permit(:req_subject, :req_description, :req_amount, :applicant_name, :req_coa)
      end
    end
  end
end
