# Instalation Guide For Transaction API

This is instalation guide for Transaction API for local environtment, in linux environtment

## Installation

For running this App make sure, you had : 
- Ruby version 2.6.4
- Rails 6
- Postgresql
- Bundler

If you had Debian base machine you can follow this tutorial to install the package need above
https://gorails.com/setup/ubuntu/20.04. After you do that steps above you can start next step
you can install required Gem using

```bash
bundle install 
```
And you had install gem Whenever and Rubocop manualy using gem install
```bash
gem install rubocop whenever 
```
After you completed the steps above, there is one last step, you must set the environtment
variable and database setting variable. 

First you have to create .env file in root folder, the file will look like this

```dotenv
# This is the example of .env file you can modified
# Database Credential Development
export HOST_DEV = localhost # Your Development Host
export USER_DEV = user_development # Your Development username
export DB_DEV =  gcore-transaction-db # Your Development Database
export PASS_DEV = password # Your Development password

# Database Credential Development
export HOST_TEST = localhost # Your Host
export USER_TEST = user_test # Your Hosta
export DB_TEST = db_test # Your Host
export PASS_TEST = password # Your Host

# Database Credential Test
export HOST_PRODUCTION = localhost # Your Host
export USER_PRODUCTION = user_production # Your Hosta
export DB_PRODUCTION = db_production # Your Host
export PASS_PRODUCTION = password_production # Your Host

# Auth URI
export AUTH_URL = localhost:3001
export MASTER_URL = localhost:3002
```

In this file above, you can change the environtment variable needed. And second you have to create 
database.yml file in config folder, there is database.yml.example file in config folder you can use as example

Congrats, you finish the installation steps, dont forget to create the database in postgre and role you desire
## Usage
Before you can start the app, you must migrate the database, I assume you already create the database
you can type in the console
```bash
rails db:migrate
```
after you migrate the database, now you can finally run the app using
```bash
rails s
```
you can specify the port the app using
```bash
rails s -p PORT_NUMBER
```