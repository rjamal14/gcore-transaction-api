# customer Controller
module Api
  module V1
    # Product  Controller
    class ProductsController < ApplicationController
      before_action :product_getter
      skip_before_action :current_user
      def index_of_products
        serial = {}
        serial = @products['data'].map do |product|
          num_of_transactions(product)
        end
        response_product(@products, serial)
      end

      private

      def product_getter
        @products = get_master "/product/?page=#{params[:page] || ''}&search=" \
        "#{params[:search] || ''}&per_page=#{params[:per_page] || ''}&type=all"
      rescue RestClient::NotFound
        response_bad('Product')
      rescue RestClient::Unauthorized
        render json: { code: 401, success: false, message: 'Unauthorized' }, status: 401
      else
        @products
      end

      def num_of_transactions(product)
        id = product['id']['$oid']
        @transactions = PawnTransaction.where(product_id: id)
        num_of_transactions = @transactions.count
        sum_of_loan = @transactions.map { |s| s[:loan_amount] }.reduce(0, :+)
        products = {}
        products[:id] = product['id']
        products[:name] = product['name']
        products[:status] = product['status']
        products[:tiering_type] = product['tiering_type']
        products[:insurance_item_type] = product['insurance_item_type']
        products[:period_of_time] = product['period_of_time']
        products[:noa] = num_of_transactions
        products[:os] = sum_of_loan
        products
      end
    end
  end
end
