class AddColumnbranchToPawnTransaction < ActiveRecord::Migration[6.0]
  def change
    add_column :pawn_transactions, :branch_id, :string
    add_column :pawn_transactions, :area_id, :string
    add_column :pawn_transactions, :region_id, :string
    add_column :pawn_transactions, :head_id, :string
  end
end
