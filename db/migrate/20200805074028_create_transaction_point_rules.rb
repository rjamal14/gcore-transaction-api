class CreateTransactionPointRules < ActiveRecord::Migration[6.0]
  def change
    create_table :transaction_point_rules do |t|
      t.string :activity_name
      t.integer :point_amount
      t.date :expired_date
      t.boolean :type
      t.string :created_by_id
      t.string :updated_by_id
      t.string :deleted_by_id
      t.datetime :deleted_at, index: true

      t.timestamps
    end
  end
end
