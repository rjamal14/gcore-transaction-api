# CustomerSerializer
class CustomerAutocompleteSerializer < ActiveModel::Serializer
  attributes :id, :name, :identity_number
end
