class ChangeProlongationOrderDefaultValueInPawnTransactions < ActiveRecord::Migration[6.0]
  def change
    remove_column :pawn_transactions, :prolongation_order
    add_column :pawn_transactions, :prolongation_order, :integer, default: 0
  end
end
