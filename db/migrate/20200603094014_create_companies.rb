class CreateCompanies < ActiveRecord::Migration[6.0]
  def change
    create_table :companies do |t|
      t.string :name
      t.string :cif_number
      t.string :phone_number
      t.string :tax_number
      t.string :city_id
      t.string :city_name
      t.text :address
      t.string :branch_office_id
      t.string :branch_office_name
      t.boolean :status, default: false
      t.string :created_by_id
      t.string :updated_by_id
      t.string :deleted_by_id
      t.datetime :deleted_at, index: true

      t.timestamps
    end
  end
end
