class AddStatusOnInstallmentItems < ActiveRecord::Migration[6.0]
  def change
    add_column :installment_items, :status, :boolean, default: false
  end
end
