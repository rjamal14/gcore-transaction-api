# Bill
module Transaction
  # Bill Service
  class TransactionBillService
    def initialize(id:, _user: '')
      @user = _user
      @auth = Auth::AuthorizeService.new
      @auth.set_token('')
      @transaction = PawnTransaction.find(id)
      @fine_amount = assign_fine_amount
      @loan_amount = @transaction.loan_amount
      @estimated_value = @transaction.transaction_insurance_items.sum(:estimated_value)
      @contract_date = @transaction.contract_date
      @number_of_days = (Date.today - @contract_date).to_i.zero? ? 1 : (Date.today - @contract_date).to_i
      set_multiplier
      @rental_cost = @multiplier * @rental_fee
      transaction_date = @auth.get_master("due_dates?product_id=#{@transaction.product_id}&contract_date=#{Date.today}")
      prolongation_date_getter
      @transaction_date = { contract_date: Date.today }
      %w[due_date grace_period auction_date].each do |assign_date|
        @transaction_date[assign_date] = transaction_date['error'].blank? ? transaction_date['data'][assign_date] : nil
      end
    end

    def set_multiplier
      case @transaction.is_option
      when 'weekly'
        @multiplier = (@number_of_days.to_f / 7).ceil
        @rental_fee = @transaction.monthly_fee / 4
      when 'daily'
        @multiplier = @number_of_days
        @rental_fee = @transaction.monthly_fee / 30
      else
        @multiplier = (@number_of_days.to_f / 15).ceil
        @rental_fee = @transaction.monthly_fee / 2
      end
    end

    def bill_master
      {
        loan_amount: @loan_amount,
        parent_sge: @transaction.sge,
        estimate_value: @estimated_value,
        rental_per_15_days: "#{(@rental_fee.to_f / @loan_amount * 100).round(2)} %",
        contract_date: @contract_date,
        due_date: @transaction.due_date,
        fine_amount: @fine_amount,
        total_rental_cost: @rental_cost,
        number_of_days: @number_of_days,
        multiplier: @multiplier,
        payment_date: Date.today
      }
    end

    def repayment_bill
      bill_master.merge(total_bill: repayment_amount)
    end

    def repayment_amount
      @loan_amount + @fine_amount + @rental_cost
    end

    def prolongation_bill
      bill_master.merge(total_bill: prolongation_amount,
                        admin_fee: (@prolongation_data['admin'].to_f * @transaction.loan_amount / 100).to_i,
                        monthly_interest: @prolongation_data['rental'].to_f,
                        interest_per_15_days: @prolongation_data['rental'].to_f / 2,
                        prolongation_date: @transaction_date,
                        prolongation_order: "Perpanjangan ke #{(@transaction.prolongation_order + 1).humanize.upcase_first}")
    end

    def prolongation_date_getter
      url = "product/#{@transaction.product_id}/prolongation?insurance_item_id=#{@transaction.insurance_item_id}&order=#{@transaction.prolongation_order + 1}"
      @prolongation_data = @auth.get_master(url)
    rescue StandardError
      raise StandardError, 'Gadai Ulang Pada Produk Belum di Setup'
    end

    def prolongation_amount
      @fine_amount + @rental_cost + @transaction.admin_fee
    end

    def assign_fine_amount
      fine_product = @auth.get_master("product/#{@transaction.product_id}/fine_detail")
      due_date = @transaction.due_date + fine_product['grace_period'].day
      return 0 if Date.today <= due_date

      date_count = (due_date..Date.today).count
      ((date_count - 1) / 15.to_f).ceil(0) * (@transaction.loan_amount * (fine_product['fine_percentage'] / 100))
    end
  end
end
