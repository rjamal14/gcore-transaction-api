class AddAreaCodeToCompany < ActiveRecord::Migration[6.0]
  def change
    add_column :companies, :area_code, :string
  end
end
