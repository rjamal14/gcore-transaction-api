require 'swagger_helper'

RSpec.describe 'api/v1/companies', type: :request do
  path '/api/v1/companies' do
    get('Get List Companies') do
      tags 'Companies'
      security [Bearer: {}]

      parameter name: 'page', in: 'query', type: :string
      parameter name: 'per_page', in: 'query', type: :string
      parameter name: 'order_by', in: 'query', type: :string
      parameter name: 'order', in: 'query', type: :string
      parameter name: 'search', in: 'query', type: :string
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    post('Create Company') do
      tags 'Companies'
      security [Bearer: {}]

      parameter name: :company, in: :body, description: 'Company Parameter', schema: {
        type: :object,
        example: {
          "company": {
            "name": 'PT Maju Mundur',
            "phone_number": '0202201930',
            "tax_number": '12818211',
            "city_id": '1',
            "city_name": 'Bandung',
            "address": 'jl lurus',
            "company_pics_attributes":
              [{
                "name": 'Acep',
                "phone_number": '088888888',
                "position": 'Manager',
                "identity_number": '0000001',
                "identity_type": 'ktp',
                "address": 'Disitu'
              },
               {
                 "name": 'Anang',
                 "phone_number": '0999998999',
                 "position": 'Manager',
                 "identity_number": '0000002',
                 "identity_type": 'ktp',
                 "address": 'Disitu'
               }]
          }
        }
      }
      response(200, 'successful') do
        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end

  path '/api/v1/companies/{id}' do
    # You'll want to customize the parameter types...
    parameter name: 'id', in: :path, type: :string, description: 'id'

    get('Show Company') do
      tags 'Companies'
      security [Bearer: {}]

      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    put('Update Company') do
      tags 'Companies'
      security [Bearer: {}]

      parameter name: :company, in: :body, description: 'Company Parameter', schema: {
        type: :object,
        example: {
          "company": {
            "name": 'PT Maju Mundur',
            "phone_number": '0202201930',
            "tax_number": '12818211',
            "city_id": '1',
            "city_name": 'Bandung',
            "address": 'jl lurus',
            "company_pics_attributes":
              [{
                "name": 'Acep',
                "phone_number": '088888888',
                "position": 'Manager',
                "identity_number": '0000001',
                "identity_type": 'ktp',
                "address": 'Disitu'
              },
               {
                 "name": 'Anang',
                 "phone_number": '0999998999',
                 "position": 'Manager',
                 "identity_number": '0000002',
                 "identity_type": 'ktp',
                 "address": 'Disitu'
               }]
          }
        }
      }
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    delete('Delete Company') do
      tags 'Companies'
      security [Bearer: {}]

      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    patch('Delete a Company PIC Data') do
      tags 'Companies'
      security [Bearer: {}]

      parameter name: :company, in: :body, description: 'Company Parameter', schema: {
        type: :object,
        example: {
          "id": 1,
          "name": 'jamasdsl',
          "cif_number": '21464370121054',
          "phone_number": '0855245703116',
          "tax_number": '12121111',
          "city_id": '1',
          "address": 'jl lurus',
          "branch_office_id": '123124',
          "branch_office_name": 'Kancab Bandung',
          "city_name": 'Bandung',
          "company_pics": [
            {
              "id": 1,
              "name": 'Unyu',
              "company_id": 6,
              "position": 'Manager',
              "identity_number": '12123121223212323',
              "identity_type": 'null',
              "phone_number": '0855090252',
              "address": 'Jalan Panjang',
              "_destroy": true
            }
          ]
        }
      }
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end

    patch('Patch Status Company') do
      tags 'Companies'
      security [Bearer: {}]

      parameter name: :company, in: :body, description: 'Company Parameter', schema: {
        type: :object,
        example: {
          "status": true
        }
      }
      response(200, 'successful') do
        let(:id) { '123' }

        after do |example|
          example.metadata[:response][:content] = {
            'application/json' => {
              example: JSON.parse(response.body, symbolize_names: true)
            }
          }
        end
        run_test!
      end
    end
  end
end
