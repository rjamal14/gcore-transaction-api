class CreateTransactionDeviations < ActiveRecord::Migration[6.0]
  def change
    create_table :transaction_deviations do |t|
      t.belongs_to :pawn_transaction
      t.integer :deviation_type
      t.integer :office_type
      t.string :office_id
      t.boolean :status, default: false
      t.datetime :deleted_at, index: true

      t.timestamps
    end
  end
end
