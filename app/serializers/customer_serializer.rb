# CustomerSerializer
class CustomerSerializer < ActiveModel::Serializer
  attributes :id, :cif_number, :name, :branch_office_name, :os, :status, :phone, :sge, :email, :city, :created_by_name,
             :created_at, :identity_number
  def os
    object.pawn_transactions.active.sum(:loan_amount)
  end

  def phone
    object.customer_contact.phone_number
  end

  def city
    object.customer_contact.residence_city_name
  end

  def sge
    object.pawn_transactions.count
  end
end
