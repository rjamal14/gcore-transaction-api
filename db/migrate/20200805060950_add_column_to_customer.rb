class AddColumnToCustomer < ActiveRecord::Migration[6.0]
  def change
    add_column :customers, :email, :string
    add_column :customers, :referral_code, :string
    add_column :customers, :point, :integer
  end
end
