class CustomerCreateBankAccountsWorker
  include Sneakers::Worker

  queue_name = queue_name = "#{Rails.env}-migrations.customers.bank_accounts.create"
  from_queue queue_name, arguments: { 'x-dead-letter-exchange': "#{queue_name}-retry" }

  def work(msg)
    data = ActiveSupport::JSON.decode(msg)
    data.each do |value|
      CustomerBankAccount.new(value.to_h).save!
    end
    ack!
  rescue StandardError => e
    logger.fatal "Error message CustomerCreateBankAccountsWorker => #{e.message}"
    reject!
  end
end
