# Point Model
class TransactionPoint < ApplicationRecord
  acts_as_paranoid
  belongs_to :transaction_point_rule
  belongs_to :customer
end
