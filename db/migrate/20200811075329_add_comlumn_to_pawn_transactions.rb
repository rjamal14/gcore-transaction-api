class AddComlumnToPawnTransactions < ActiveRecord::Migration[6.0]
  def change
    add_column :pawn_transactions, :slte_rate, :float
    add_column :pawn_transactions, :maximum_loan, :integer
    add_column :pawn_transactions, :maximum_loan_percentage, :float
    add_column :pawn_transactions, :notes, :string
  end
end
