# Transaction Insurance Item Model
class CashoutTransaction < ApplicationRecord
  acts_as_paranoid

  enum status: [:approved, :waiting_approval, :rejected]

  validates_presence_of :user_id, :user_name, :date,
                        :amount, :description, :status, :office_id

  validates :created_by_id, presence: true, on: :create
  validates :updated_by_id, presence: true, on: :update

  has_many :cashout_accounts, index_errors: true, dependent: :destroy

  accepts_nested_attributes_for :cashout_accounts, allow_destroy: true
end
